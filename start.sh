#!/bin/bash
DATE="$(date --iso=s)"
(
  killall light-locker
  xset -dpms
  xset s off
  xrandr --output LVDS-0 --same-as HDMI-0 --scale-from 1920x1080
  while true
  do
    RDEX_UPLOAD="http://rdex/sounding-diy-iii-vm-ml/upload/new" RDEX_SESSION="${HOME}/opt/var/rdex/session" "${HOME}/opt/bin/rdex"
    sleep 1
  done &
) > "${HOME}/opt/var/rdex/log/${DATE}.log" 2>&1
