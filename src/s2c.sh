#!/bin/bash
echo "/* machine-generated file, do not edit */"
echo "static const char $1[] = \"#extension GL_EXT_gpu_shader4 : enable\n\""
sed 's|//.*||' |
sed 's|^|"|' |
sed 's|$|"|'
echo ";"
