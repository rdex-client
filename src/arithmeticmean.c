/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Arithmetic Mean Shader
===================================================================== */

#include <math.h>
#include "util.h"
#include "arithmeticmean.h"
#include "arithmeticmean.frag.c"

//======================================================================
// arithmetic mean shader initialization
struct arithmeticmean *arithmeticmean_init(
  struct arithmeticmean *arithmeticmean
) {
  if (! arithmeticmean) { return 0; }
  if (! shader_init(
          &arithmeticmean->shader, 0, arithmeticmean_frag
     )) {
    return 0;
  }
  shader_uniform(arithmeticmean, texture);
  shader_uniform(arithmeticmean, dx);
  shader_uniform(arithmeticmean, dy);
  arithmeticmean->value.texture = 0;
  arithmeticmean->value.dx = 0;
  arithmeticmean->value.dy = 0;
  glGenTextures(arithmeticmean_tex_max, arithmeticmean->textures);
  glEnable(GL_TEXTURE_2D);
  for (int i = 0; i < arithmeticmean_tex_max; ++i) {
    glBindTexture(GL_TEXTURE_2D, arithmeticmean->textures[i]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  }
  glDisable(GL_TEXTURE_2D);
  arithmeticmean->result[0] = 0;
  arithmeticmean->result[1] = 0;
  arithmeticmean->result[2] = 0;
  arithmeticmean->result[3] = 0;
  arithmeticmean->stddev = 0;
  return arithmeticmean;
}

//======================================================================
// arithmetic mean shader reshape callback
void arithmeticmean_reshape(
  struct arithmeticmean *arithmeticmean, int w, int h
) {
  int w2 = roundtwo(w);
  int h2 = roundtwo(h);
  int d = w2>h2 ? w2 : h2;
  arithmeticmean->count = logtwo(d);
  glEnable(GL_TEXTURE_2D);
  for (int i = 0; i < arithmeticmean->count; ++i) {
    glBindTexture(GL_TEXTURE_2D, arithmeticmean->textures[i]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F_ARB,
      1<<i, 1<<i, 0, GL_RGBA, GL_FLOAT, 0
    );
  }
  glDisable(GL_TEXTURE_2D);
}

//======================================================================
// arithmetic mean display callback
void arithmeticmean_display(
  struct arithmeticmean *arithmeticmean, GLuint fbo, GLuint texture
) {
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, texture);
//  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
  glUseProgramObjectARB(arithmeticmean->shader.program);
  for (int i = arithmeticmean->count - 1; i >= 0; --i) {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, 1, 0, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glViewport(0, 0, 1<<i, 1<<i);
    glFramebufferTexture2DEXT(
      GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D,
      arithmeticmean->textures[i], 0
    );
    arithmeticmean->value.dx = 0.5 / (1 << i);
    arithmeticmean->value.dy = 0.5 / (1 << i);
    shader_updatei(arithmeticmean, texture);
    shader_updatef(arithmeticmean, dx);
    shader_updatef(arithmeticmean, dy);
    glBegin(GL_QUADS); { glColor4f(1,1,1,1);
      glTexCoord2f(0, 0); glVertex2f(0, 0);
      glTexCoord2f(1, 0); glVertex2f(1, 0);
      glTexCoord2f(1, 1); glVertex2f(1, 1);
      glTexCoord2f(0, 1); glVertex2f(0, 1);
    } glEnd();
    glBindTexture(GL_TEXTURE_2D, arithmeticmean->textures[i]);
  }
  glGetTexImage(
    GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, &arithmeticmean->result
  );
  glUseProgramObjectARB(0);
  //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  glDisable(GL_TEXTURE_2D);
}

//======================================================================
// arithmetic mean idle callback
void arithmeticmean_idle(struct arithmeticmean *arithmeticmean) {
  arithmeticmean->stddev =
    sqrt(arithmeticmean->result[1]
       - arithmeticmean->result[0] * arithmeticmean->result[0]) +
    sqrt(arithmeticmean->result[3]
       - arithmeticmean->result[2] * arithmeticmean->result[2]);
  if (isnan(arithmeticmean->stddev)) { arithmeticmean->stddev = 0; }
}

// EOF
