/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2010,2011  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Sphere shader for ball picking
===================================================================== */

varying float ignore;

void main(void) {
  vec2 p = gl_TexCoord[0].xy;               // float in [0..1]
  vec2 q0 = gl_TexCoord[0].zw;
  vec2 q1 = 2.0 * (q0 - vec2(0.5, 0.5));
  float d = dot(q1,q1);
  if (ignore < 0.5 && d < 1.0) {
    float k = gl_FragCoord.z - 0.1 * cos(sqrt(d) * 3.1415926*0.5);
    gl_FragDepth = k;
    gl_FragColor = vec4(p, 0.0, 1.0);
  } else {
    discard;
  }
}
