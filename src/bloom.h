/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Bloom/Glow Effect
===================================================================== */

#ifndef BLOOM_H
#define BLOOM_H 1

#include "shader.h"
#include "gblur.h"

//======================================================================
// shader data
struct bloom { struct shader shader;
  struct { GLint tex0, tex1, tex2, tex3, tex4, tex5, tex6, tex7; } uniform;
  struct { int   tex0, tex1, tex2, tex3, tex4, tex5, tex6, tex7; } value;
  struct gblur gblur;
};

//======================================================================
// prototypes
struct bloom *bloom_init(struct bloom *bloom);
void bloom_reshape(struct bloom *bloom, int w, int h);
void bloom_display(struct bloom *bloom, GLuint fbo, GLuint texture);

#endif
