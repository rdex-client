/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2010,2019  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Screenshot Module
===================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <GL/glew.h>
#include "screenshot.h"

//======================================================================
// screenshot data
struct screenshot_data {
  int width;
  int height;
  unsigned char *data;
};

// background consumer
void screenshot_output(struct screenshot *screenshot, size_t s, struct screenshot_data *sd) {
  if (sd && sd->data) {
    fprintf(stdout, "P6\n%d %d 255\n", sd->width, sd->height);
    for (int y = sd->height - 1; y >= 0; --y) {
      fwrite(sd->data + y * sd->width * 3, sd->width * 3, 1, stdout);
    }
    fflush(stdout);
    free(sd->data);
  }
}

//======================================================================
// screenshot module initialization
struct screenshot *screenshot_init(struct screenshot *screenshot, int nrt) {
  if (! screenshot) { return 0; }
  screenshot->nrt = nrt;
  screenshot->rate = 1; // FIXME use environment variable for this
  screenshot->frame = 0;
  screenshot->width = 0;
  screenshot->height = 0;
  screenshot->buffer = 0;
  // fbo
//  glGenFramebuffersEXT(1, &screenshot->fbo);
  // pbo
  glGenBuffersARB(1, &screenshot->pbo);
  glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, screenshot->pbo);
  glBufferDataARB(GL_PIXEL_PACK_BUFFER_ARB, 1024 * 768 * 3, 0, GL_STREAM_READ_ARB); // FIXME hardcoded
  glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, 0);
  screenshot->ready = 0;
  // background output
#if 0
  screenshot->queue = pfifo_create((pfifo_consumer *) screenshot_output, screenshot);
#endif
  return screenshot;
}

//======================================================================
// screenshot module display callbacks


// save PBO data
void screenshot_display1(struct screenshot *screenshot) {
  if (! screenshot->ready) { return; }
  glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, screenshot->pbo);
  unsigned char *p = glMapBufferARB(GL_PIXEL_PACK_BUFFER_ARB, GL_READ_ONLY_ARB);
  if (p) {
    struct screenshot_data sd;
    sd.width = screenshot->width;
    sd.height = screenshot->height;
    sd.data = malloc(sd.width * sd.height * 3);
    memcpy(sd.data, p, sd.width * sd.height * 3);
#if 0
    pfifo_enqueue(screenshot->queue, sizeof(struct screenshot_data), &sd);
#endif
    if (! glUnmapBufferARB(GL_PIXEL_PACK_BUFFER_ARB)) {
      fprintf(stderr, "unmap buffer error\n");
    }
#if 1
    screenshot_output(screenshot, sizeof(struct screenshot_data), &sd);
#endif
  }
  glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, 0);
}

// copy frame to PBO and start download
void screenshot_display2(struct screenshot *screenshot) {
  int fps = 25;
  int rate = screenshot->rate;
  int frame = screenshot->frame;
  int period = 60 * 60 * fps * rate; // 60 minutes
  int interval = 5 * 60 * fps * rate; // 5 minutes
  if (screenshot->nrt && (frame % rate) == 0 && (frame % period) < interval) {
    // copy framebuffer to PBO
    glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, screenshot->pbo);
    glReadPixels(0, 0, screenshot->width, screenshot->height, GL_RGB, GL_UNSIGNED_BYTE, 0);
    glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, 0);
/*
    glReadPixels(0, 0, screenshot->width, screenshot->height, GL_RGB, GL_UNSIGNED_BYTE, screenshot->buffer);
    fprintf(stdout, "P6\n%d %d 255\n", screenshot->width, screenshot->height);
    for (int y = screenshot->height - 1; y >= 0; --y) {
      fwrite(screenshot->buffer + y * screenshot->width * 3, screenshot->width * 3, 1, stdout);
    }
*/
    screenshot->ready = 1;
  } else {
    screenshot->ready = 0;
  }
  screenshot->frame += 1;
}

//======================================================================
// screenshot module reshape callback
void screenshot_reshape(struct screenshot *screenshot, int w, int h) {
  screenshot->width = w;
  screenshot->height = h;
  if (screenshot->buffer) free(screenshot->buffer);
  screenshot->buffer = malloc(w * h * 3);
  glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, screenshot->pbo);
  glBufferDataARB(GL_PIXEL_PACK_BUFFER_ARB, (w + 4) * h * 3, 0, GL_STREAM_READ_ARB);
  glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, 0);
}

//======================================================================
// screenshot module idle callback
void screenshot_idle(struct screenshot *screenshot) {
}

// EOF
