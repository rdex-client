/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Numeric Error Shader
===================================================================== */

#include "util.h"
#include "numericerror.h"
#include "numericerror.frag.c"

//======================================================================
// numeric error shader initialization
struct numericerror *numericerror_init(
  struct numericerror *numericerror
) {
  if (! numericerror) { return 0; }
  if (! shader_init(&numericerror->shader, 0, numericerror_frag)) {
    return 0;
  }
  shader_uniform(numericerror, texture);
  shader_uniform(numericerror, dx);
  shader_uniform(numericerror, dy);
  numericerror->value.texture = 0;
  numericerror->value.dx      = 0;
  numericerror->value.dy      = 0;
  if (! arithmeticmean_init(&numericerror->arithmeticmean)) {
    return 0;
  }
  glGenTextures(1, &numericerror->texture);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, numericerror->texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glDisable(GL_TEXTURE_2D);
  numericerror->width = 0;
  numericerror->height = 0;
  numericerror->calc = 0;
  numericerror->mean = 0;
  return numericerror;
}

//======================================================================
// numeric error shader reshape callback
void numericerror_reshape(
  struct numericerror *numericerror, int w, int h
) {
  numericerror->width  = roundtwo(w);
  numericerror->height = roundtwo(h);
  numericerror->value.dx = 1.0 / numericerror->width;
  numericerror->value.dy = 1.0 / numericerror->height;
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, numericerror->texture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F_ARB,
    numericerror->width, numericerror->height, 0, GL_RGBA, GL_FLOAT, 0
  );
  glDisable(GL_TEXTURE_2D);
  arithmeticmean_reshape(&numericerror->arithmeticmean, w, h);
}

//======================================================================
// numeric error shader display callback
void numericerror_display(
  struct numericerror *numericerror, GLuint fbo, GLuint texture
) {
  if (numericerror->calc) {
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);
    //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
    glFramebufferTexture2DEXT(
      GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D,
      numericerror->texture, 0
    );
    glUseProgramObjectARB(numericerror->shader.program);
    shader_updatei(numericerror, texture);
    shader_updatef(numericerror, dx);
    shader_updatef(numericerror, dy);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, 1, 0, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glViewport(0, 0, numericerror->width, numericerror->height);
    glBegin(GL_QUADS); { glColor4f(1,1,1,1);
      glTexCoord2f(0, 0); glVertex2f(0, 0);
      glTexCoord2f(1, 0); glVertex2f(1, 0);
      glTexCoord2f(1, 1); glVertex2f(1, 1);
      glTexCoord2f(0, 1); glVertex2f(0, 1);
    } glEnd();
    glUseProgramObjectARB(0);
    //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
    glDisable(GL_TEXTURE_2D);
    arithmeticmean_display(
      &numericerror->arithmeticmean, fbo, numericerror->texture
    );
    numericerror->mean = numericerror->arithmeticmean.result[0];
    numericerror->calc = 0;
  }
}

//======================================================================
// numeric error shader idle callback
void numericerror_idle(struct numericerror *numericerror) {
  arithmeticmean_idle(&numericerror->arithmeticmean);
}

// EOF
