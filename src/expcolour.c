/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Exponential colour space conversion
===================================================================== */

#include "expcolour.h"
#include "expcolour.frag.c"

//======================================================================
// shader initialization
struct expcolour *expcolour_init(struct expcolour *expcolour) {
  if (! expcolour) { return 0; }
  if (! shader_init(&expcolour->shader, 0, expcolour_frag)) {
    return 0;
  }
  shader_uniform(expcolour, texture);
  expcolour->value.texture = 0;
  return expcolour;
}

// EOF
