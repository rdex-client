/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Difference Shader
===================================================================== */

#include "util.h"
#include "difference.h"
#include "difference.frag.c"

//======================================================================
// difference shader initialization
struct difference *difference_init(struct difference *difference) {
  if (! difference) { return 0; }
  if (! shader_init(&difference->shader, 0, difference_frag)) {
    return 0;
  }
  shader_uniform(difference, texture1);
  shader_uniform(difference, texture2);
  difference->value.texture1 = 0;
  difference->value.texture2 = 1;
  if (! arithmeticmean_init(&difference->arithmeticmean)) { return 0; }
  glGenTextures(2, difference->textures);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, difference->textures[0]);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glBindTexture(GL_TEXTURE_2D, difference->textures[1]);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glDisable(GL_TEXTURE_2D);
  difference->width = 0;
  difference->height = 0;
  difference->snap = 0;
  difference->calc = 0;
  difference->mean = 0;
  return difference;
}

//======================================================================
// difference shader reshape callback
void difference_reshape(struct difference *difference, int w, int h) {
  difference->width  = roundtwo(w);
  difference->height = roundtwo(h);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, difference->textures[0]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F_ARB,
    difference->width, difference->height, 0, GL_RGBA, GL_FLOAT, 0
  );
  glBindTexture(GL_TEXTURE_2D, difference->textures[1]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F_ARB,
    difference->width, difference->height, 0, GL_RGBA, GL_FLOAT, 0
  );
  glDisable(GL_TEXTURE_2D);
  arithmeticmean_reshape(&difference->arithmeticmean, w, h);
}

//======================================================================
// difference shader display callback
void difference_display(
  struct difference *difference, GLuint fbo, GLuint texture
) {
  if (difference->snap) {
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);
    //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
    glFramebufferTexture2DEXT(
      GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D,
      difference->textures[0], 0
    );
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, 1, 0, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glViewport(0, 0, difference->width, difference->height);
    glBegin(GL_QUADS); { glColor4f(1,1,1,1);
      glTexCoord2f(0, 0); glVertex2f(0, 0);
      glTexCoord2f(1, 0); glVertex2f(1, 0);
      glTexCoord2f(1, 1); glVertex2f(1, 1);
      glTexCoord2f(0, 1); glVertex2f(0, 1);
    } glEnd();
    //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
    glDisable(GL_TEXTURE_2D);
    difference->snap = 0;
  } else if (difference->calc) {
    glEnable(GL_TEXTURE_2D);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, texture);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, difference->textures[0]);
    //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
    glFramebufferTexture2DEXT(
      GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D,
      difference->textures[1], 0
    );
    glUseProgramObjectARB(difference->shader.program);
    shader_updatei(difference, texture1);
    shader_updatei(difference, texture2);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, 1, 0, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glViewport(0, 0, difference->width, difference->height);
    glBegin(GL_QUADS); { glColor4f(1,1,1,1);
      glTexCoord2f(0, 0); glVertex2f(0, 0);
      glTexCoord2f(1, 0); glVertex2f(1, 0);
      glTexCoord2f(1, 1); glVertex2f(1, 1);
      glTexCoord2f(0, 1); glVertex2f(0, 1);
    } glEnd();
    glUseProgramObjectARB(0);
    //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
    glDisable(GL_TEXTURE_2D);
    arithmeticmean_display(
      &difference->arithmeticmean, fbo, difference->textures[1]
    );
    difference->mean = difference->arithmeticmean.result[0];
    difference->calc = 0;
  }
}

//======================================================================
// difference shader idle callback
void difference_idle(struct difference *difference) {
  arithmeticmean_idle(&difference->arithmeticmean);
}

// EOF
