/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2017 Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Based on: fire-2.2/FeatureExtractors/tamurafeature.cpp
Homepage: http://www-i6.informatik.rwth-aachen.de/~deselaers/fire/
------------------------------------------------------------------------
Tumara Feature Extraction
===================================================================== */

#define _DEFAULT_SOURCE 1

#include <assert.h>
#include <math.h>
#include "tamura.h"

float tamura_coarseness(
  struct tamura *tamura, struct image *image, int channel
);
float tamura_directionality(
  struct tamura *tamura, struct image *image, int channel
);
float tamura_contrast(
  struct tamura *tamura, struct image *image, int channel
);
float tamura_localmean(struct tamura *tamura, int x, int y, int k);
void tamura_runningsum(
  struct tamura *tamura, struct image *image, int channel
);

struct tamura *tamura_init(
  struct tamura *tamura, int width, int height
) {
  assert(tamura);
  assert(width > 0);
  assert(height > 0);
  tamura->width    = width;
  tamura->height   = height;
  tamura->Rsum  = image_alloc(width, height, 1);  assert(tamura->Rsum);
  tamura->Ak    = image_alloc(width, height, 5);  assert(tamura->Ak);
  tamura->Ekh   = image_alloc(width, height, 5);  assert(tamura->Ekh);
  tamura->Ekv   = image_alloc(width, height, 5);  assert(tamura->Ekv);
  tamura->Sbest = image_alloc(width, height, 1);  assert(tamura->Sbest);
  return tamura;
}

void tamura_calculate(
  struct tamura *tamura,
  struct tamura_features *features,
  struct image *image
) {
  assert(tamura);
  assert(features);
  assert(image);
  assert(tamura->width  == image->width);
  assert(tamura->height == image->height);
  assert(2              <= image->channels);
  for (int c = 0; c < 2; ++c) {
    features[c].coarseness     = tamura_coarseness    (tamura, image, c);
    features[c].contrast       = tamura_contrast      (tamura, image, c);
    features[c].directionality = tamura_directionality(tamura, image, c);
  }
}

void tamura_runningsum(
  struct tamura *tamura, struct image *image, int channel
) {
  struct image *r = tamura->Rsum;
  float l, u, ul;
  for (int y = 0; y < tamura->height; ++y) {
    for (int x = 0; x < tamura->width; ++x) {
      l   = x      ? I(r,x-1,y,  0) : 0;
      u   = y      ? I(r,x,  y-1,0) : 0;
      ul  = x && y ? I(r,x-1,y-1,0) : 0;
      I(r,x,y,0) = I(image,x,y,channel) + l + u + ul;
    }
  }
}

float tamura_localmean(struct tamura *tamura, int x, int y, int k) { // FIXME handle wrap-around more excitingly
  struct image *r = tamura->Rsum;
  int k2 = k/2;
  int x0 = x - k2 - 1;
  int x1 = x + k2 - 1;
  int y0 = y - k2 - 1;
  int y1 = y + k2 - 1;
  if (x0 < 0) x0 = 0;
  if (y0 < 0) y0 = 0;
  if (x1 >= tamura->width ) x1 = tamura->width  - 1;
  if (y1 >= tamura->height) y1 = tamura->height - 1;
  int count = (x1 - x0 + 1) * (y1 - y0 + 1);
  float l  = x0 > 0           ? I(r,x0-1,y1,  0) : 0;
  float t  = y0 > 0           ? I(r,x1,  y0-1,0) : 0;
  float tl = x0 > 0 && y0 > 0 ? I(r,x0-1,y0-1,0) : 0;
  float b  =                    I(r,x1,  y1,  0);
  return (b - l - t + tl) / count;
}

float tamura_contrast(
  struct tamura *tamura, struct image *image, int channel
) {
  float size = 121.0;
  float contrast = 0;
  for (int y = 0; y < tamura->height; ++y) {
    for (int x = 0; x < tamura->width; ++x) {
      float mean = 0;
      float sigma = 0;
      float kurtosis = 0;
      for (int j = y - 5; j <= y + 5; ++j) {
        int jj = j;
        while (jj <  0             ) { jj += tamura->height; }
        while (jj >= tamura->height) { jj -= tamura->height; }
        for (int i = x - 5; i <= x + 5; ++i) {
          int ii = i;
          while (ii <  0            ) { ii += tamura->width; }
          while (ii >= tamura->width) { ii -= tamura->width; }
          float t = I(image,ii,jj,channel);
          mean += t;
          sigma += t*t;
        }
      }
      mean /= size;
      sigma /= size;
      sigma -= mean * mean;
      for (int j = y - 5; j <= y + 5; ++j) {
        int jj = j;
        while (jj <  0             ) { jj += tamura->height; }
        while (jj >= tamura->height) { jj -= tamura->height; }
        for (int i = x - 5; i <= x + 5; ++i) {
          int ii = i;
          while (ii <  0            ) { ii += tamura->width; }
          while (ii >= tamura->width) { ii -= tamura->width; }
          float t = I(image,ii,jj,channel) - mean;
          t *= t;
          t *= t;
          kurtosis += t;
        }
      }
      kurtosis /= size;
      if (kurtosis > 0.000001) {
        contrast += sigma / sqrtf(sqrtf(kurtosis));
      }
    }
  }
  return contrast / (tamura->width * tamura->height);
}

float tamura_directionality(
  struct tamura *tamura, struct image *image, int channel
) {
  const float mh[3][3] = { { 1, 2, 1 }, { 0, 0, 0 }, { -1, -2, -1 } };
  const float mv[3][3] = { { 1, 0, -1 }, { 2, 0, -2 }, { 1, 0, -1 } };
  float phi = 0;
  for (int y = 0; y < tamura->height; ++y) {
    for (int x = 0; x < tamura->width; ++x) {
      float v = 0, h = 0;
      for (int j = 0; j < 3; ++j) {
        int jj = y + j - 1;
        while (jj <  0             ) { jj += tamura->height; }
        while (jj >= tamura->height) { jj -= tamura->height; }
        for (int i = 0; i < 3; ++i) {
          int ii = x + i - 1;
          while (ii <  0            ) { ii += tamura->width; }
          while (ii >= tamura->width) { ii -= tamura->width; }
          float t = I(image,ii,jj,channel);
          h += mh[i][j] * t;
          v += mv[i][j] * t;
        }
      }
      phi += sqrtf(0.5f*(h*h+v*v));
      //if (h != 0) { phi += atanf(v/h) + M_PI/2.0 + 0.001; } // FIXME ?
    }
  }
  return phi / (tamura->width * tamura->height);
}

float tamura_coarseness(
  struct tamura *tamura, struct image *image, int channel
) {
  tamura_runningsum(tamura, image, channel);
  //step 1
  int lenOfk=1;
  for(int k=1;k<=5;++k) {
    lenOfk*=2;
    for(int y=0;y<tamura->height;++y) {
      for(int x=0;x<tamura->width;++x) {
        I(tamura->Ak,x,y,k-1) = tamura_localmean(tamura,x,y,lenOfk);
      }
    }
  }
  //step 2
  lenOfk=1;
  for(int k=1;k<=5;++k) {
    int k2=lenOfk;
    lenOfk*=2;
    for(int y=0;y<tamura->height;++y) {
      for(int x=0;x<tamura->width;++x) {
        int posx1=x+k2;
        int posx2=x-k2;
        int posy1=y+k2;
        int posy2=y-k2;
        if(posx1<tamura->width && posx2>=0) {
          I(tamura->Ekh,x,y,k-1)=fabsf(I(tamura->Ak,posx1,y,k-1)-I(tamura->Ak,posx2,y,k-1));
        } else {
          I(tamura->Ekh,x,y,k-1)=0;
        }
        if(posy1<tamura->height && posy2>=0) {
          I(tamura->Ekv,x,y,k-1)=fabsf(I(tamura->Ak,x,posy1,k-1)-I(tamura->Ak,x,posy2,k-1));
        } else {
          I(tamura->Ekv,x,y,k-1)=0;
        }
      }
    }
  }
  float coarseness=0.0;
  //step3
  for(int y=0;y<tamura->height;++y) {
    for(int x=0;x<tamura->width;++x) {
      float maxE=0;
      int maxk=0;
      for(int k=1;k<=5;++k) {
        if(I(tamura->Ekh,x,y,k-1)>maxE) {
          maxE=I(tamura->Ekh,x,y,k-1);
          maxk=k;
        }
        if(I(tamura->Ekv,x,y,k-1)>maxE) {
          maxE=I(tamura->Ekv,x,y,k-1);
          maxk=k;
        }
      }
//      I(tamura->Sbest,x,y,0)=maxk;
      coarseness+=maxk;
    }
  }
  return coarseness / ((tamura->width - 32) * (tamura->height - 32));
}

/*
vector<uint> histogramImageBinImage(const ImageFeature &image) {
  vector<uint> result(image.xsize()*image.ysize());
  vector<double> t(3,0);
  
  vector<uint> steps(3,8);
  HistogramFeature tmp(steps);
  tmp.min()[0]=0;    tmp.max()[0]=1.0;
  tmp.min()[1]=0;    tmp.max()[1]=1.0;
  tmp.min()[2]=0;    tmp.max()[2]=1.0;
  
  tmp.initStepsize();
  
  for(uint x=0;x<image.xsize();++x) {
    for(uint y=0;y<image.ysize();++y) {
      for(uint z=0;z<3;++z) {t[z]=image(x,y,z);}
      result[y*image.xsize()+x]=tmp.posToBin(tmp.pointToPos(t));
    }
  }
  
  return result;
}


HistogramFeature histogramize(const ImageFeature image,
                              const vector<uint> &bins, 
                              const uint left, 
                              const uint top, 
                              const uint right, 
                              const uint bottom) {
  vector<uint> steps(3,8);
  HistogramFeature result(steps);
  result.min()[0]=0;    result.max()[0]=1.0;
  result.min()[1]=0;    result.max()[1]=1.0;
  result.min()[2]=0;    result.max()[2]=1.0;
  result.initStepsize();
  
  uint xsize=image.xsize();
  
  for(uint x=left;x<right;++x) {
    for(uint y=top;y<bottom;++y) {
      //      DBG(10) << y << " " << x << " "<< y*xsize+x << " " << bins[y*xsize+x] << endl;
      result.feedbin(bins[y*xsize+x]);
    }
  }
  
  return result;
}


HistogramFeature histogramize(const ImageFeature &image) {
  // min -> max: 0,0,0 -> pi,5,128
  // steps: 8,8,8
  
  vector<uint> steps(3,8);
  HistogramFeature result(steps);
  result.min()[0]=0;    result.max()[0]=1.0;
  result.min()[1]=0;    result.max()[1]=1.0;
  result.min()[2]=0;    result.max()[2]=1.0;
  result.initStepsize();

  vector<double> tmp(3,0.0);

  for(uint x=0;x<image.xsize();++x) {
    for(uint y=0;y<image.ysize();++y) {
      for(uint z=0;z<3;++z) {tmp[z]=image(x,y,z);}
      result.feed(tmp);
    }
  }
  return result;
}
*/
