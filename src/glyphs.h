/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Sequencer glyphs
===================================================================== */

#ifndef GLYPHS_H
#define GLYPHS_H 1

#include <GL/glew.h>

struct glyphs {
  GLuint textures[4];
};

struct glyphs *glyphs_init(struct glyphs *glyphs);

#define sequence_curve_less 0
#define sequence_curve_more 1
#define sequence_speed_less 2
#define sequence_speed_more 3

#endif
