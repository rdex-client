/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Sequence vertex shader
===================================================================== */

attribute vec2 myspin0;
//attribute float mystep0;
attribute float mycursor0;
attribute float myactive0;
attribute float mypitch0;

varying vec2 myspin2;
//varying float mystep2;
varying float mycursor2;
varying float myactive2;
varying float mypitch2;

void main(void) {
  gl_TexCoord[0] = gl_MultiTexCoord0;
  gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
  myspin2 = myspin0;
//  mystep2 = mystep0;
  mycursor2 = mycursor0;
  myactive2 = myactive0;
  mypitch2 = mypitch0;
}
