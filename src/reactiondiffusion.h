/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Reaction Diffusion Shader
===================================================================== */

#ifndef REACTIONDIFFUSION_H
#define REACTIONDIFFUSION_H 1

#include "shader.h"

//======================================================================
// reaction diffusion shader data
struct reactiondiffusion { struct shader shader;
  struct { GLint texture; GLint dx, dy, ru, rv, f, k; } uniform;
  struct { int   texture; float dx, dy, ru, rv, f, k; } value;
  GLuint textures[2];
  GLuint texture;
  int buffer;
  int width;
  int height;
  int seed;
  int frame;
  double spawnangle;
};

//======================================================================
// prototypes
void reactiondiffusion_randomize(
  struct reactiondiffusion *reactiondiffusion
);
void reactiondiffusion_near(
  struct reactiondiffusion *reactiondiffusion, float ru, float rv, float f, float k, float d
);
void reactiondiffusion_perturb(
  struct reactiondiffusion *reactiondiffusion
);
struct reactiondiffusion *reactiondiffusion_init(
  struct reactiondiffusion *reactiondiffusion
);
void reactiondiffusion_reshape(
  struct reactiondiffusion *reactiondiffusion, int w, int h
);
void reactiondiffusion_display(
  struct reactiondiffusion *reactiondiffusion, GLuint fbo
);
void reactiondiffusion_idle(
  struct reactiondiffusion *reactiondiffusion
);

#endif
