/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
World sphere shader
===================================================================== */

#ifndef WORLDSPHERE_H
#define WORLDSPHERE_H 1

#include "shader.h"

//======================================================================
// borderwindow shader data
struct worldsphere { struct shader shader;
  struct { GLint texture; GLint rot;    GLint size;    } uniform;
  struct { int   texture; float rot[2]; float size[2]; } value;
  float delta[2];
};

//======================================================================
// prototypes
struct worldsphere *worldsphere_init(struct worldsphere *worldsphere);
void worldsphere_use(struct worldsphere *worldsphere);
void worldsphere_idle(struct worldsphere *worldsphere);

#endif
