/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2009,2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Main Module
===================================================================== */

#ifndef RDEX_H
#define RDEX_H 1

#include <time.h>
#include <GL/glew.h>
#include <GL/glut.h>

#include "reactiondiffusion.h"
#include "copysquare.h"
#include "arithmeticmean.h"
#include "numericerror.h"
#include "difference.h"
#include "falsecolour.h"
#include "library.h"
#include "screenshot.h"
#include "audio.h"
#include "sequence.h"
#include "intro.h"
#include "timeline.h"
#include "text.h"

//======================================================================
// configuration
#define rdex_tex_size 256
// FIXME: breakage will occur when rdex_count % rdex_overdrive != 0
// FIXME: check audio when audioHz % videoHz != 0
// FIXME: check audio when overdrive % (audioHz / videoHz) != 0
#define rdex_count 96
// FIXME optimal overdrive depends on sample rate!
//   14   * 25 * 126 = 44100
//   16   * 25 * 120 = 48000
#define rdex_overdrive (AUDIO_COUNT * 2)
//#define rdex_videoHz 33

enum rdex_mode {
  mode_random = 100, mode_textual, mode_sequence
};

//======================================================================
// main module data
struct rdex {
  struct reactiondiffusion reactiondiffusion;
  struct copysquare        copysquare;
  struct arithmeticmean    arithmeticmean;
  struct numericerror      numericerror;
  struct difference        difference;
  struct falsecolour       falsecolour;
  struct library           library;
  struct screenshot        screenshot;
  struct audio             audio;
//  struct sequence          sequence;
  struct intro             intro;
  struct timeline          timeline;
  struct text              text;
  GLuint fbo;
  time_t starttime;
  struct timespec clock0, clock1;
  unsigned int frame;
  unsigned int framedrops;
  unsigned int species;
  unsigned int uniform;
  unsigned int stable;
  unsigned int dynamic;
  unsigned int erratic;
  int done;
  int fullscreen;
  char *behaviour;
  enum rdex_mode mode;
  int nrt;
  // timing statistics
  double sum1, sumdt, sumdt2;
};

//======================================================================
// prototypes
int rdex_init(void);
void rdex_reshape(int w, int h);
void rdex_display(void);
void rdex_atexit(void);
void rdex_timer(int v);
//void rdex_idle();
void rdex_pmotion(int x, int y);
void rdex_amotion(int x, int y);
void rdex_mouse(int button, int state, int x, int y);
void rdex_keynormal(unsigned char key, int x, int y);
void rdex_keyspecial(int key, int x, int y);

#endif
