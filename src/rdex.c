/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2009,2010,2017,2019  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Main Module
===================================================================== */
#define _DEFAULT_SOURCE

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "rdex.h"

//======================================================================
// main module global mutable state
static struct rdex rdex;

//======================================================================
// main module initialization
int rdex_init() {
  int nrt = 0 != getenv("RDEX_RENDER");
  memset(&rdex, 0, sizeof(rdex));
  glGenFramebuffersEXT(1, &rdex.fbo);
  glClampColorARB( GL_CLAMP_VERTEX_COLOR_ARB , GL_FALSE );
  glClampColorARB( GL_CLAMP_FRAGMENT_COLOR_ARB, GL_FALSE );
  glClampColorARB( GL_CLAMP_READ_COLOR_ARB , GL_FALSE );
  if (! reactiondiffusion_init(&rdex.reactiondiffusion)) { return 0; }
  if (! copysquare_init       (&rdex.copysquare       )) { return 0; }
  if (! arithmeticmean_init   (&rdex.arithmeticmean   )) { return 0; }
  if (! numericerror_init     (&rdex.numericerror     )) { return 0; }
  if (! difference_init       (&rdex.difference       )) { return 0; }
  if (! falsecolour_init      (&rdex.falsecolour      )) { return 0; }
  if (! library_init          (&rdex.library, rdex.fbo)) { return 0; }
  if (! screenshot_init       (&rdex.screenshot, nrt  )) { return 0; }
  if (! audio_init            (&rdex.audio, nrt       )) { return 0; }
//  if (! sequence_init         (&rdex.sequence         )) { return 0; }
  if (! intro_init            (&rdex.intro            )) { return 0; }
  if (! timeline_init         (&rdex.timeline         )) { return 0; }
  if (! text_init             (&rdex.text             )) { return 0; }
  rdex.starttime = 0;
  rdex.fullscreen = 0;
//  reactiondiffusion_spawn(&rdex.reactiondiffusion);
  rdex.done = 0;
  rdex.behaviour = "none";
#if 1
  rdex.mode = mode_random;
#else
  rdex.mode = mode_textual;
#endif
  rdex.nrt = nrt;
  return 1;
}

//======================================================================
// main module reshape callback
void rdex_reshape(int w, int h) {
  reactiondiffusion_reshape(&rdex.reactiondiffusion, rdex_tex_size, rdex_tex_size);
  copysquare_reshape       (&rdex.copysquare,        rdex_tex_size, rdex_tex_size);
  arithmeticmean_reshape   (&rdex.arithmeticmean,    rdex_tex_size, rdex_tex_size);
  numericerror_reshape     (&rdex.numericerror,      rdex_tex_size, rdex_tex_size);
  difference_reshape       (&rdex.difference,        rdex_tex_size, rdex_tex_size);
  falsecolour_reshape      (&rdex.falsecolour,       rdex_tex_size, rdex_tex_size);
  library_reshape          (&rdex.library,           w, h);
  screenshot_reshape       (&rdex.screenshot,        w, h);
//  sequence_reshape         (&rdex.sequence,          w, h);
  intro_reshape            (&rdex.intro,             w, h);
  text_reshape             (&rdex.text,              w, h);
}

//======================================================================
// main module display callback
void rdex_display() {
  if (rdex.timeline.state1 != rdex.timeline.state) {
    switch (rdex.timeline.state) {
      case timeline_intro1:
        intro_start(&rdex.intro);
        audio_start(&rdex.audio);
        break;
      case timeline_intro2:
        rdex.library.phase = 0;
        rdex.library.state = library_state_intro;
        break;
      case timeline_main:
        rdex.library.phase = 0;
        rdex.library.state = library_state_active;
        break;
      case timeline_outro:
        rdex.library.phase = 0;
        rdex.library.state = library_state_outro;
        audio_stop(&rdex.audio);
        break;
      case timeline_done:
        rdex.library.phase = 0;
        rdex.library.state = library_state_done;
        break;
      case timeline_idle:
        break;
    }
  }
  timeline_advance(&rdex.timeline);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, rdex.fbo);
  screenshot_display1      (&rdex.screenshot);
  for (int g = 0; g < rdex_overdrive; ++g) {
    reactiondiffusion_display(&rdex.reactiondiffusion, rdex.fbo);
    if (g % 2 == 0) {
      audio_display1       (&rdex.audio,             rdex.fbo, rdex.reactiondiffusion.texture, g / 2);
    }
  }
  copysquare_display       (&rdex.copysquare,        rdex.fbo, rdex.reactiondiffusion.texture);
  arithmeticmean_display   (&rdex.arithmeticmean,    rdex.fbo, rdex.copysquare       .texture);
  numericerror_display     (&rdex.numericerror,      rdex.fbo, rdex.reactiondiffusion.texture);
  difference_display       (&rdex.difference,        rdex.fbo, rdex.reactiondiffusion.texture);
  falsecolour_display      (&rdex.falsecolour,       rdex.fbo, rdex.reactiondiffusion.texture);
  library_display_save(
    &rdex.library,
    rdex.fbo,
    rdex.falsecolour.texture,
    rdex.reactiondiffusion.texture,
    rdex.reactiondiffusion.value.ru,
    rdex.reactiondiffusion.value.rv,
    rdex.reactiondiffusion.value.f,
    rdex.reactiondiffusion.value.k,
    (rdex.reactiondiffusion.frame / (double) (1 << 14)) *
    (rdex.reactiondiffusion.frame / (double) (1 << 14)),
    rdex.behaviour
  );
  library_display          (&rdex.library,           rdex.fbo, rdex.falsecolour.texture);
  if (rdex.library.picked != -1) {
    rdex.reactiondiffusion.value.ru = rdex.library.array[rdex.library.picked].v[0];
    rdex.reactiondiffusion.value.rv = rdex.library.array[rdex.library.picked].v[1];
    rdex.reactiondiffusion.value.f  = rdex.library.array[rdex.library.picked].v[2];
    rdex.reactiondiffusion.value.k  = rdex.library.array[rdex.library.picked].v[3];
    rdex.library.target.v[0] = rdex.library.array[rdex.library.picked].v[0];
    rdex.library.target.v[1] = rdex.library.array[rdex.library.picked].v[1];
    rdex.library.target.v[2] = rdex.library.array[rdex.library.picked].v[2];
    rdex.library.target.v[3] = rdex.library.array[rdex.library.picked].v[3];
    rdex.library.picked = -1;
  }

//  sequence_display         (&rdex.sequence, &rdex.library, &rdex.reactiondiffusion, &rdex.audio);
  text_display             (&rdex.text);
  intro_display            (&rdex.intro, 0.04); /* FIXME hardcoded frame rate */
  glutSwapBuffers();
  audio_display2           (&rdex.audio);
  screenshot_display2      (&rdex.screenshot);
  rdex.frame += 1;
  int reseed = rdex.done;
  if (rdex.done) {
    float ru = 0, rv = 0, f = 0, k = 0, ru2, rv2, f2, k2;
    switch (rdex.mode) {
    case mode_random: {
      int i = 0;
      float randomness = 1e6; //16.0;
      while (! library_pick(&rdex.library, &ru2, &rv2, &f2, &k2, randomness)) {
        ru += ru2;
        rv += rv2;
        f  += f2;
        k  += k2;
        i  += 1;
//      randomness = 2.0;
      }
      if (i == 0) {
        reactiondiffusion_randomize(&rdex.reactiondiffusion);
      } else {
        ru /= i;
        rv /= i;
        f  /= i;
        k  /= i;
        reactiondiffusion_near(&rdex.reactiondiffusion, ru, rv, f, k, 0.01);
      }
      rdex.audio.sphase = 0;
    } break;
    case mode_textual: {
      if (reseed) { rdex.reactiondiffusion.frame = 0; }
    } break;
    case mode_sequence: {
      if (reseed) { rdex.reactiondiffusion.frame = 0; }
    /*
      if (reseed) {
      float randomness = 1.1;
      if (! library_pick(&rdex.library, &ru, &rv, &f, &k, randomness)) {
        rdex.loop[rdex.loopindex].v[0] = ru;
        rdex.loop[rdex.loopindex].v[1] = rv;
        rdex.loop[rdex.loopindex].v[2] = f;
        rdex.loop[rdex.loopindex].v[3] = k;
      } else { //if (randomness * rand() / (double) RAND_MAX < 1.0) {
        rdex.loop[rdex.loopindex].v[0] = 0.3 * rand() / (double) RAND_MAX;
        rdex.loop[rdex.loopindex].v[1] = 0.3 * rand() / (double) RAND_MAX;
        rdex.loop[rdex.loopindex].v[2] = 0.1 * rand() / (double) RAND_MAX;
        rdex.loop[rdex.loopindex].v[3] = 0.1 * rand() / (double) RAND_MAX;
      }
    }
      ru = rdex.loop[rdex.loopindex].v[0];
      rv = rdex.loop[rdex.loopindex].v[1];
      f  = rdex.loop[rdex.loopindex].v[2];
      k  = rdex.loop[rdex.loopindex].v[3];
      if (reseed) {
        reactiondiffusion_near(&rdex.reactiondiffusion, ru, rv, f, k, 0.001);
      } else {
        rdex.reactiondiffusion.value.ru = ru;
        rdex.reactiondiffusion.value.rv = rv;
        rdex.reactiondiffusion.value.f  = f;
        rdex.reactiondiffusion.value.k  = k;
      }
      fprintf(stderr, "L\t");
    */
    } break;
    }
    rdex.library.worldsphere.delta[0] = (rand() / (double) RAND_MAX - 0.5) / 64.0;
    rdex.library.worldsphere.delta[1] = (rand() / (double) RAND_MAX - 0.5) / 64.0;
    rdex.done = 0;
  }
  glutReportErrors();
  if (rdex.library.state == library_state_active)
    rdex.audio.state = audio_state_rolling;
  if (rdex.timeline.state == timeline_idle)
    timeline_start(&rdex.timeline);
}

//======================================================================
// main module exit callback
void rdex_atexit(void) {
// /* not safe to call at exit it seems... */
//  if (glutGameModeGet(GLUT_GAME_MODE_ACTIVE)) {
//    glutLeaveGameMode();
//  }
  audio_atexit(&rdex.audio);
  double timeelapsed = (double) time(NULL) - (double) rdex.starttime;
  fprintf(stderr, "\n\n--------------------------------------------------------------------------\n");
  fprintf(stderr, "------------------------------- statistics -------------------------------\n");
  fprintf(stderr, "--------------------------------------------------------------------------\n");
  fprintf(stderr, "%10d seconds elapsed (+/- 1)\n", (int) timeelapsed);
  fprintf(stderr, "%10d frames rendered (%f fps)\n", rdex.frame, rdex.frame / timeelapsed);
  fprintf(stderr, "%10d frames dropped\n", (int) (25 * timeelapsed - rdex.frame));
  fprintf(stderr, "%10d species analyzed (%f sps)\n", rdex.species, rdex.species / timeelapsed);
  fprintf(stderr, "        %6.2f%% uniform (%d)\n", 100.0 * rdex.uniform / (double) rdex.species, rdex.uniform);
  fprintf(stderr, "        %6.2f%% stable  (%d)\n", 100.0 * rdex.stable  / (double) rdex.species, rdex.stable);
  fprintf(stderr, "        %6.2f%% dynamic (%d)\n", 100.0 * rdex.dynamic / (double) rdex.species, rdex.dynamic);
  fprintf(stderr, "        %6.2f%% erratic (%d)\n", 100.0 * rdex.erratic / (double) rdex.species, rdex.erratic);
  fprintf(stderr, "--------------------------------------------------------------------------\n");
  fprintf(stderr, "average frame time: %f seconds\n", rdex.sumdt / rdex.sum1);
  fprintf(stderr, "standard deviation: %f\n", sqrt( rdex.sumdt2 / rdex.sum1 - pow(rdex.sumdt / rdex.sum1, 2) ));
  fprintf(stderr, "--------------------------------------------------------------------------\n");
}

//======================================================================
// main module timer callback
//void rdex_idle() {
void rdex_timer(int v) {
  
  // initialize
  if (rdex.starttime == 0) {
    rdex.starttime = time(NULL);
    rdex.frame = 0;
    rdex.sum1 = 0;
    rdex.sumdt = 0;
    rdex.sumdt2 = 0;
    atexit(rdex_atexit);
    clock_gettime(CLOCK_REALTIME, &rdex.clock0);
  }

  // check if there are enough free buffers to render a frame
  int r = audio_read;
  int w = audio_write;
  if (rdex.nrt || (w < r && w + AUDIO_COUNT < r) || ( r < w && w + AUDIO_COUNT < r + AUDIO_BUFFERS)) {

  for (int g = 0; g < rdex_overdrive; ++g) {
    reactiondiffusion_idle(&rdex.reactiondiffusion);
  }
  copysquare_idle       (&rdex.copysquare);
  arithmeticmean_idle   (&rdex.arithmeticmean);
  numericerror_idle     (&rdex.numericerror);
  difference_idle       (&rdex.difference);
  falsecolour_idle      (&rdex.falsecolour);
  library_idle          (&rdex.library);
  screenshot_idle       (&rdex.screenshot);
  if (rdex.reactiondiffusion.frame > (rdex_count*1) && rdex.arithmeticmean.stddev < 0.01) {
//    if (rdex.species % 32 == 0) {
//      if (rdex.species) { fputc('|', stderr); }
//      fprintf(stderr, "\n%08x|", rdex.species);
//    }
    rdex.done = 1; //(rdex.mode == mode_random);
    rdex.species += 1;
    rdex.uniform += 1;
    rdex.behaviour = "uniform";
//    rdex.library.snap = 1;
    //fprintf(stderr, "_\t%d", rdex.reactiondiffusion.frame);
//    reactiondiffusion_randomize(&rdex.reactiondiffusion);
  } else if (rdex.reactiondiffusion.frame % (rdex_count*2) == (rdex_count*1)) {
    rdex.numericerror.calc = 1;
  } else if (rdex.reactiondiffusion.frame % (rdex_count*2) == (rdex_count*1) + rdex_overdrive) {
    if (rdex.numericerror.mean > 0.2) {
//      if (rdex.species % 32 == 0) {
//        if (rdex.species) { fputc('|', stderr); }
//        fprintf(stderr, "\n%08x|", rdex.species);
//      }
      rdex.done = 1;
      rdex.species += 1;
      rdex.erratic += 1;
      //fprintf(stderr, ".\t%d", rdex.reactiondiffusion.frame);
      //reactiondiffusion_randomize(&rdex.reactiondiffusion);
    }
  } else if (rdex.reactiondiffusion.frame % (rdex_count*3) == (rdex_count*1)) {
    rdex.difference.snap = 1;
  } else if (rdex.reactiondiffusion.frame % (rdex_count*3) == (rdex_count*2)) {
    rdex.difference.calc = 1;
  } else if (rdex.reactiondiffusion.frame % (rdex_count*3) == (rdex_count*2) + rdex_overdrive) {
    if (rdex.difference.mean < 0.011) {
      rdex.done = (rdex.mode == mode_random);
      rdex.species += 1;
      rdex.stable += 1;
      rdex.behaviour = "stable";
      rdex.library.snap = (rdex.mode == mode_random);
      //fprintf(stderr, "o\t%d", rdex.reactiondiffusion.frame);
    } else if (rdex.reactiondiffusion.frame > 16000 && rdex.mode == mode_random) {
      rdex.done = 1;
      rdex.species += 1;
      rdex.dynamic += 1;
      rdex.behaviour = "dynamic";
      rdex.library.snap = (rdex.mode == mode_random);
      //fprintf(stderr, "*\t%d", rdex.reactiondiffusion.frame);
    }
  }
/* else if (rdex.reactiondiffusion.frame == 14400) {
    if (rdex.species % 32 == 0) {
      if (rdex.species) { fputc('|', stderr); }
      fprintf(stderr, "\n%08x|", rdex.species);
    }
    rdex.species += 1;
    int which = rdex.difference.mean < 0.01;
    if (which) { rdex.stable += 1; } else { rdex.dynamic += 1; }
    fputc(which ? 'o' : '*', stderr);
    //reactiondiffusion_perturb(&rdex.reactiondiffusion);
    reactiondiffusion_randomize(&rdex.reactiondiffusion);
  }
*/
  glutPostRedisplay();


  // gather timing statistics
  clock_gettime(CLOCK_REALTIME, &rdex.clock1);
  double dt = (rdex.clock1.tv_sec - rdex.clock0.tv_sec) + 1.0e-9 * (rdex.clock1.tv_nsec - rdex.clock0.tv_nsec);
  rdex.sum1 += 1;
  rdex.sumdt += dt;
  rdex.sumdt2 += dt*dt;
  clock_gettime(CLOCK_REALTIME, &rdex.clock0);

  }

  // sleep for 1ms and check again if jack is ready
  glutTimerFunc(1, rdex_timer, v + 1);

}

void rdex_pmotion(int x, int y) {
  library_pmotion(&rdex.library, x, y);
}

void rdex_amotion(int x, int y) {
  library_amotion(&rdex.library, x, y);
}

void rdex_mouse(int button, int state, int x, int y) {
  library_mouse(&rdex.library, button, state, x, y);
}

void rdex_keynormal(unsigned char key, int x, int y) {
  return;
  if (text_keynormal(&rdex.text, key, x, y)) {
    if (key == ' ') {
      struct libelem *e = &rdex.library.array[rand() % (rdex.library.count - 1)];
      rdex.reactiondiffusion.value.ru = e->v[0];
      rdex.reactiondiffusion.value.rv = e->v[1];
      rdex.reactiondiffusion.value.f  = e->v[2];
      rdex.reactiondiffusion.value.k  = e->v[3];
	}
    reactiondiffusion_perturb(&rdex.reactiondiffusion);
  } else {
//  if (! sequence_keynormal(&rdex.sequence, &rdex.library, key, x, y)) {
	  switch (key) {
		  case 27: // escape
				exit(0);
		    break;
		  case 9: // tab
        timeline_start(&rdex.timeline);
//		    audio_start(&rdex.audio);
//        intro_start(&rdex.intro);
		    break;
		  case 127: // delete
        timeline_stop(&rdex.timeline);
//		    audio_stop(&rdex.audio);
        //outro_start(&rdex.outro);
		    break;
		  default:
		    break;
	  }
  }
}

void rdex_keyspecial(int key, int x, int y) {
return;
//  if (! sequence_keyspecial(&rdex.sequence, &rdex.library, key, x, y)) {
    if (! text_keyspecial(&rdex.text, key, x, y)) {
    switch (key) {
	    default:
	      break;
    }
  }
}

// EOF
