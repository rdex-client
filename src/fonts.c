/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010,2011  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Text bitmap fonts
===================================================================== */

#include <GL/glew.h>
#include "fonts.h"

extern char _binary_font_rgba_start;

struct fonts *fonts_init(struct fonts *fonts) {
  fonts->size = 512;
  fonts->pack = 8;
  glGenTextures(1, &fonts->texture);
  glBindTexture(GL_TEXTURE_2D, fonts->texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, fonts->size, fonts->size, 0, GL_RGBA, GL_UNSIGNED_BYTE, &_binary_font_rgba_start);
  return fonts;
}
