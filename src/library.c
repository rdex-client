/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2009,2010,2011,2019  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Library Module
===================================================================== */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wordexp.h>
#include <math.h>
#include "library.h"
#include "background.h"
#include "rdex.h"

static void library_copy_texture_to_array(struct library *library, int layer) {
  glBindTexture(GL_TEXTURE_2D, library->texture);
  glBindTexture(GL_TEXTURE_2D_ARRAY, library->texture_array);
  glBindBuffer(GL_PIXEL_PACK_BUFFER, library->pbo);
  glBindBuffer(GL_PIXEL_UNPACK_BUFFER, library->pbo);
  for (int level = 0, size = library_tex_size; size > 0; level += 1, size >>= 1) {
    glGetTexImage(GL_TEXTURE_2D, level, GL_RGB, GL_UNSIGNED_BYTE, 0);
    glTexSubImage3D(GL_TEXTURE_2D_ARRAY, level, 0, 0, layer, size, size, 1, GL_RGB, GL_UNSIGNED_BYTE, 0);
  }
  glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
  glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
}

// find index to insert at
int library_index(struct library *library, float score) {
  if (library->count == library_tex_layers) {
    float minscore = score;
    int minindex = -1;
    for (int j = 0; j < library_tex_layers; ++j) {
      if (library->array[j].score <= minscore) {
        minscore = library->array[j].score;
        minindex = j;
      }
    }
    return minindex;
  } else {
    return library->count++;
  }
}

// pick a random point weighted by score
int library_pick(struct library *library, float *ru, float *rv, float *f, float *k, float randomness) {
  double p = randomness * library->score * (rand() / (double) RAND_MAX);
  double s = 0.0;
  for (int i = 0; i < library->count; ++i) {
    s += library->array[i].score;
    if (p <= s) {
      *ru = library->array[i].v[0];
      *rv = library->array[i].v[1];
      *f  = library->array[i].v[2];
      *k  = library->array[i].v[3];
      return 0;
    }
  }
  return 1;
}

struct library *library_init(struct library *library, GLuint fbo) {
  if (!library) { return 0; }
  if (! borderwindow_init(&library->borderwindow)) { return 0; }
  if (! borderwindow_pick_init(&library->borderwindow_pick)) { return 0; }
  if (! worldsphere_init(&library->worldsphere)) { return 0; }
  if (! blur_init(&library->blur)) { return 0; }
  //if (! bloom_init(&library->bloom)) { return 0; }
  library->count = 0;
  library->frame = 0;
  camera_init(&library->camera);
  library->width = 1;
  library->height = 1;
  library->mousex = -1;
  library->mousey = -1;
  library->picked = -1;
  library->hover = -1;
  library->snap = 0;
  library->session = getenv("RDEX_SESSION");
  if (! library->session) { library->session = "."; }
  background_init(&library->background, library->session); //FIXME -- curl global init must be before any threads are created
  { // allocate texture array
    glGenTextures(1, &library->texture_array);
    glBindTexture(GL_TEXTURE_2D_ARRAY, library->texture_array);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    size_t bytes = library_tex_layers * library_tex_size * library_tex_size * 3;
    char *white = malloc(bytes);
    memset(white, 255, bytes);
    glTexImage3D(
      GL_TEXTURE_2D_ARRAY, 0, GL_RGB,
      library_tex_size, library_tex_size, library_tex_layers, 0,
      GL_RGB, GL_UNSIGNED_BYTE, white
    );
    glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
    // allocate pixel buffer
    glGenBuffers(1, &library->pbo);
    glBindBuffer(GL_PIXEL_PACK_BUFFER, library->pbo);
    glBufferData(GL_PIXEL_PACK_BUFFER, library_tex_size * library_tex_size * 4, 0, GL_DYNAMIC_COPY);
    glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
    // allocate texture
    glEnable(GL_TEXTURE_2D);
    glGenTextures(1, &library->texture);
    glBindTexture(GL_TEXTURE_2D, library->texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(
      GL_TEXTURE_2D, 0, GL_RGB,
      library_tex_size, library_tex_size, 0,
      GL_RGB, GL_UNSIGNED_BYTE, white
    );
    glGenerateMipmap(GL_TEXTURE_2D);
    free(white);
  }
  library->state = library_state_idle;
  library->phase = 0;
  library->loaded = 0;
  return library;
}

void library_insert(
  struct library *library,
  GLfloat ru, GLfloat rv, GLfloat f, GLfloat k, int index, GLfloat score
) {
  assert(0 <= index && index < library->count);
  // remove old element's stats
  library->score -= library->array[index].score;
  library->score2 -= library->array[index].score * library->array[index].score;
  library->scorev[0] -= library->array[index].score * library->array[index].v[0];
  library->scorev[1] -= library->array[index].score * library->array[index].v[1];
  library->scorev[2] -= library->array[index].score * library->array[index].v[2];
  library->scorev[3] -= library->array[index].score * library->array[index].v[3];
  library->scorev2[0] -= library->array[index].score * library->array[index].v[0] * library->array[index].v[0];
  library->scorev2[1] -= library->array[index].score * library->array[index].v[1] * library->array[index].v[1];
  library->scorev2[2] -= library->array[index].score * library->array[index].v[2] * library->array[index].v[2];
  library->scorev2[3] -= library->array[index].score * library->array[index].v[3] * library->array[index].v[3];
  // fill element
  library->array[index].score = score;
  library->array[index].v[0] = ru;
  library->array[index].v[1] = rv;
  library->array[index].v[2] = f;
  library->array[index].v[3] = k;
  library->array[index].spin[0] = 0;
  library->array[index].spin[1] = 0;
  library->array[index].dspin[0] = (rand() / (double) RAND_MAX - 0.5) / 32.0;
  library->array[index].dspin[1] = (rand() / (double) RAND_MAX - 0.5) / 32.0;
  // add new element's stats
  library->score += library->array[index].score;
  library->score2 += library->array[index].score * library->array[index].score;
  library->scorev[0] += library->array[index].score * library->array[index].v[0];
  library->scorev[1] += library->array[index].score * library->array[index].v[1];
  library->scorev[2] += library->array[index].score * library->array[index].v[2];
  library->scorev[3] += library->array[index].score * library->array[index].v[3];
  library->scorev2[0] += library->array[index].score * library->array[index].v[0] * library->array[index].v[0];
  library->scorev2[1] += library->array[index].score * library->array[index].v[1] * library->array[index].v[1];
  library->scorev2[2] += library->array[index].score * library->array[index].v[2] * library->array[index].v[2];
  library->scorev2[3] += library->array[index].score * library->array[index].v[3] * library->array[index].v[3];
  // compute stats
  library->stddev[0] = sqrt((library->scorev2[0] * library->score - library->scorev[0] * library->scorev[0]) / (library->score * library->score - library->score2));
  library->stddev[1] = sqrt((library->scorev2[1] * library->score - library->scorev[1] * library->scorev[1]) / (library->score * library->score - library->score2));
  library->stddev[2] = sqrt((library->scorev2[2] * library->score - library->scorev[2] * library->scorev[2]) / (library->score * library->score - library->score2));
  library->stddev[3] = sqrt((library->scorev2[3] * library->score - library->scorev[3] * library->scorev[3]) / (library->score * library->score - library->score2));
}

void library_addimage(struct library *library, GLuint fbo, GLuint t, int index) {
  glEnable(GL_TEXTURE_2D);
  glDisable(GL_DEPTH_TEST);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glViewport(0, 0, library_tex_size, library_tex_size);
  gluOrtho2D(0, 1, 0, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
  // combine 
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, library->texture, 0);
  // packed texture
  glBindTexture(GL_TEXTURE_2D, t);
  glBegin(GL_QUADS); {
    glColor4f(1,1,1,1);
    glTexCoord2f(0, 0); glVertex2f(0, 0);
    glTexCoord2f(1, 0); glVertex2f(1, 0);
    glTexCoord2f(1, 1); glVertex2f(1, 1);
    glTexCoord2f(0, 1); glVertex2f(0, 1);
  } glEnd();
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  glBindTexture(GL_TEXTURE_2D, library->texture);
  glGenerateMipmap(GL_TEXTURE_2D);
  library_copy_texture_to_array(library, index);
  glBindTexture(GL_TEXTURE_2D, 0);
}

int library_load(struct library *library, GLuint fbo) {
  glEnable(GL_TEXTURE_2D);
  /* temporary texture for loading */
  GLuint t;
  glGenTextures(1, &t);
  glBindTexture(GL_TEXTURE_2D, t);
//  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  wordexp_t p;
  const char *pattern2 = "/ru_*__rv_*__f_*__k_*__s_*.ppm";
  int patternlen = strlen(library->session) + strlen(pattern2) + 1;
  char *pattern = malloc(patternlen);
  snprintf(pattern, patternlen, "%s%s", library->session, pattern2);
  if (0 == wordexp(pattern, &p, WRDE_NOCMD | WRDE_SHOWERR | WRDE_UNDEF)) {
    char sru[9]; char srv[9]; char sf[9]; char sk[9]; char ss[9];
    for (int i = 0; i < p.we_wordc; ++i) {
      int l = strlen(p.we_wordv[i]);
      const char *pattern3 = "ru_%8c__rv_%8c__f_%8c__k_%8c__s_%8c.ppm";
      const int expectLen = 3 + 8 + 5 + 8 + 4 + 8 + 4 + 8 + 4 + 8 + 4;
      if (
        l >= expectLen &&
        5 == sscanf(p.we_wordv[i] + l - expectLen, pattern3, sru, srv, sf, sk, ss)
      ) {
        sru[8] = '\0'; srv[8] = '\0'; sf[8] = '\0'; sk[8] = '\0'; ss[8] = '\0';
        double ru, rv, f, k, s;
        int bsize = rdex_tex_size * rdex_tex_size * 3;
        char buffer[rdex_tex_size * rdex_tex_size * 3];
        if (1 != sscanf(sru, "%lg", &ru)) { break; }
        if (1 != sscanf(srv, "%lg", &rv)) { break; }
        if (1 != sscanf(sf,  "%lg", &f))  { break; }
        if (1 != sscanf(sk,  "%lg", &k))  { break; }
        if (1 != sscanf(ss,  "%lg", &s))  { break; }
        FILE *image = fopen(p.we_wordv[i], "rb");
        if (!image) { break; }
        fseek(image, -bsize, SEEK_END);
        if (fread(buffer, bsize, 1, image) != 1) {
          fprintf(stderr, "warning: failed to read image?\n");
        }
        fclose(image);
        glBindTexture(GL_TEXTURE_2D, t);
        glTexImage2D(
          GL_TEXTURE_2D, 0, GL_RGB,
          rdex_tex_size, rdex_tex_size, 0,
          GL_RGB, GL_UNSIGNED_BYTE, buffer
        );
        int index = library_index(library, s);
        if (index >= 0) {
          library_addimage(library, fbo, t, index);
          library_insert(library, ru, rv, f, k, index, s);
        }
      }
    }
    wordfree(&p);
  }
  free(pattern);
  glDeleteTextures(1, &t);
  glDisable(GL_TEXTURE_2D);
  return 1;
}

void library_display_save(
  struct library *library, GLuint fbo, GLuint texture, GLuint rawtexture,
  double ru, double rv, double f, double k, double score, const char *behaviour
) {
  library->target.v[0] = ru;
  library->target.v[1] = rv;
  library->target.v[2] = f;
  library->target.v[3] = k;
  // capture to library
  if (! library->snap) { return; }
  library->snap = 0;
  int index = library_index(library, score);
  if (index < 0) {
    return;
  }
  // downscale part of the original texture into the new texture
  glEnable(GL_TEXTURE_2D);
  library_addimage(library, fbo, texture, index);
/*
  // copy thumbnail texture to main memory as 1-byte data
  char *thumb_ppm = malloc(library_tex_size * library_tex_size * 3 + 1024);
  int thumb_ppm_bytes;
  thumb_ppm_bytes = snprintf(thumb_ppm, 1000, "P6\n%d %d 255\n", library_tex_size, library_tex_size);
  glReadPixels((index % library_pack) * library_tex_size, (index / library_pack) * library_tex_size, library_tex_size, library_tex_size, GL_RGB, GL_UNSIGNED_BYTE, thumb_ppm + thumb_ppm_bytes);
  thumb_ppm_bytes += library_tex_size * library_tex_size * 3;
*/
  //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  // copy original texture to main memory as 1-byte data
  char *full_ppm = malloc(rdex_tex_size * rdex_tex_size * 3 + 1024);
  int full_ppm_bytes;
  full_ppm_bytes = snprintf(full_ppm, 1000, "P6\n%d %d 255\n", rdex_tex_size, rdex_tex_size);
  glBindTexture(GL_TEXTURE_2D, texture);
  glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_UNSIGNED_BYTE, full_ppm + full_ppm_bytes);
  full_ppm_bytes += rdex_tex_size * rdex_tex_size * 3;
  // copy original raw texture to main memory as float data
  struct image *raw_image = image_alloc(rdex_tex_size, rdex_tex_size, 3);
  image_copytexture(raw_image, rawtexture);
  // copy original texture to main memory as float data
  struct image *full_image = image_alloc(rdex_tex_size, rdex_tex_size, 3);
  image_copytexture(full_image, texture);
  // pass all these images to the background thread, which will free them
  background_enqueue(&library->background, ru, rv, f, k, score, behaviour,
    /*thumb_ppm, thumb_ppm_bytes,*/ full_ppm, full_ppm_bytes, raw_image, full_image);
  // store in the library
  library_insert(library, ru, rv, f, k, index, score);
  glDisable(GL_TEXTURE_2D);
}

void library_reshape(struct library *library, int w, int h) {
  library->width = w;
  library->height = h;
//  bloom_reshape(&library->bloom, 1024, 1024); // FIXME hardcoded
}

void library_display(struct library *library, GLuint fbo, GLuint texture) {
  switch (library->state) {
    case library_state_intro:
      if (! library->loaded) {
        library_load(library, fbo);
        library->loaded = 1;
      } else {
        library->state = library_state_active;
      }
      // fall-through
    case library_state_active: {
      glEnable(GL_TEXTURE_2D);
      glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
    /*
      glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
      glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, library->balltex, 0);
    */
      glViewport(0, 0, library->width, library->height);

      // update camera
      for (int i = 0; i < 4; ++i) {
        library->camera.scale.v[i] = library->stddev[i] > 0 ? 2.0 / library->stddev[i] : 1.0;
      }
      for (int i = 0; i < 10; ++i) camera_update(&library->camera, &library->target);
      for (int i = 0; i < 4; ++i) {
        library->borderwindow.value.origin[i] = library->camera.C.v[i];
        library->borderwindow_pick.value.origin[i] = library->camera.C.v[i];
      }
      for (int i = 0; i < 16; ++i) {
        library->borderwindow.value.rotate[i] = library->camera.M.m[i];
        library->borderwindow_pick.value.rotate[i] = library->camera.M.m[i];
      }
      for (int i = 0; i < 4; ++i) {
        library->borderwindow.value.scale[i] = library->camera.scale.v[i];
        library->borderwindow_pick.value.scale[i] = library->camera.scale.v[i];
      }

      glClearColor(0,0,0,0);  
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      
    //  int width, height;
      glEnable(GL_TEXTURE_2D);
      glBindTexture(GL_TEXTURE_2D, texture);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glMatrixMode(GL_PROJECTION);
      glLoadIdentity();
      gluOrtho2D(0, 1, 0, 1);
      glMatrixMode(GL_MODELVIEW);
      glLoadIdentity();
    //  glViewport(0, 0, library->width, library->height);
    //  glGetTexLevelParameteriv(
    //    GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &width
    //  );
    //  glGetTexLevelParameteriv(
    //    GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &height
    //  );
      library->worldsphere.value.size[0] = rdex_tex_size; //1024; //width;
      library->worldsphere.value.size[1] = rdex_tex_size; //height;
      worldsphere_use(&library->worldsphere);
      glBegin(GL_QUADS); {
        float x = 3.0;
        float y = 3.0 * library->height / library->width;
        glColor4f(1,1,1,1);
        glTexCoord4f(0.5 - x, 0.5 - y,  x, -y); glVertex2f(0, 0);
        glTexCoord4f(0.5 + x, 0.5 - y, -x, -y); glVertex2f(1, 0);
        glTexCoord4f(0.5 + x, 0.5 + y, -x,  y); glVertex2f(1, 1);
        glTexCoord4f(0.5 - x, 0.5 + y,  x,  y); glVertex2f(0, 1);
      } glEnd();
      worldsphere_use(0);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    
      // draw balls  
      glEnable(GL_DEPTH_TEST);
      glMatrixMode(GL_PROJECTION);
      glLoadIdentity();
      float v = library->width / (float) library->height;
      glFrustum(-v, v, -1, 1, 4, 12);
      glMatrixMode(GL_MODELVIEW);
      glLoadIdentity();
      glTranslatef(0, 0, -8);
      glScalef(2, 2, 2);
      borderwindow_use(&library->borderwindow);
      float a = library->height / (float) library->width;
      for (int i = 0; i < library->count; ++i) {
        library->array[i].spin[0] += library->array[i].dspin[0];
        library->array[i].spin[0] -= floor(library->array[i].spin[0]);
        library->array[i].spin[1] += library->array[i].dspin[1];
        library->array[i].spin[1] -= floor(library->array[i].spin[1]);
        GLfloat z  = sqrt(library->array[i].score) + 1.0/256.0;
        // note: y texcoord flipped for correct display from loaded image files
        int dx[4] = { 0, 1, 1, 0 };
        int dy[4] = { 1, 1, 0, 0 };
        for (int j = 0; j < 4; ++j) {
          for (int k = 0; k < 4; ++k) {
            library->varray[i][j].vertex[k] = library->array[i].v[k];
          }
          library->varray[i][j].texcoord[0] = i;
          library->varray[i][j].texcoord[1] = 0;
          library->varray[i][j].texcoord[2] = dx[j];
          library->varray[i][j].texcoord[3] = dy[j];
          library->varray[i][j].delta[0] = (dx[j] * 2 - 1) * z * a;
          library->varray[i][j].delta[1] = (1 - 2 * dy[j]) * z;
          library->varray[i][j].spin[0] = library->array[i].spin[0];
          library->varray[i][j].spin[1] = library->array[i].spin[1];
        }
      }
      glEnableClientState(GL_VERTEX_ARRAY);
      glEnableClientState(GL_TEXTURE_COORD_ARRAY);
      glEnableVertexAttribArray(library->borderwindow.attr.delta);
      glEnableVertexAttribArray(library->borderwindow.attr.spin);
      glVertexPointer  (4, GL_FLOAT, sizeof(struct libvert), &library->varray[0][0].vertex[0]);
      glTexCoordPointer(4, GL_FLOAT, sizeof(struct libvert), &library->varray[0][0].texcoord[0]);
      glVertexAttribPointer(library->borderwindow.attr.delta, 2, GL_FLOAT, GL_FALSE, sizeof(struct libvert), &library->varray[0][0].delta[0]);
      glVertexAttribPointer(library->borderwindow.attr.spin,  2, GL_FLOAT, GL_FALSE, sizeof(struct libvert), &library->varray[0][0].spin[0]);
      glDrawArrays(GL_QUADS, 0, library->count * 4);
      glDisableClientState(GL_VERTEX_ARRAY);
      glDisableClientState(GL_TEXTURE_COORD_ARRAY);
      glDisableVertexAttribArray(library->borderwindow.attr.delta);
      glDisableVertexAttribArray(library->borderwindow.attr.spin);
        
      // reset texture mode for accurate image save
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
      glBindTexture(GL_TEXTURE_2D, 0);
    //  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
      borderwindow_use(0);
      glDisable(GL_DEPTH_TEST);
    
      // apply bloom filter (modifies texture in place)
     // bloom_display(&library->bloom, fbo, library->balltex);
    /*  
      // draw balls
      glViewport(0, 0, 1024, 576);
      glMatrixMode(GL_PROJECTION);
      glLoadIdentity();
      gluOrtho2D(0, 1, 0, 1);
      glMatrixMode(GL_MODELVIEW);
      glLoadIdentity();
      glEnable(GL_TEXTURE_2D);
      glBindTexture(GL_TEXTURE_2D, library->balltex);
      glBegin(GL_QUADS); {
        float x = 0.5;
        float y = 0.5 * 576.0/1024.0; // FIXME hardcoded
        glColor4f(1,1,1,1);
        glTexCoord2f(0.5 - x, 0.5 - y); glVertex2f(0, 0);
        glTexCoord2f(0.5 + x, 0.5 - y); glVertex2f(1, 0);
        glTexCoord2f(0.5 + x, 0.5 + y); glVertex2f(1, 1);
        glTexCoord2f(0.5 - x, 0.5 + y); glVertex2f(0, 1);
      } glEnd();
    */  
      glDisable(GL_TEXTURE_2D);

#if 0
      /* draw cursor */
      glMatrixMode(GL_PROJECTION);
      glLoadIdentity();
      gluOrtho2D(0, library->width, 0, library->height);
      glMatrixMode(GL_MODELVIEW);
      glLoadIdentity();
      glBegin(GL_LINES); {
        glColor4f(0,1,0,1);
        glVertex2f(library->mousex, 0);
        glVertex2f(library->mousex, library->height);
        glVertex2f(0, library->mousey);
        glVertex2f(library->width, library->mousey);
      } glEnd();
#endif
      
      glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
      library->frame += 1;
      break;
    }

#if 0
    case library_state_intro: {
      if (! library->loaded) {
        library_load(library, fbo);
        library->loaded = 1;
      }
      glEnable(GL_TEXTURE_2D);
      glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
      glViewport(0, 0, library->width, library->height);
      glClearColor(0,0,1,1);  
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      glEnable(GL_TEXTURE_2D);
      glBindTexture(GL_TEXTURE_2D, texture);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glMatrixMode(GL_PROJECTION);
      glLoadIdentity();
      gluOrtho2D(0, 1, 0, 1);
      glMatrixMode(GL_MODELVIEW);
      glLoadIdentity();
      library->worldsphere.value.size[0] = rdex_tex_size;
      library->worldsphere.value.size[1] = rdex_tex_size;
      worldsphere_use(&library->worldsphere);
      glBegin(GL_QUADS); {
        float x0 = 3.0;
        float y0 = x0 * library->height / library->width;
        float x1 = 3.0 / (library->phase * library->phase + 0.001);
        float y1 = x1 * library->height / library->width;
        glColor4f(1,1,1,1);
        glTexCoord4f(0.5 - x0, 0.5 - y0,  x1, -y1); glVertex2f(0, 0);
        glTexCoord4f(0.5 + x0, 0.5 - y0, -x1, -y1); glVertex2f(1, 0);
        glTexCoord4f(0.5 + x0, 0.5 + y0, -x1,  y1); glVertex2f(1, 1);
        glTexCoord4f(0.5 - x0, 0.5 + y0,  x1,  y1); glVertex2f(0, 1);
      } glEnd();
      worldsphere_use(0);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
      library->phase += 0.04 / 25.0;
      if (! (library->phase < 1.0)) {
        library->state = library_state_active;
        library->phase = 0;
      }
      glBindTexture(GL_TEXTURE_2D, 0);
      glDisable(GL_TEXTURE_2D);
      glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
      library->frame += 1;
      break;
    }
#endif
    case library_state_outro: {
      glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
      glViewport(0, 0, library->width, library->height);
      glClearColor(0,0,1,1);  
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);      
      break;
    }
    case library_state_idle:    
    case library_state_done: {
      glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
      glViewport(0, 0, library->width, library->height);
      glClearColor(0,0,0,1);
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
      break;
    }
  }
}

void library_pmotion(struct library *library, int x, int y) {
  library->mousex = x;
  library->mousey = library->height - y - 1;
}

void library_amotion(struct library *library, int x, int y) {
  library->mousex = x;
  library->mousey = library->height - y - 1;
}

void library_mouse(struct library *library, int button, int state, int x, int y) {
  if (state == GLUT_DOWN) {
    library->picked = library->hover;
  }
}

void library_idle(struct library *library) {
  worldsphere_idle(&library->worldsphere);
}

// EOF
