/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2009,2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
pthread-based fifo
===================================================================== */

#include <assert.h>
#include <string.h>
#include "pfifo.h"

//#include <stdio.h>

//======================================================================
// prototypes
void *pfifo_consumerthread(void *);

//======================================================================
// create a new fifo with a consumer thread
struct pfifo *pfifo_create(pfifo_consumer *consumer, void *consumerdata) {
  struct pfifo *p = malloc(sizeof(struct pfifo));
  if (!p) return 0;
  list_init(&(p->list));
  p->consumer = consumer;
  p->consumerdata = consumerdata;
  p->mutex = malloc(sizeof(pthread_mutex_t));
  pthread_mutex_init(p->mutex, 0);
  p->nonempty = malloc(sizeof(pthread_cond_t));
  pthread_cond_init(p->nonempty, 0);
  pthread_create(&(p->thread), 0, pfifo_consumerthread, p);
  return p;
}

//======================================================================
// append to the fifo, copying data
void pfifo_enqueue(struct pfifo *p, size_t length, const void *data) {
  struct pfifo_node *n = malloc(sizeof(struct pfifo_node));
  assert(n);
  n->length = length;
  n->data = malloc(length);
  assert(n->data);
  memcpy(n->data, data, length);
  pthread_mutex_lock(p->mutex);
  list_inserttail(&(p->list), &(n->node));
  //int i = list_length(&(p->list));
  pthread_mutex_unlock(p->mutex);
  pthread_cond_signal(p->nonempty);
  //fprintf(stderr, "\ndebug: pfifo size %d\n", i);
}

//======================================================================
// loop running consumer callback on each fifo item, freeing data
void *pfifo_consumerthread(void *fifo) {
  struct pfifo *p = fifo;
  while (1) {
    pthread_mutex_lock(p->mutex);
    while (list_isempty(&(p->list))) pthread_cond_wait(p->nonempty, p->mutex);
    struct pfifo_node *n = (struct pfifo_node *) list_removehead(&(p->list));
    pthread_mutex_unlock(p->mutex);
    p->consumer(p->consumerdata, n->length, n->data);
    free(n->data);
    free(n);
  }
  return 0;
}


// EOF
