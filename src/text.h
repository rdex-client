/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010,2011  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Text module
===================================================================== */

#ifndef TEXT_H
#define TEXT_H 1

#include "shader.h"
#include "fonts.h"

#define TEXT_LENGTH 32

struct text { struct shader shader;
  struct {
    GLint texture; // fragment
  } uniform;
  struct {
    int texture; // fragment
  } value;
  struct fonts fonts;
  char textstr[TEXT_LENGTH]; // characters to display
  int width;
  int height;
};

struct text *text_init(struct text *text);
void text_reshape(struct text *text, int w, int h);
void text_display(struct text *text);
int text_keynormal(struct text *text, int key, int x, int y);
int text_keyspecial(struct text *text, int key, int x, int y);

#endif
