/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
CPU-Based Images
===================================================================== */

#include <stdlib.h>
#include <GL/glew.h>
#include "image.h"

struct image *image_alloc(int width, int height, int channels) {
  struct image *image =
    malloc(sizeof(struct image) + width*height*channels*sizeof(float));
  if (! image) { return 0; }
  image->width = width;
  image->height = height;
  image->channels = channels;
  image->data = (float *) (image+1);
  return image;
}

void image_free(struct image *image) {
  free(image);
}

void image_copytexture(struct image *image, GLuint texture) {
  glBindTexture(GL_TEXTURE_2D, texture);
  glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_FLOAT, image->data); // FIXME check data is big enough for the image!!!!
}
