/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
False Colour Shader
===================================================================== */

#include "util.h"
#include "falsecolour.h"
#include "falsecolour.frag.c"

//======================================================================
// false colour shader initialization
struct falsecolour *falsecolour_init(struct falsecolour *falsecolour) {
  if (! falsecolour) { return 0; }
  if (! shader_init(&falsecolour->shader, 0, falsecolour_frag)) {
    return 0;
  }
  if (! blur_init(&falsecolour->blur)) { return 0; }
  shader_uniform(falsecolour, texture);
  falsecolour->value.texture = 0;
  glGenTextures(1, &falsecolour->texture0);
  glGenTextures(1, &falsecolour->texture);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, falsecolour->texture0);
//  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glBindTexture(GL_TEXTURE_2D, falsecolour->texture);
//  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glBindTexture(GL_TEXTURE_2D, 0);
  glDisable(GL_TEXTURE_2D);
  falsecolour->width = 0;
  falsecolour->height = 0;
  return falsecolour;
}

//======================================================================
// false colour shader reshape callback
void falsecolour_reshape(
  struct falsecolour *falsecolour, int w, int h
) {
  falsecolour->width  = roundtwo(w);
  falsecolour->height = roundtwo(h);
  falsecolour->blur.value.d[0] = 0.5 / falsecolour->width;
  falsecolour->blur.value.d[1] = 0.5 / falsecolour->height;
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, falsecolour->texture0);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F_ARB,
    falsecolour->width * 2, falsecolour->height * 2, 0, GL_RGBA, GL_FLOAT, 0
  );
  glBindTexture(GL_TEXTURE_2D, falsecolour->texture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F_ARB,
    falsecolour->width, falsecolour->height, 0, GL_RGBA, GL_FLOAT, 0
  );
  glBindTexture(GL_TEXTURE_2D, 0);
  glDisable(GL_TEXTURE_2D);
}

//======================================================================
// false colour shader display callback
void falsecolour_display(
  struct falsecolour *falsecolour, GLuint fbo, GLuint texture
) {
  glEnable(GL_TEXTURE_2D);
  glViewport(0, 0, falsecolour->width * 2, falsecolour->height * 2);
  glBindTexture(GL_TEXTURE_2D, texture);
  //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
  glFramebufferTexture2DEXT(
    GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D,
    falsecolour->texture0, 0
  );
  glUseProgramObjectARB(falsecolour->shader.program);
  shader_updatei(falsecolour, texture);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, 1, 0, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glBegin(GL_QUADS); { glColor4f(1,1,1,1);
    glTexCoord2f(0, 0); glVertex2f(0, 0);
    glTexCoord2f(1, 0); glVertex2f(1, 0);
    glTexCoord2f(1, 1); glVertex2f(1, 1);
    glTexCoord2f(0, 1); glVertex2f(0, 1);
  } glEnd();
  glUseProgramObjectARB(0);
  glBindTexture(GL_TEXTURE_2D, 0);
  // linear interpolation
  glViewport(0, 0, falsecolour->width, falsecolour->height);
  glFramebufferTexture2DEXT(
    GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D,
    falsecolour->texture, 0
  );
  glUseProgramObjectARB(falsecolour->blur.shader.program);
  shader_updatei(&(falsecolour->blur), texture);
  shader_updatef2(&(falsecolour->blur), d);
  glBindTexture(GL_TEXTURE_2D, falsecolour->texture0);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, 1, 0, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glBegin(GL_QUADS); { glColor4f(1,1,1,1);
    glTexCoord2f(0, 0); glVertex2f(0, 0);
    glTexCoord2f(1, 0); glVertex2f(1, 0);
    glTexCoord2f(1, 1); glVertex2f(1, 1);
    glTexCoord2f(0, 1); glVertex2f(0, 1);
  } glEnd();
  glUseProgramObjectARB(0);
  //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  glBindTexture(GL_TEXTURE_2D, 0);
  glDisable(GL_TEXTURE_2D);
}

//======================================================================
// false colour shader idle callback
void falsecolour_idle(struct falsecolour *falsecolour) {
}

// EOF
