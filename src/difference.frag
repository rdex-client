/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
===================================================================== */

uniform sampler2D texture1;       // first texture
uniform sampler2D texture2;       // second texture

void main(void) {
  vec2 p = gl_TexCoord[0].st;      // texel coordinates
  vec4 d = texture2D(texture1, p)  // difference between channels
         - texture2D(texture2, p);
  float q = sqrt(dot(d,d));        // length of the difference vector
  gl_FragColor = vec4(q, q, q, 1.0); // output
}
