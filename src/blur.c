/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Blur shader
===================================================================== */

#include "blur.h"
#include "blur.frag.c"

//======================================================================
// blur shader initialization
struct blur *blur_init(struct blur *blur) {
  if (! blur) { return 0; }
  if (! shader_init(&blur->shader, 0, blur_frag)) { return 0; }
  shader_uniform(blur, texture);
  shader_uniform(blur, d);
  blur->value.texture = 0;
  blur->value.d[0] = 0;
  blur->value.d[1] = 0;
  return blur;
}

// EOF
