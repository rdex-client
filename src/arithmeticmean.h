/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Arithmetic Mean Shader
===================================================================== */

#ifndef ARITHMETIC_MEAN_H
#define ARITHMETIC_MEAN_H 1

#include "shader.h"

//======================================================================
// maximum number of textures to allocate (16 => 65536x65536 max size)
#define arithmeticmean_tex_max 16

//======================================================================
// arithmetic mean shader data
struct arithmeticmean { struct shader shader;
  struct { GLint texture; GLint dx; GLint dy; } uniform;
  struct { int   texture; float dx; float dy; } value;
  int count;
  GLuint textures[arithmeticmean_tex_max];
  GLfloat result[4];
  float stddev;
};

//======================================================================
// prototypes
struct arithmeticmean *arithmeticmean_init(
  struct arithmeticmean *arithmeticmean
);
void arithmeticmean_reshape(
  struct arithmeticmean *arithmeticmean, int w, int h
);
void arithmeticmean_display(
  struct arithmeticmean *arithmeticmean, GLuint fbo, GLuint texture
);
void arithmeticmean_idle(struct arithmeticmean *arithmeticmean);

#endif
