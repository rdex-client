/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
False Colour Shader
===================================================================== */

#ifndef FALSECOLOUR_H
#define FALSECOLOUR_H 1

#include "shader.h"
#include "blur.h"

//======================================================================
// false colour shader data
struct falsecolour { struct shader shader;
  struct { GLint  texture; } uniform;
  struct { int    texture; } value;
  GLuint texture0, texture;
  int width;
  int height;
  struct blur blur;
};

//======================================================================
// protoypes
struct falsecolour *falsecolour_init(struct falsecolour *falsecolour);
void falsecolour_reshape(
  struct falsecolour *falsecolour, int w, int h
);
void falsecolour_display(
  struct falsecolour *falsecolour, GLuint fbo, GLuint texture
);
void falsecolour_idle(struct falsecolour *falsecolour);

#endif
