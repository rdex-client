/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Bloom/Glow effect
===================================================================== */

uniform sampler2D tex0;
uniform sampler2D tex1;
uniform sampler2D tex2;
uniform sampler2D tex3;
uniform sampler2D tex4;
uniform sampler2D tex5;
uniform sampler2D tex6;
uniform sampler2D tex7;

void main(void) {
  //float size;
  //vec2 p0, p1, dp;
  vec2 p = gl_TexCoord[0].st;
  //vec4 o, o0;
/* // waaaaaaay too big for most hardware :(
  size = 1024.0; // FIXME hardcoded
  p0 = floor(p * size) / size;
  p1 = p0 + vec2(1.0, 1.0) / size;
  dp = (p1 - p0) * size;
  o0 = mix(mix(texture2D(tex0, p0), texture2D(tex0, vec2(p1.x, p0.y)), dp.x),
           mix(texture2D(tex0, vec2(p0.x, p1.y)), texture2D(tex0, p1), dp.x), dp.y);
  o = o0 * 9.0;

  size *= 0.5;
  p0 = floor(p * size) / size;
  p1 = p0 + vec2(1.0, 1.0) / size;
  dp = (p1 - p0) * size;
  o += mix(mix(texture2D(tex1, p0), texture2D(tex1, vec2(p1.x, p0.y)), dp.x),
           mix(texture2D(tex1, vec2(p0.x, p1.y)), texture2D(tex1, p1), dp.x), dp.y);

  size *= 0.5;
  p0 = floor(p * size) / size;
  p1 = p0 + vec2(1.0, 1.0) / size;
  dp = (p1 - p0) * size;
  o += mix(mix(texture2D(tex2, p0), texture2D(tex2, vec2(p1.x, p0.y)), dp.x),
           mix(texture2D(tex2, vec2(p0.x, p1.y)), texture2D(tex2, p1), dp.x), dp.y);

  size *= 0.5;
  p0 = floor(p * size) / size;
  p1 = p0 + vec2(1.0, 1.0) / size;
  dp = (p1 - p0) * size;
  o += mix(mix(texture2D(tex3, p0), texture2D(tex3, vec2(p1.x, p0.y)), dp.x),
           mix(texture2D(tex3, vec2(p0.x, p1.y)), texture2D(tex3, p1), dp.x), dp.y);

  size *= 0.5;
  p0 = floor(p * size) / size;
  p1 = p0 + vec2(1.0, 1.0) / size;
  dp = (p1 - p0) * size;
  o += mix(mix(texture2D(tex4, p0), texture2D(tex4, vec2(p1.x, p0.y)), dp.x),
           mix(texture2D(tex4, vec2(p0.x, p1.y)), texture2D(tex4, p1), dp.x), dp.y);

  size *= 0.5;
  p0 = floor(p * size) / size;
  p1 = p0 + vec2(1.0, 1.0) / size;
  dp = (p1 - p0) * size;
  o += mix(mix(texture2D(tex5, p0), texture2D(tex5, vec2(p1.x, p0.y)), dp.x),
           mix(texture2D(tex5, vec2(p0.x, p1.y)), texture2D(tex5, p1), dp.x), dp.y);

  size *= 0.5;
  p0 = floor(p * size) / size;
  p1 = p0 + vec2(1.0, 1.0) / size;
  dp = (p1 - p0) * size;
  o += mix(mix(texture2D(tex6, p0), texture2D(tex6, vec2(p1.x, p0.y)), dp.x),
           mix(texture2D(tex6, vec2(p0.x, p1.y)), texture2D(tex6, p1), dp.x), dp.y);

  size *= 0.5;
  p0 = floor(p * size) / size;
  p1 = p0 + vec2(1.0, 1.0) / size;
  dp = (p1 - p0) * size;
  o += mix(mix(texture2D(tex7, p0), texture2D(tex7, vec2(p1.x, p0.y)), dp.x),
           mix(texture2D(tex7, vec2(p0.x, p1.y)), texture2D(tex7, p1), dp.x), dp.y);
*/
  vec4 o0 = texture2D(tex0, p);
  vec4 o1 = texture2D(tex1, p);
  vec4 o2 = texture2D(tex2, p);
  vec4 o3 = texture2D(tex3, p);
  vec4 o4 = texture2D(tex4, p);
  vec4 o5 = texture2D(tex5, p);
  vec4 o6 = texture2D(tex6, p);
  vec4 o7 = texture2D(tex7, p);
  vec4 o;
  float k = 1.1;
  float t = 1.0;
  o  = o7; o *= k; t *= k;
  o += o6; o *= k; t += 1.0; t *= k;
  o += o5; o *= k; t += 1.0; t *= k;
  o += o4; o *= k; t += 1.0; t *= k;
  o += o3; o *= k; t += 1.0; t *= k;
  o += o2; o *= k; t += 1.0; t *= k;
  o += o1; o *= k; t += 1.0; t *= k;
  o += o0; o *= k; t += 1.0; t *= k;
  o /= t;
  float l = length(o.rgb);
  gl_FragColor = vec4(o.rgb * clamp(log(5.0 * log(5.0 * l + 1.0) + 1.0), 0.0, 1.0), 1.0);
}
