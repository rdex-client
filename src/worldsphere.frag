/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
World sphere shader
===================================================================== */

uniform sampler2D texture;
uniform vec2 rot;
uniform vec2 size;

void main(void) {
  vec2 p = gl_TexCoord[0].xy;
  vec2 s = sqrt(0.5) * gl_TexCoord[0].zw;
  float d = dot(s,s);
  vec3 n;
  float a;
  float c;
  if (d < 1.0) {
    c = 1.0;
    a = 1.0;
    n = normalize(vec3(s, sqrt(1.0 - d)));
    p = sqrt(2.0) * s / (1.0 + sqrt(1.0 - d));
  } else {
    c = 0.0;
    a = 0.0;
    n = vec3(0.0, 0.0, 0.0);
  }
  c = c * 0.0625 + pow(dot(vec3(0.0, 0.0, 1.0), n), 2.0);
  p += rot;
  p -= floor(p);
  vec2 p0 = floor(p * size) / size;
  vec2 dp = (p - p0) * size;
  vec3 tl = texture2D(texture, p0).rgb;
  vec3 tr = texture2D(texture, p0 + vec2(1.0/size.x, 0.0       )).rgb;
  vec3 bl = texture2D(texture, p0 + vec2(0.0,        1.0/size.y)).rgb;
  vec3 br = texture2D(texture, p0 + vec2(1.0/size.x, 1.0/size.y)).rgb;
  vec3 t = tl * (1.0 - dp.x) + dp.x * tr;
  vec3 b = bl * (1.0 - dp.x) + dp.x * br;
  vec3 l = tl * (1.0 - dp.y) + dp.y * bl;
  vec3 r = tr * (1.0 - dp.y) + dp.y * br;
  vec3 m1 = t * (1.0 - dp.y) + dp.y * b;
  vec3 m2 = l * (1.0 - dp.x) + dp.x * r;
  vec3 m = 0.5 * (m1 + m2);
  gl_FragColor = vec4(m * c, a);
}
