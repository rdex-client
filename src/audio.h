/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010,2019  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Audio output
===================================================================== */

#ifndef AUDIO_H
#define AUDIO_H 1

#include <stdio.h>
#include <pthread.h>
#include <jack/jack.h>
#include <sndfile.h>
#include "shader.h"

//======================================================================
// audio state

#define AUDIO_BUFFERS 64
#define AUDIO_CHANNELS 2
#define audio_tex_size 16
#define AUDIO_BUFSIZE (audio_tex_size * audio_tex_size)
#define AUDIO_COUNT 8

// synchronise between JACK dsp callback and GLUT timer callback
// JACK is the master clock
extern volatile char audio_read;
extern volatile char audio_write;

struct audio { struct shader shader;
  struct { GLint texture, coords; /*texsize, center, radius, size;*/ } uniform;
  struct { GLint texture, coords; /*GLfloat texsize[2], center[2], radius, size;*/ } value;
  GLuint textures[2];
  float coords[4 * audio_tex_size * audio_tex_size];
  double pitch; // frequency in Hz
  double x, y, a; // scan point
  double curve, speed, dcurve, dspeed; // scan movement
  double samples; // total samples scanned
  GLuint pbos[AUDIO_CHANNELS][AUDIO_COUNT]; // pixel buffers for reading back from GPU (FIXME hardcoded count)

  /* non-realtime mode */
  int nrt;
  int frame;
  jack_default_audio_sample_t *nrtbuffers[2];
  float *nrtbuffers_interleaved;
  SNDFILE *nrtf;
  /* OSS handles */
  int running;
  pthread_t oss_thread;
  int oss_fd;
  int16_t oss_buffer[AUDIO_BUFSIZE][AUDIO_CHANNELS];
  jack_default_audio_sample_t oss_jack_buffer[AUDIO_CHANNELS][AUDIO_BUFSIZE];
  /* JACK handles */
  jack_client_t *client;
  jack_port_t *port[2];
  enum {
    audio_state_idle,
    audio_state_prepare,
    audio_state_intro,
    audio_state_rolling,
    audio_state_outro
  } state;
  double sphase;
  /* audio buffers */
  unsigned int index;
  jack_default_audio_sample_t buffer[AUDIO_BUFFERS][AUDIO_CHANNELS][AUDIO_BUFSIZE];
  int size;
  int sr;
  /* DC blocker filters */
  jack_default_audio_sample_t r[2][AUDIO_CHANNELS], xn1[2][AUDIO_CHANNELS], yn1[2][AUDIO_CHANNELS], gain[AUDIO_CHANNELS];
  /* intro */
  double iphase[AUDIO_CHANNELS];
  jack_nframes_t sample[AUDIO_CHANNELS];
};

//======================================================================
// prototypes
struct audio *audio_init(struct audio *audio, int nrt);
void audio_atexit(struct audio *audio);
void audio_display1(struct audio *audio, GLuint fbo, GLuint texture, int iter);
void audio_display2(struct audio *audio);
void audio_start(struct audio *audio);
void audio_stop(struct audio *audio);

#endif
