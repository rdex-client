/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Chase camera
===================================================================== */

/*

The camera is modelled by some point masses connected together:

    T  = target
    V* = vertices of camera cage

where:

    massV* << massT

Each V is connected to T by a stretchy spring of appropriate rest length.
Each V is connected to each other V by a stiff spring of appropriate rest
length.

The camera rotation matrix is:

    C = sum(V*) / count(V*)
    M = normalize([ V0, V1, V2, V3 ] - [ C, C, C, C ])

Updating the view is peformed by updating the mass spring damper model.

*/

#include <math.h>
#include <string.h>
#include "camera.h"

void camera_init(struct camera *camera) {
  memset(camera, 0, sizeof(*camera));
  camera->dt = 0.004;
  // camera scale
  float r = 1.0 / 16.0;
  // initialize target point
  camera->T.position.v[3] = r;
  camera->T.mass = 1 << 20;
  // initialize camera points
  camera->V[0].position.v[0] = r;
  camera->V[1].position.v[1] = r;
  camera->V[2].position.v[2] = r;
  camera->V[3].position.v[3] = r;
  camera->V[4].position.v[0] = -r;
  camera->V[5].position.v[1] = -r;
  camera->V[6].position.v[2] = -r;
  camera->V[7].position.v[3] = -r;
  camera->r = r;
  for (int i = 0; i < 8; ++i) {
    camera->V[i].mass = 1.0 / 8.0; // + 4.0 * ((i == 3) || (i == 7));
  }
  // initialize external links
  for (int i = 0; i < 8; ++i) {
    camera->TV[i].p = &camera->T;
    camera->TV[i].q = &camera->V[i];
    camera->TV[i].length = vdistance4(&camera->T.position, &camera->V[i].position);
    camera->TV[i].k = 1;
    camera->TV[i].c = 2 * sqrt(camera->TV[i].k); // * camera->V[i].mass);
  }
  // initialize internal links
  int k = 0;
  for (int i = 0; i < 8 - 1; ++i) {
    for (int j = i + 1; j < 8; ++j) {
      camera->VV[k].p = &camera->V[i];
      camera->VV[k].q = &camera->V[j];
      camera->VV[k].length = vdistance4(&camera->V[i].position, &camera->V[j].position);
      camera->VV[k].k = 1;
      camera->VV[k].c = 2 * sqrt(camera->VV[k].k * sqrt(camera->V[i].mass * camera->V[j].mass));
      k++;
    }
  }
  // reset position
  for (int i = 0; i < 8; ++i) {
    camera->V[i].position.v[3] -= r;
  }
}

void camera_force(struct link *l) {
  struct vec4 zero = { { 0, 0, 0, 0 } };
  struct vec4 f, dir, ndir, dv;
  float ldir, x, v;
  vcopy4(&f, &zero);
  /* dir  = l->p->position - l->q->position
   * ldir = length(dir)
   * ndir = dir / ldir
   * x    = l->length - ldir
   * v    = dot(l->p->velocity - l->q->velocity, ndir)
   * f    = - (k x + c v)
   */
  vvsub4(&dir, &l->p->position, &l->q->position);
  ldir = vlength4(&dir);
  if (! (ldir > 0)) {
    return;
  }
  x = ldir - l->length;
  vsdiv4(&ndir, &dir, ldir);
  vvsub4(&dv, &l->p->velocity, &l->q->velocity);
  v = vvdot4(&dv, &ndir);
  vsmul4(&f, &ndir, l->k * x + l->c * v);
  vvsub4(&l->p->force, &l->p->force, &f);
  vvadd4(&l->q->force, &l->q->force, &f);  
}

void camera_action1(struct mass *m, float dt) {
  struct vec4 a, dv;
  vsdiv4(&a, &m->force, m->mass);
  vsmul4(&dv, &a, dt);
  vvadd4(&m->velocity, &m->velocity, &dv);
}

void camera_friction(struct mass *m, struct vec4 *v0, struct vec4 *v0dir) {
  struct vec4 dv;
  vvsub4(&dv, &m->velocity, v0);
  vsmul4(&m->velocity, v0, 0.995);
  vvadd4(&m->velocity, &m->velocity, &dv);
//  vsmul4(&m->velocity, &m->velocity, 0.99);
}

void camera_action2(struct mass *m, float dt) {
  struct vec4 dp;
  vsmul4(&dp, &m->velocity, dt);
  vvadd4(&m->position, &m->position, &dp);
}

void camera_update(struct camera *camera, struct vec4 *target) {
  // set target
  struct vec4 zero = { { 0, 0, 0, 0 } };
  vvmul4(&camera->T.position, target, &camera->scale);
  vcopy4(&camera->T.velocity, &zero);
  // reset forces
  vcopy4(&camera->T.force, &zero);
  for (int i = 0; i < 8; ++i) {
    vcopy4(&camera->V[i].force, &zero);
  }
  // accumulate forces from links
  for (int i = 0; i < 8; ++i) {
    camera_force(&camera->TV[i]);
  }
  for (int i = 0; i < 28; ++i) {
    camera_force(&camera->VV[i]);
  }
  // convert forces to movement
  for (int i = 0; i < 8; ++i) {
    camera_action1(&camera->V[i], camera->dt);
  }
  struct vec4 v0, v0dir;
  vcopy4(&v0, &zero);
  for (int i = 0; i < 8; ++i) {
    vvadd4(&v0, &v0, &camera->V[i].velocity);
  }
  vsdiv4(&v0, &v0, 8.0);
  float vl = vlength4(&v0);
  vl = vl > 0 ? vl : 1.0;
  vsdiv4(&v0dir, &v0, vl);
  for (int i = 0; i < 8; ++i) {
    camera_friction(&camera->V[i], &v0, &v0dir);
  }
  for (int i = 0; i < 8; ++i) {
    camera_action2(&camera->V[i], camera->dt);
  }
  // calculate camera center
  vcopy4(&camera->C, &zero);
  for (int i = 0; i < 8; ++i) {
    vvadd4(&camera->C, &camera->C, &camera->V[i].position);
  }
  vsdiv4(&camera->C, &camera->C, 8);
  // calculate camera matrix
  struct vec4 v[8];
  for (int i = 0; i < 8; ++i) {
    vvsub4(&v[i], &camera->V[i].position, &camera->C);
  }
  int k = 0;
  for (int i = 0; i < 4; ++i) {
    float d = vdistance4(&v[i], &v[i+4]);
    for (int j = 0; j < 4; ++j) {
      camera->M.m[k++] = (v[i].v[j] - v[i+4].v[j]) / d; //(n + m);
    }
  }
  vcopy4(&camera->C, &camera->V[3].position);
  vvdiv4(&camera->C, &camera->C, &camera->scale);
}

// EOF
