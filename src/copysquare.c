/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Copy And Square Shader
===================================================================== */

#include "util.h"
#include "copysquare.h"
#include "copysquare.frag.c"

//======================================================================
// copysquare shader initialization
struct copysquare *copysquare_init(struct copysquare *copysquare) {
  if (! copysquare) { return 0; }
  if (! shader_init(&copysquare->shader, 0, copysquare_frag)) {
    return 0;
  }
  shader_uniform(copysquare, texture);
  copysquare->value.texture = 0;
  glGenTextures(1, &copysquare->texture);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, copysquare->texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glDisable(GL_TEXTURE_2D);
  copysquare->width = 0;
  copysquare->height = 0;
  return copysquare;
}

//======================================================================
// copysquare shader reshape callback
void copysquare_reshape(struct copysquare *copysquare, int w, int h) {
  copysquare->width  = roundtwo(w);
  copysquare->height = roundtwo(h);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, copysquare->texture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F_ARB,
    copysquare->width, copysquare->height, 0, GL_RGBA, GL_FLOAT, 0
  );
  glDisable(GL_TEXTURE_2D);
}

//======================================================================
// copysquare shader display callback
void copysquare_display(
  struct copysquare *copysquare, GLuint fbo, GLuint texture
) {
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, texture);
  //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
  glFramebufferTexture2DEXT(
    GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D,
    copysquare->texture, 0
  );
  glUseProgramObjectARB(copysquare->shader.program);
  shader_updatei(copysquare ,texture);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, 1, 0, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glViewport(0, 0, copysquare->width, copysquare->height);
  glBegin(GL_QUADS); { glColor4f(1,1,1,1);
    glTexCoord2f(0, 0); glVertex2f(0, 0);
    glTexCoord2f(1, 0); glVertex2f(1, 0);
    glTexCoord2f(1, 1); glVertex2f(1, 1);
    glTexCoord2f(0, 1); glVertex2f(0, 1);
  } glEnd();
  glUseProgramObjectARB(0);
  //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  glDisable(GL_TEXTURE_2D);
}

//======================================================================
// copysquare shader idle callback
void copysquare_idle(struct copysquare *copysquare) { }

// EOF
