/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Matrix computations
===================================================================== */

#include <math.h>
#include <stdio.h>
#include "matrix.h"

// vector copy

void vcopy3(struct vec3 *m, struct vec3 *x) {
  for (int i = 0; i < 3; ++i) {
    m->v[i] = x->v[i];
  }
}

void vcopy4(struct vec4 *m, struct vec4 *x) {
  for (int i = 0; i < 4; ++i) {
    m->v[i] = x->v[i];
  }
}

void vcopy5(struct vec5 *m, struct vec5 *x) {
  for (int i = 0; i < 5; ++i) {
    m->v[i] = x->v[i];
  }
}

// matrix copy

void mcopy3(struct mat3 *m, struct mat3 *x) {
  for (int i = 0; i < 9; ++i) {
    m->m[i] = x->m[i];
  }
}

void mcopy4(struct mat4 *m, struct mat4 *x) {
  for (int i = 0; i < 16; ++i) {
    m->m[i] = x->m[i];
  }
}

void mcopy5(struct mat5 *m, struct mat5 *x) {
  for (int i = 0; i < 25; ++i) {
    m->m[i] = x->m[i];
  }
}

// vector scalar multiplication

void vsmul3(struct vec3 *m, struct vec3 *x, float y) {
  for (int i = 0; i < 3; ++i) {
    m->v[i] = x->v[i] * y;
  }
}

void vsmul4(struct vec4 *m, struct vec4 *x, float y) {
  for (int i = 0; i < 4; ++i) {
    m->v[i] = x->v[i] * y;
  }
}

void vsmul5(struct vec5 *m, struct vec5 *x, float y) {
  for (int i = 0; i < 5; ++i) {
    m->v[i] = x->v[i] * y;
  }
}

// vector scalar division

void vsdiv3(struct vec3 *m, struct vec3 *x, float y) {
  for (int i = 0; i < 3; ++i) {
    m->v[i] = x->v[i] / y;
  }
}

void vsdiv4(struct vec4 *m, struct vec4 *x, float y) {
  for (int i = 0; i < 4; ++i) {
    m->v[i] = x->v[i] / y;
  }
}

void vsdiv5(struct vec5 *m, struct vec5 *x, float y) {
  for (int i = 0; i < 5; ++i) {
    m->v[i] = x->v[i] / y;
  }
}

// vector vector addition

void vvadd3(struct vec3 *m, struct vec3 *x, struct vec3 *y) {
  for (int i = 0; i < 3; ++i) {
    m->v[i] = x->v[i] + y->v[i];
  }
}

void vvadd4(struct vec4 *m, struct vec4 *x, struct vec4 *y) {
  for (int i = 0; i < 4; ++i) {
    m->v[i] = x->v[i] + y->v[i];
  }
}

void vvadd5(struct vec5 *m, struct vec5 *x, struct vec5 *y) {
  for (int i = 0; i < 5; ++i) {
    m->v[i] = x->v[i] + y->v[i];
  }
}

// vector vector subtraction

void vvsub3(struct vec3 *m, struct vec3 *x, struct vec3 *y) {
  for (int i = 0; i < 3; ++i) {
    m->v[i] = x->v[i] - y->v[i];
  }
}

void vvsub4(struct vec4 *m, struct vec4 *x, struct vec4 *y) {
  for (int i = 0; i < 4; ++i) {
    m->v[i] = x->v[i] - y->v[i];
  }
}

void vvsub5(struct vec5 *m, struct vec5 *x, struct vec5 *y) {
  for (int i = 0; i < 5; ++i) {
    m->v[i] = x->v[i] - y->v[i];
  }
}

// vector vector multiplication (componentwise)

void vvmul4(struct vec4 *m, struct vec4 *x, struct vec4 *y) {
  for (int i = 0; i < 4; ++i) {
    m->v[i] = x->v[i] * y->v[i];
  }
}

// vector vector division (componentwise)

void vvdiv4(struct vec4 *m, struct vec4 *x, struct vec4 *y) {
  for (int i = 0; i < 4; ++i) {
    m->v[i] = x->v[i] / y->v[i];
  }
}


// matrix vector multiplication

void mvmul3(struct vec3 *v, struct mat3 *x, struct vec3 *y) {
  int k = 0;
  for (int i = 0; i < 3; ++i) {
    v->v[i] = 0;
    for (int j = 0; j < 3; ++j) {
      v->v[i] += x->m[k++] * y->v[j];
    }
  }
}

void mvmul4(struct vec4 *v, struct mat4 *x, struct vec4 *y) {
  int k = 0;
  for (int i = 0; i < 4; ++i) {
    v->v[i] = 0;
    for (int j = 0; j < 4; ++j) {
      v->v[i] += x->m[k++] * y->v[j];
    }
  }
}

void mvmul5(struct vec5 *v, struct mat5 *x, struct vec5 *y) {
  int k = 0;
  for (int i = 0; i < 5; ++i) {
    v->v[i] = 0;
    for (int j = 0; j < 5; ++j) {
      v->v[i] += x->m[k++] * y->v[j];
    }
  }
}

// matrix matrix multiplication

void mmmul3(struct mat3 *m, struct mat3 *x, struct mat3 *y) {
  const int N = 3;
  int kk = 0;
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      m->m[kk] = 0;
      for (int k = 0; k < N; ++k) {
        m->m[kk] += x->m[k * N + j] * y->m[i * N + k];
      }
      kk++;
    }
  }
}

void mmmul4(struct mat4 *m, struct mat4 *x, struct mat4 *y) {
  const int N = 4;
  int kk = 0;
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      m->m[kk] = 0;
      for (int k = 0; k < N; ++k) {
        m->m[kk] += x->m[k * N + j] * y->m[i * N + k];
      }
      kk++;
    }
  }
}

void mmmul5(struct mat5 *m, struct mat5 *x, struct mat5 *y) {
  const int N = 5;
  int kk = 0;
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      m->m[kk] = 0;
      for (int k = 0; k < N; ++k) {
        m->m[kk] += x->m[k * N + j] * y->m[i * N + k];
      }
      kk++;
    }
  }
}

// homogeneous matrix construction

void identity3(struct mat3 *m) {
  const int N = 3;
  int k = 0;
  for (int i = 0; i < N; ++i) {
  for (int j = 0; j < N; ++j) {
    m->m[k++] = (i == j);
  } }
}

void identity4(struct mat4 *m) {
  const int N = 4;
  int k = 0;
  for (int i = 0; i < N; ++i) {
  for (int j = 0; j < N; ++j) {
    m->m[k++] = (i == j);
  } }
}

void identity5(struct mat5 *m) {
  const int N = 5;
  int k = 0;
  for (int i = 0; i < N; ++i) {
  for (int j = 0; j < N; ++j) {
    m->m[k++] = (i == j);
  } }
}

void homogen4(struct mat4 *m, struct mat3 *x) {
  m->m[ 0] = x->m[0]; m->m[ 1] = x->m[1]; m->m[ 2] = x->m[2]; m->m[ 3] = 0;
  m->m[ 4] = x->m[3]; m->m[ 5] = x->m[4]; m->m[ 6] = x->m[5]; m->m[ 7] = 0;
  m->m[ 8] = x->m[6]; m->m[ 9] = x->m[7]; m->m[10] = x->m[8]; m->m[11] = 0;
  m->m[12] = 0;       m->m[13] = 0;       m->m[14] = 0;       m->m[15] = 1;
}

void homogen5(struct mat5 *m, struct mat4 *x) {
  m->m[ 0] = x->m[ 0]; m->m[ 1] = x->m[ 1]; m->m[ 2] = x->m[ 2]; m->m[ 3] = x->m[ 3]; m->m[ 4] = 0;
  m->m[ 5] = x->m[ 4]; m->m[ 6] = x->m[ 5]; m->m[ 7] = x->m[ 6]; m->m[ 8] = x->m[ 7]; m->m[ 9] = 0;
  m->m[10] = x->m[ 8]; m->m[11] = x->m[ 9]; m->m[12] = x->m[10]; m->m[13] = x->m[11]; m->m[14] = 0;
  m->m[15] = x->m[12]; m->m[16] = x->m[13]; m->m[17] = x->m[14]; m->m[18] = x->m[15]; m->m[19] = 0;
  m->m[20] = 0;        m->m[21] = 0;        m->m[22] = 0;        m->m[23] = 0;        m->m[24] = 1;
}

void translate4(struct mat4 *m, struct vec3 *v) {
  const int N = 4;
  identity4(m);
  int k = N - 1;
  for (int i = 0; i < N - 1; ++i) {
    m->m[k] = v->v[i];
    k += N;
  }
}

void translate5(struct mat5 *m, struct vec4 *v) {
  const int N = 5;
  identity5(m);
  int k = N - 1;
  for (int i = 0; i < N - 1; ++i) {
    m->m[k] = v->v[i];
    k += N;
  }
}

// rolling ball

void rollingball4(struct mat4 *m, float R, struct vec3 *v) {
  float r = sqrt(v->v[0] * v->v[0] + v->v[1] * v->v[1] + v->v[2] * v->v[2]);
  float D = sqrt(R*R+r*r);
  float c = R / D;
  float s = r / D;
  float x, y, z;
  if (r > 0.00001) {
    x = v->v[0] / r;
    y = v->v[1] / r;
    z = v->v[2] / r;
  } else {
    x = 0;
    y = 0;
    z = 0;
  }
  float t[16] = {
    1-x*x*(1-c), -(1-c)*x*y,  -(1-c)*x*z,  s*x,
    -(1-c)*x*y,  1-y*y*(1-c), -(1-c)*y*z,  s*y,
    -(1-c)*x*z,  -(1-c)*y*z,  1-z*z*(1-c), s*z,
    -s*x,        -s*y,        -s*z,        c
  };
  for (int i = 0; i < 16; ++i) {
    m->m[i] = t[i];
  }
}

// helper functions

float vlength4(struct vec4 *x) {
  float v = 0;
  for (int i = 0; i < 4; ++i) {
    v += x->v[i] * x->v[i];
  }
  return sqrt(v);
}

float vdistance4(struct vec4 *x, struct vec4 *y) {
  struct vec4 d;
  vvsub4(&d, x, y);
  return vlength4(&d);
}

float vvdot4(struct vec4 *x, struct vec4 *y) {
  float v = 0;
  for (int i = 0; i < 4; ++i) {
    v += x->v[i] * y->v[i];
  }
  return v;
}

// debugging

void vprint4(struct vec4 *m) {
  fprintf(stderr, "%f %f %f %f\n", m->v[0], m->v[1], m->v[2], m->v[3]);
}

void mprint4(struct mat4 *m) {
  fprintf(
    stderr, "%f %f %f %f\n%f %f %f %f\n%f %f %f %f\n%f %f %f %f\n",
    m->m[ 0], m->m[ 1], m->m[ 2], m->m[ 3],
    m->m[ 4], m->m[ 5], m->m[ 6], m->m[ 7],
    m->m[ 8], m->m[ 9], m->m[10], m->m[11],
    m->m[12], m->m[13], m->m[14], m->m[15]
  );
}

// EOF
