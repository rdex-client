/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2009,2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
doubly-linked list implementation
===================================================================== */

#ifndef LIST_H
#define LIST_H 1

struct node {
  struct node *next;
  struct node *pred;
};

struct list {
  struct node *head;
  struct node *tail;
  // private
  struct node headNode;
  struct node tailNode;
};

void list_init(struct list *l);
int list_ishead(struct node *n);
int list_istail(struct node *n);
int list_isempty(struct list *l);
int list_length(struct list *l);
void list_remove(struct node *n);
void list_insertbefore(struct node *n, struct node *beforethis);
void list_insertafter(struct node *n, struct node *afterthis);
void list_inserttail(struct list *l, struct node *n);
struct node *list_removehead(struct list *l);

#endif

