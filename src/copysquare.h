/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Copy And Square Shader
===================================================================== */

#ifndef COPYSQUARE_H
#define COPYSQUARE_H 1

#include "shader.h"

//======================================================================
// copysquare shader data
struct copysquare { struct shader shader;
  struct { GLint texture; } uniform;
  struct { int   texture; } value;
  GLuint texture;
  int width;
  int height;
};

//======================================================================
// prototypes
struct copysquare *copysquare_init(struct copysquare *copysquare);
void copysquare_reshape(struct copysquare *copysquare, int w, int h);
void copysquare_display(
  struct copysquare *copysquare, GLuint fbo, GLuint texture
);
void copysquare_idle(struct copysquare *copysquare);

#endif
