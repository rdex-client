/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010,2011  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Text module
===================================================================== */

#include <math.h>
#include <GL/glew.h>
#include <GL/glut.h>
#include "text.h"
#include "text.vert.c"
#include "text.frag.c"

struct text *text_init(struct text *text) {
  if (! shader_init(&text->shader, text_vert, text_frag)) {
    return 0;
  }
  shader_uniform(text, texture);
  fonts_init(&text->fonts);
  for (int i = 0; i < TEXT_LENGTH; ++i) {
    text->textstr[i] = ' ';
  }
  return text;
}

void text_reshape(struct text *text, int w, int h) {
  text->width = w;
  text->height = h;
}

void text_display(struct text *text) {
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glViewport(0, 0, text->width, text->height);
  gluOrtho2D(0, text->width, 0, text->height);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  // text
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, text->fonts.texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glUseProgramObjectARB(text->shader.program);
  text->value.texture = 0;
  shader_updatei(text, texture);
  glBegin(GL_QUADS); {
    for (int j = 0; j < TEXT_LENGTH; ++j) {
      float x0 = text->width * (j + 0.5) / TEXT_LENGTH;
      float y0 = text->height * 0.75 - text->width * 0.5 / TEXT_LENGTH;
      float dx = text->width * 0.5 / TEXT_LENGTH;
      float dy = text->width * 0.5 / TEXT_LENGTH;
      int i = text->textstr[j];
      if ('a' <= i && i <= 'z') {
        i -= 'a';
      } else if ('A' <= i && i <= 'Z') {
        i -= 'A';
      } else {
        i = 255;
      }
      glTexCoord4f( (i % text->fonts.pack)      / (float) text->fonts.pack, ((i / text->fonts.pack) + 1) / (float) text->fonts.pack, 0, 1);
      glVertex2f(x0 - dx, y0 - dy);
      glTexCoord4f(((i % text->fonts.pack) + 1) / (float) text->fonts.pack, ((i / text->fonts.pack) + 1) / (float) text->fonts.pack, 1, 1);
      glVertex2f(x0 + dx, y0 - dy);
      glTexCoord4f(((i % text->fonts.pack) + 1) / (float) text->fonts.pack,  (i / text->fonts.pack)      / (float) text->fonts.pack, 1, 0);
      glVertex2f(x0 + dx, y0 + dy);
      glTexCoord4f( (i % text->fonts.pack)      / (float) text->fonts.pack,  (i / text->fonts.pack)      / (float) text->fonts.pack, 0, 0);
      glVertex2f(x0 - dx, y0 + dy);
    }
  } glEnd();
  glUseProgramObjectARB(0);
  glBindTexture(GL_TEXTURE_2D, 0);
  glDisable(GL_TEXTURE_2D);
}

int text_keynormal(struct text *text, int key, int x, int y) {
  if (('A' <= key && key <= 'Z') || ('a' <= key && key <= 'z') || (key == ' ')) {
    for (int i = 1; i < TEXT_LENGTH; ++i) {
      text->textstr[i-1] = text->textstr[i];
    }
    text->textstr[TEXT_LENGTH-1] = key;
    return 1;
  } else if (('\r' == key) || ('\n' == key)) {
    for (int i = 0; i < TEXT_LENGTH; ++i) {
      text->textstr[i] = ' ';
    }
    return 1;
  } else if ('\b' == key) {
    for (int i = TEXT_LENGTH - 1; i >= 1; --i) {
      text->textstr[i] = text->textstr[i-1];
    }
    text->textstr[0] = ' ';
    return 1;
  } else {
    return 0;
  }
}

int text_keyspecial(struct text *text, int key, int x, int y) {
  return 0;
}
