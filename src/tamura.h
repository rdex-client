/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Based on: fire-2.2/FeatureExtractors/tamurafeature.cpp
Homepage: http://www-i6.informatik.rwth-aachen.de/~deselaers/fire/
------------------------------------------------------------------------
Tumara Feature Extraction
===================================================================== */

#ifndef TAMURA_H
#define TAMURA_H

#include "image.h"

struct tamura_features {
  float coarseness;
  float contrast;
  float directionality;
};

struct tamura {
  int width;
  int height;
  struct image *Rsum;  // w*h*1
  struct image *Ak;    // w*h*5
  struct image *Ekh;   // w*h*5
  struct image *Ekv;   // w*h*5
  struct image *Sbest; // w*h*1
};

struct tamura *tamura_init(
  struct tamura *tamura, int width, int height
);

void tamura_calculate(
  struct tamura *tamura,
  struct tamura_features *features,
  struct image *image
);

#endif
