/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2010,2011  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Border/Window Shader
===================================================================== */

#ifndef BORDERWINDOW_H
#define BORDERWINDOW_H 1

#include "shader.h"

//======================================================================
// borderwindow shader data
struct borderwindow { struct shader shader;
  struct {
    GLint origin, scale, rotate, translate, near, far; // vertex
    GLint texture, pack, size; // fragment
  } uniform;
  struct {
    float origin[4], scale[4], rotate[16], translate[4], near[4], far[4]; // vertex
    int texture; float pack, size; // fragment
  } value;
  struct {
    GLint delta, spin, pick; // vertex
  } attr;
};

struct borderwindow_pick { struct shader shader;
  struct {
    GLint origin, scale, rotate, translate, near, far; // vertex
  } uniform;
  struct {
    float origin[4], scale[4], rotate[16], translate[4], near[4], far[4]; // vertex
  } value;
  struct {
    GLint delta; // vertex
  } attr;
};

//======================================================================
// prototypes
struct borderwindow *borderwindow_init(struct borderwindow *borderwindow);
void borderwindow_use(struct borderwindow *borderwindow);

struct borderwindow_pick *borderwindow_pick_init(struct borderwindow_pick *borderwindow_pick);
void borderwindow_pick_use(struct borderwindow_pick *borderwindow_pick);

#endif
