/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>
===================================================================== */

uniform sampler2D texture;
uniform vec2 d;

void main(void) {
  vec2 p = gl_TexCoord[0].st;
  vec4 x00 = texture2D(texture, p);
  vec4 x01 = texture2D(texture, p + vec2(0.0,d.y));
  vec4 x10 = texture2D(texture, p + vec2(d.x,0.0));
  vec4 x11 = texture2D(texture, p + vec2(d.x,d.y));
  vec2 p0 = floor(p / d) * d;
  vec2 dp = (p - p0) / d;
  vec3 tl = texture2D(texture, p0).rgb;
  vec3 tr = texture2D(texture, p0 + vec2(d.x, 0.0)).rgb;
  vec3 bl = texture2D(texture, p0 + vec2(0.0, d.y)).rgb;
  vec3 br = texture2D(texture, p0 + vec2(d.x, d.y)).rgb;
  vec3 t = tl * (1.0 - dp.x) + dp.x * tr;
  vec3 b = bl * (1.0 - dp.x) + dp.x * br;
  vec3 l = tl * (1.0 - dp.y) + dp.y * bl;
  vec3 r = tr * (1.0 - dp.y) + dp.y * br;
  vec3 m1 = t * (1.0 - dp.y) + dp.y * b;
  vec3 m2 = l * (1.0 - dp.x) + dp.x * r;
  vec3 m = 0.5 * (m1 + m2);
  gl_FragColor = vec4(m, 1.0);
}
