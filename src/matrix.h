/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Matrix computations
===================================================================== */

#ifndef MATRIX_H
#define MATRIX_H 1

// data structures
struct vec3 { float v[3]; };
struct vec4 { float v[4]; };
struct vec5 { float v[5]; };
struct mat3 { float m[3*3]; };
struct mat4 { float m[4*4]; };
struct mat5 { float m[5*5]; };

// copying
void vcopy3(struct vec3 *m, struct vec3 *x);
void vcopy4(struct vec4 *m, struct vec4 *x);
void vcopy5(struct vec5 *m, struct vec5 *x);
void mcopy3(struct mat3 *m, struct mat3 *x);
void mcopy4(struct mat4 *m, struct mat4 *x);
void mcopy5(struct mat5 *m, struct mat5 *x);

// basic operations
void vsmul3(struct vec3 *m, struct vec3 *x, float y);
void vsmul4(struct vec4 *m, struct vec4 *x, float y);
void vsmul5(struct vec5 *m, struct vec5 *x, float y);
void vsdiv3(struct vec3 *m, struct vec3 *x, float y);
void vsdiv4(struct vec4 *m, struct vec4 *x, float y);
void vsdiv5(struct vec5 *m, struct vec5 *x, float y);

void vvadd3(struct vec3 *m, struct vec3 *x, struct vec3 *y);
void vvadd4(struct vec4 *m, struct vec4 *x, struct vec4 *y);
void vvadd5(struct vec5 *m, struct vec5 *x, struct vec5 *y);
void vvsub3(struct vec3 *m, struct vec3 *x, struct vec3 *y);
void vvsub4(struct vec4 *m, struct vec4 *x, struct vec4 *y);
void vvsub5(struct vec5 *m, struct vec5 *x, struct vec5 *y);

void vvmul4(struct vec4 *m, struct vec4 *x, struct vec4 *y);
void vvdiv4(struct vec4 *m, struct vec4 *x, struct vec4 *y);

void mvmul3(struct vec3 *v, struct mat3 *x, struct vec3 *y);
void mvmul4(struct vec4 *v, struct mat4 *x, struct vec4 *y);
void mvmul5(struct vec5 *v, struct mat5 *x, struct vec5 *y);

void mmmul3(struct mat3 *m, struct mat3 *x, struct mat3 *y);
void mmmul4(struct mat4 *m, struct mat4 *x, struct mat4 *y);
void mmmul5(struct mat5 *m, struct mat5 *x, struct mat5 *y);

// homogeneous matrix construction
void identity3(struct mat3 *m);
void identity4(struct mat4 *m);
void identity5(struct mat5 *m);
void homogen4(struct mat4 *m, struct mat3 *x);
void homogen5(struct mat5 *m, struct mat4 *x);
void translate4(struct mat4 *m, struct vec3 *v);
void translate5(struct mat5 *m, struct vec4 *v);
void rollingball4(struct mat4 *m, float R, struct vec3 *v);

// helper functions
float vlength4(struct vec4 *x);
float vdistance4(struct vec4 *x, struct vec4 *y);
float vvdot4(struct vec4 *x, struct vec4 *y);

// debugging
void vprint4(struct vec4 *m);
void mprint4(struct mat4 *m);

#endif
