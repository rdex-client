/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2009,2010,2019  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Library Module
===================================================================== */

#ifndef LIBRARY_H
#define LIBRARY_H 1

#include <GL/glew.h>
#include "borderwindow.h"
#include "worldsphere.h"
#include "background.h"
#include "matrix.h"
#include "camera.h"
//#include "bloom.h"
#include "blur.h"

//======================================================================
// library data structures

#define library_tex_size 256
#define library_tex_layers 512

struct libelem {
  GLfloat v[4];       // raw parameter coordinates
  float score;        // analysis result
  float spin[2];      // spinning orbs
  float dspin[2];
};

struct libvert {
  GLfloat vertex[4];
  GLfloat texcoord[4];
  GLfloat delta[2];
  GLfloat spin[2];
};

struct library {
  int loaded;
  int count;          // invariant: count <= library_tex_layers
  struct libelem array[library_tex_layers];
  double score, score2, scorev[4], scorev2[4];
  GLfloat stddev[4];
  GLuint texture, texture_array;
  GLuint pbo;
  int which;
  unsigned int frame;
  int width;
  int height;
  int mousex;
  int mousey;
  GLuint picktex;
  int hover;
  int picked;
  int snap;
  char *session;
  struct borderwindow borderwindow;
  struct borderwindow_pick borderwindow_pick;
  struct worldsphere worldsphere;
  struct background background;
  struct camera camera;
  struct vec4 target;
//  struct bloom bloom;
  struct blur blur;
  GLuint blurtex[1]; // FIXME need log2(rdex_tex_size / library_tex_size)-1
//  GLuint balltex;
  enum { library_state_idle,
         library_state_intro,
         library_state_active,
         library_state_outro,
         library_state_done } state;
  double phase;
  // draw arrays stuff
  struct libvert varray[library_tex_layers][4];
};

//======================================================================
// prototypes

struct library *library_init(struct library *library, GLuint fbo);
int library_load(struct library *library, GLuint fbo);
void library_display_save(
  struct library *library, GLuint fbo, GLuint texture, GLuint rawtexture,
  double ru, double rv, double f, double k, double score, const char *behaviour
);
void library_reshape(struct library *library, int w, int h);
void library_display(struct library *library, GLuint fbo, GLuint texture);
void library_pmotion(struct library *library, int x, int y);
void library_amotion(struct library *library, int x, int y);
void library_mouse(struct library *library, int button, int state, int x, int y);
void library_idle(struct library *library);
int library_pick(struct library *library, float *ru, float *rv, float *f, float *k, float randomness);

#endif
