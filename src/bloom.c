/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Bloom/Glow Effect
===================================================================== */

#include "util.h"
#include "bloom.h"
#include "bloom.frag.c"

//======================================================================
// initialization
struct bloom *bloom_init(struct bloom *bloom) {
  if (! bloom) { return 0; }
  if (! gblur_init(&bloom->gblur)) { return 0; }
  if (! shader_init(&bloom->shader, 0, bloom_frag)) { return 0; }
  shader_uniform(bloom, tex0);
  shader_uniform(bloom, tex1);
  shader_uniform(bloom, tex2);
  shader_uniform(bloom, tex3);
  shader_uniform(bloom, tex4);
  shader_uniform(bloom, tex5);
  shader_uniform(bloom, tex6);
  shader_uniform(bloom, tex7);
  bloom->value.tex0 = 0;
  bloom->value.tex1 = 1;
  bloom->value.tex2 = 2;
  bloom->value.tex3 = 3;
  bloom->value.tex4 = 4;
  bloom->value.tex5 = 5;
  bloom->value.tex6 = 6;
  bloom->value.tex7 = 7;
  return bloom;
}

//======================================================================
// reshape callback
void bloom_reshape(struct bloom *bloom, int w, int h) {
  gblur_reshape(&bloom->gblur, w, h);
}

//======================================================================
// display callback
void bloom_display(struct bloom *bloom, GLuint fbo, GLuint texture) {
  // generate blurred copies
  gblur_display(&bloom->gblur, fbo, texture);
  // combine them (back into the input texture)
  glEnable(GL_TEXTURE_2D);
  //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
  glViewport(0, 0, 1<<bloom->gblur.count, 1<<bloom->gblur.count);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, 1, 0, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, texture, 0);
  glActiveTexture(GL_TEXTURE7); glBindTexture(GL_TEXTURE_2D, bloom->gblur.textures[0][bloom->gblur.count - 7]);
  glActiveTexture(GL_TEXTURE6); glBindTexture(GL_TEXTURE_2D, bloom->gblur.textures[0][bloom->gblur.count - 6]);
  glActiveTexture(GL_TEXTURE5); glBindTexture(GL_TEXTURE_2D, bloom->gblur.textures[0][bloom->gblur.count - 5]);
  glActiveTexture(GL_TEXTURE4); glBindTexture(GL_TEXTURE_2D, bloom->gblur.textures[0][bloom->gblur.count - 4]);
  glActiveTexture(GL_TEXTURE3); glBindTexture(GL_TEXTURE_2D, bloom->gblur.textures[0][bloom->gblur.count - 3]);
  glActiveTexture(GL_TEXTURE2); glBindTexture(GL_TEXTURE_2D, bloom->gblur.textures[0][bloom->gblur.count - 2]);
  glActiveTexture(GL_TEXTURE1); glBindTexture(GL_TEXTURE_2D, bloom->gblur.textures[0][bloom->gblur.count - 1]);
  glActiveTexture(GL_TEXTURE0); glBindTexture(GL_TEXTURE_2D, bloom->gblur.textures[0][bloom->gblur.count - 0]);
  glUseProgramObjectARB(bloom->shader.program);
  shader_updatei(bloom, tex0);
  shader_updatei(bloom, tex1);
  shader_updatei(bloom, tex2);
  shader_updatei(bloom, tex3);
  shader_updatei(bloom, tex4);
  shader_updatei(bloom, tex5);
  shader_updatei(bloom, tex6);
  shader_updatei(bloom, tex7);
  glBegin(GL_QUADS); { glColor4f(1,1,1,1);
    glTexCoord2f(0, 0); glVertex2f(0, 0);
    glTexCoord2f(1, 0); glVertex2f(1, 0);
    glTexCoord2f(1, 1); glVertex2f(1, 1);
    glTexCoord2f(0, 1); glVertex2f(0, 1);
  } glEnd();
  // clean up
  glUseProgramObjectARB(0);
  //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  glActiveTexture(GL_TEXTURE7); glBindTexture(GL_TEXTURE_2D, 0);
  glActiveTexture(GL_TEXTURE6); glBindTexture(GL_TEXTURE_2D, 0);
  glActiveTexture(GL_TEXTURE5); glBindTexture(GL_TEXTURE_2D, 0);
  glActiveTexture(GL_TEXTURE4); glBindTexture(GL_TEXTURE_2D, 0);
  glActiveTexture(GL_TEXTURE3); glBindTexture(GL_TEXTURE_2D, 0);
  glActiveTexture(GL_TEXTURE2); glBindTexture(GL_TEXTURE_2D, 0);
  glActiveTexture(GL_TEXTURE1); glBindTexture(GL_TEXTURE_2D, 0);
  glActiveTexture(GL_TEXTURE0); glBindTexture(GL_TEXTURE_2D, 0);
  glDisable(GL_TEXTURE_2D);
}

// EOF
