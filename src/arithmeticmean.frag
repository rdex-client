/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
===================================================================== */

uniform sampler2D texture; // all four channels considered
uniform float dx;          // horizontal distance between source texels
uniform float dy;          // vertical distance between source texels

void main(void) {
  vec2 p = gl_TexCoord[0].st;
  vec4 x00 = texture2D(texture, p);               // box of 4 texels
  vec4 x01 = texture2D(texture, p + vec2(0.0,dy));
  vec4 x10 = texture2D(texture, p + vec2(dx,0.0));
  vec4 x11 = texture2D(texture, p + vec2(dx,dy));
  gl_FragColor = 0.25*(x00+x01+x10+x11);          // arithmetic mean
}
