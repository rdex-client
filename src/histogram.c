/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2017  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
CPU-Based Image Histograms
===================================================================== */

#include "histogram.h"

void histogram_calc(struct histogram *h, const struct image *i) {
  for (int r = 0; r < HISTOGRAM_BUCKETS; ++r) {
  for (int g = 0; g < HISTOGRAM_BUCKETS; ++g) {
  for (int b = 0; b < HISTOGRAM_BUCKETS; ++b) {
    h->bucket[r][g][b] = 0;
  }
  }
  }
  for (int y = 0; y < i->height; ++y) {
  for (int x = 0; x < i->width; ++x) {
    int r = HISTOGRAM_BUCKETS * I(i,x,y,0);
    int g = HISTOGRAM_BUCKETS * I(i,x,y,1);
    int b = HISTOGRAM_BUCKETS * I(i,x,y,2);
    if (r < 0) r = 0;
    if (r >= HISTOGRAM_BUCKETS) r = HISTOGRAM_BUCKETS - 1;
    if (g < 0) g = 0;
    if (g >= HISTOGRAM_BUCKETS) g = HISTOGRAM_BUCKETS - 1;
    if (b < 0) b = 0;
    if (b >= HISTOGRAM_BUCKETS) b = HISTOGRAM_BUCKETS - 1;
    h->bucket[r][g][b] += 1;
  }
  }
  float pixels = i->width * i->height;
  for (int r = 0; r < HISTOGRAM_BUCKETS; ++r) {
  for (int g = 0; g < HISTOGRAM_BUCKETS; ++g) {
  for (int b = 0; b < HISTOGRAM_BUCKETS; ++b) {
    h->bucket[r][g][b] /= pixels;
  }
  }
  }
}
