/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010,2011  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Intro module
===================================================================== */

#include <math.h>
#include <GL/glew.h>
#include <GL/glut.h>
#include "intro.h"

extern char _binary_intro_image_rgba_start;

struct intro *intro_init(struct intro *intro) {
  glGenTextures(1, &intro->tex);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, intro->tex);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, INTRO_IMAGE_SIZE, INTRO_IMAGE_SIZE, 0, GL_RGBA, GL_UNSIGNED_BYTE, &_binary_intro_image_rgba_start);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glBindTexture(GL_TEXTURE_2D, 0);
  glDisable(GL_TEXTURE_2D);
  intro->state = intro_idle;
  intro->phase = 0;
  return intro;
}

void intro_reshape(struct intro *intro, int w, int h) {
  intro->width = w;
  intro->height = h;
}

void intro_display(struct intro *intro, double dt) { /* FIXME aspect ratio? */
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  switch (intro->state) {
    case intro_idle:
    case intro_active:
      intro->rx = 0;//32.0 * exp(-32.0 * (intro->phase * intro->phase) / (INTRO_TIME * INTRO_TIME));
      intro->ry = 0;//32.0 * exp(-16.0 * (intro->phase * intro->phase) / (INTRO_TIME * INTRO_TIME));
      glMatrixMode(GL_PROJECTION);
      glLoadIdentity();
      glViewport(0, 0, intro->width, intro->height);
      gluOrtho2D(0, 1, 0, 1);
      glMatrixMode(GL_MODELVIEW);
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      glEnable(GL_TEXTURE_2D);
      glBindTexture(GL_TEXTURE_2D, intro->tex);
      glBegin(GL_QUADS); {
        glColor4f(1,1,1,1);
        glTexCoord2f(0+intro->rx, 1+intro->ry); glVertex2f(0, 0);
        glTexCoord2f(0+intro->rx, 0+intro->ry); glVertex2f(0, 1);
        glTexCoord2f(1+intro->rx, 0+intro->ry); glVertex2f(1, 1);
        glTexCoord2f(1+intro->rx, 1+intro->ry); glVertex2f(1, 0);
      } glEnd();
      glDisable(GL_TEXTURE_2D);
      if (intro->state == intro_active) {
        intro->phase += dt;
        if (! (intro->phase < INTRO_TIME)) {
          intro->state = intro_done;
          intro->phase = 0;
        }
      }
      break;
    default:
      break;
  }
}

void intro_start(struct intro *intro) {
  if (intro->state == intro_idle) {
    intro->state = intro_active;
    intro->phase = 0;
  }
}
