/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Blur shader
===================================================================== */

#ifndef BLUR_H
#define BLUR_H 1

#include "shader.h"

//======================================================================
// arithmetic mean shader data
struct blur { struct shader shader;
  struct { GLint texture; GLint d; } uniform;
  struct { int   texture; float d[2]; } value;
};

//======================================================================
// prototypes
struct blur *blur_init(struct blur *blur);

#endif
