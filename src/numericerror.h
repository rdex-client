/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Numeric Error Shader
===================================================================== */

#ifndef NUMERICERROR_H
#define NUMERICERROR_H 1

#include "arithmeticmean.h"

//======================================================================
// numeric error shader data
struct numericerror { struct shader shader;
  struct { GLint texture; GLint dx; GLint dy; } uniform;
  struct { int   texture; float dx; float dy; } value;
  struct arithmeticmean arithmeticmean;
  GLuint texture;
  int width;
  int height;
  int calc;
  float mean;
};

//======================================================================
// prototypes
struct numericerror *numericerror_init(
  struct numericerror *numericerror
);
void numericerror_reshape(
  struct numericerror *numericerror, int w, int h
);
void numericerror_display(
  struct numericerror *numericerror, GLuint fbo, GLuint texture
);
void numericerror_idle(struct numericerror *numericerror);

#endif
