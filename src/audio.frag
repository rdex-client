/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Extract from input texture using a coordinate texture
===================================================================== */

uniform sampler2D texture;
uniform sampler2D coords;

/*
uniform vec2 texsize;
uniform vec2 center;
uniform float radius;
uniform float size;
*/

void main(void) {
  gl_FragColor = texture2D(texture, texture2D(coords, gl_TexCoord[0].st).rg);
/*
  vec2 p = gl_TexCoord[0].st;
  float k = texsize.x * texsize.y / size;
  float da = 2.0 * 3.1415926 / size;
  float a = (p.y + p.x / texsize.x) * 2.0 * 3.1415926 * k;
  vec4 c = vec4(0.0, 0.0, 0.0, 0.0);
  vec2 q;
  q = center + radius * vec2(cos(a), sin(a));
  c += texture2D(texture, q);
  a += da;
  q = center + radius * vec2(cos(a), sin(a));
  c += texture2D(texture, q);
  gl_FragColor = c * 0.5;
*/
}
