/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2009  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
pthread-based fifo
===================================================================== */

#ifndef PFIFO_H
#define PFIFO_H 1

#include <stdlib.h>
#include <pthread.h>
#include "list.h"

typedef void (pfifo_consumer)(void *, size_t, void *);

struct pfifo_node {
  struct node node;
  size_t length;
  void *data;
};

struct pfifo {
  pfifo_consumer *consumer;
  void *consumerdata;
  struct list list;
  pthread_t thread;
  pthread_mutex_t *mutex;
  pthread_cond_t *nonempty;
};

struct pfifo *pfifo_create(pfifo_consumer *consumer, void *consumerdata);
void pfifo_destroy(struct pfifo *fifo);
void pfifo_enqueue(struct pfifo *fifo, size_t length, const void *data);

#endif
