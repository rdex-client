/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Multi-scale Gaussian Blur
===================================================================== */

#include "util.h"
#include "gblur.h"
#include "gblur.frag.c"

//======================================================================
// initialization
struct gblur *gblur_init(struct gblur *gblur) {
  if (! gblur) { return 0; }
  if (! shader_init(&gblur->shader, 0, gblur_frag)) {
    return 0;
  }
  if (! expcolour_init(&gblur->expcolour)) {
    return 0;
  }
  shader_uniform(gblur, texture);
  shader_uniform(gblur, d);
  gblur->value.texture = 0;
  gblur->value.d[0] = 0;
  gblur->value.d[1] = 0;
  glGenTextures(gblur_tex_max, &gblur->textures[0][0]);
  glGenTextures(gblur_tex_max, &gblur->textures[1][0]);
  glEnable(GL_TEXTURE_2D);
  for (int i = 0; i < gblur_tex_max; ++i) {
    for (int c = 0; c < 2; ++c) {
      glBindTexture(GL_TEXTURE_2D, gblur->textures[c][i]);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    }
  }
  glDisable(GL_TEXTURE_2D);
  return gblur;
}

//======================================================================
// reshape callback
void gblur_reshape(struct gblur *gblur, int w, int h) {
  // allocate textures
  int w2 = roundtwo(w);
  int h2 = roundtwo(h);
  int d = w2>h2 ? w2 : h2;
  gblur->count = logtwo(d);
  glEnable(GL_TEXTURE_2D);
  for (int i = 0; i <= gblur->count; ++i) {
    for (int c = 0; c < 2; ++c) {
      glBindTexture(GL_TEXTURE_2D, gblur->textures[c][i]);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1<<i, 1<<i, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
      //glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F_ARB, 1<<i, 1<<i, 0, GL_RGBA, GL_FLOAT, 0);
    }
  }
  glDisable(GL_TEXTURE_2D);
}

//======================================================================
// display callback
void gblur_display(struct gblur *gblur, GLuint fbo, GLuint texture) {
  // copy texture
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, texture);
  //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, 1, 0, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glViewport(0, 0, 1<<gblur->count, 1<<gblur->count);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, gblur->textures[0][gblur->count], 0);
  glUseProgramObjectARB(gblur->expcolour.shader.program);
  shader_updatei(&gblur->expcolour, texture);
  glBegin(GL_QUADS); { glColor4f(1,1,1,1);
    glTexCoord2f(0, 0); glVertex2f(0, 0);
    glTexCoord2f(1, 0); glVertex2f(1, 0);
    glTexCoord2f(1, 1); glVertex2f(1, 1);
    glTexCoord2f(0, 1); glVertex2f(0, 1);
  } glEnd();
  // multi-scale blur
  glUseProgramObjectARB(gblur->shader.program);
  for (int i = gblur->count - 1; i >= 0; --i) {
    // blur horizontally
    glBindTexture(GL_TEXTURE_2D, gblur->textures[0][i+1]);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, 1, 0, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glViewport(0, 0, 1<<(i+1), 1<<(i+1));
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, gblur->textures[1][i+1], 0);
    gblur->value.d[0] = 1.0 / (1<<(i+1));
    gblur->value.d[1] = 0.0;
    shader_updatei(gblur, texture);
    shader_updatef2(gblur, d);
    glBegin(GL_QUADS); { glColor4f(1,1,1,1);
      glTexCoord2f(0, 0); glVertex2f(0, 0);
      glTexCoord2f(1, 0); glVertex2f(1, 0);
      glTexCoord2f(1, 1); glVertex2f(1, 1);
      glTexCoord2f(0, 1); glVertex2f(0, 1);
    } glEnd();
    // blur vertically (and downscale)
    glBindTexture(GL_TEXTURE_2D, gblur->textures[1][i+1]);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, 1, 0, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glViewport(0, 0, 1<<i, 1<<i);
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, gblur->textures[0][i], 0);
    gblur->value.d[0] = 0.0;
    gblur->value.d[1] = 1.0 / (1<<(i+1));
    shader_updatei(gblur, texture);
    shader_updatef2(gblur, d);
    glBegin(GL_QUADS); { glColor4f(1,1,1,1);
      glTexCoord2f(0, 0); glVertex2f(0, 0);
      glTexCoord2f(1, 0); glVertex2f(1, 0);
      glTexCoord2f(1, 1); glVertex2f(1, 1);
      glTexCoord2f(0, 1); glVertex2f(0, 1);
    } glEnd();
  }
  // clean up
  glUseProgramObjectARB(0);
  //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  glBindTexture(GL_TEXTURE_2D, 0);
  glDisable(GL_TEXTURE_2D);
}

// EOF
