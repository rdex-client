/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Sphere shader for sequencer
===================================================================== */

uniform sampler2D texture;
uniform float pack;
uniform float size;

varying vec2 myspin2;
varying float mycursor2;
//varying float mystep2;
varying float myactive2;
varying float mypitch2;

void main(void) {
  vec2 p = gl_TexCoord[0].xy;               // float in [0..1]
  vec2 q0 = gl_TexCoord[0].zw;
  vec2 q1 = 3.0 * (q0 - vec2(0.5, 0.5));
  float d = dot(q1,q1);
  if (d < 1.0 && 0.0 < myactive2) {
    vec3 n = normalize(vec3(q1, sqrt(1.0-d)));
    vec2 q = q1 / (1.0 + sqrt(1.0 - d));
    vec2 q2 = 0.5 * q + myspin2;
    vec2 cell = floor(p * pack);             // int   in [0..(pack-1)]
    //vec2 frac = p * pack - cell;             // float in [0..1]
    //vec2 frac = vec2(0.0, 0.0);
    vec2 frac = q2;
    frac -= floor(frac);
    // FIXME check the ratio - maybe (size/pack) / (size/pack + 2) instead?
    frac -= vec2(0.5, 0.5);
    frac *= (size / pack - 2.0) / (size / pack);
    frac += vec2(0.5, 0.5);
    vec2 px0 = floor(  frac * size / pack ); // pixels
    vec2 px1 = px0 + vec2(1.0, 1.0);         // pixels
    // coordinates to interpolate
    vec2 p0 = cell / pack + px0 / size;
    vec2 p1 = cell / pack + px1 / size;
    vec2 dp = frac;
    // linear interpolation
    vec3 tl = texture2D(texture, p0).rgb;
    vec3 tr = texture2D(texture, vec2(p1.x, p0.y)).rgb;
    vec3 bl = texture2D(texture, vec2(p0.x, p1.y)).rgb;
    vec3 br = texture2D(texture, p1).rgb;
    vec3 t = tl * (1.0 - dp.x) + dp.x * tr;
    vec3 b = bl * (1.0 - dp.x) + dp.x * br;
    vec3 l = tl * (1.0 - dp.y) + dp.y * bl;
    vec3 r = tr * (1.0 - dp.y) + dp.y * br;
    vec3 m1 = t * (1.0 - dp.y) + dp.y * b;
    vec3 m2 = l * (1.0 - dp.x) + dp.x * r;
    vec3 m = 0.5 * (m1 + m2);
    // output
    float a = 1.0;
    float k = gl_FragCoord.z - 0.1 * cos(sqrt(d) * 3.1415926*0.5);
    float c = 0.0625 + pow(dot(vec3(0.0, 0.0, 1.0), n), 2.0);
    gl_FragDepth = k;
    gl_FragColor = vec4(m * c, a);
  } else if (abs(d - 1.3) < 0.3 && 0.0 < mycursor2) {
    float c = 1.0 - abs(d - 1.3) / 0.3;
    float a = fract((atan(q1.y, q1.x) / (3.1415926 * 2.0) + 0.5) * mypitch2) < 0.5 ? 1.0 : 0.0;
    gl_FragDepth = gl_FragCoord.z;
    gl_FragColor = vec4(0.0, a * c*c, 0.0, c*c);
  } else {
    discard;
  }
}
