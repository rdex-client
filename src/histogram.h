/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
CPU-Based Image Histograms
===================================================================== */

#ifndef HISTOGRAM_H
#define HISTOGRAM_H 1

#include "image.h"

#define HISTOGRAM_BUCKETS 8
struct histogram {
  float bucket[HISTOGRAM_BUCKETS][HISTOGRAM_BUCKETS][HISTOGRAM_BUCKETS];
};

void histogram_calc(struct histogram * h, const struct image *i);

#endif
