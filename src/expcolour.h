/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Exponential colour space conversion
===================================================================== */

#ifndef EXPCOLOUR_H
#define EXPCOLOUR_H 1

#include "shader.h"

//======================================================================
// shader data
struct expcolour { struct shader shader;
  struct { GLint texture; } uniform;
  struct { int   texture; } value;
};

//======================================================================
// prototypes
struct expcolour *expcolour_init(struct expcolour *expcolour);

#endif
