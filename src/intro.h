/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010,2011  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Intro module
===================================================================== */

#ifndef INTRO_H
#define INTRO_H 1

#include <GL/glew.h>
#include <GL/glut.h>

#define INTRO_TIME 5.0
#define INTRO_IMAGE_SIZE 1024

struct intro {
  GLuint tex;
  int width;
  int height;
  enum { intro_idle, intro_active, intro_done } state;
  double phase;
  double rx;
  double ry;
};

struct intro *intro_init(struct intro *intro);
void intro_reshape(struct intro *intro, int w, int h);
void intro_display(struct intro *intro, double dt);
void intro_start(struct intro *intro);

#endif
