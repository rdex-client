/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010,2011  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Text vertex shader
===================================================================== */

void main(void) {
  gl_TexCoord[0] = gl_MultiTexCoord0;
  gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
