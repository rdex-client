/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Chase camera
===================================================================== */

#ifndef CAMERA_H
#define CAMERA_H 1

#include "matrix.h"

struct mass {
  struct vec4 position;
  struct vec4 velocity;
  struct vec4 force;
  float mass;
};

struct link {
  struct mass *p;
  struct mass *q;
  float length;
  float k;
  float c;
};

struct camera {
  struct mass T;
  struct mass V[8];
  struct link TV[8];
  struct link VV[28];
  struct vec4 C;
  struct mat4 M;
  float dt;
  float r;
  struct vec4 scale;
};

void camera_init(struct camera *camera);
void camera_update(struct camera *camera, struct vec4 *target);

#endif
