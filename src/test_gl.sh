#!/bin/bash
grep -h gl *.c | sed "s|.*gl\(.*\)(.*|gl\1|g" | grep -v "=" | grep -v "^#" | 
grep -v "^/" | sed "s|(.*||g" | grep -v "^gls_" | grep -v "^global_" |
grep -v "^glXGetCurrent" | grep -v " " | sort | uniq |
(echo "
#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <GL/glut.h>
static void test_display(void);
int main(int argc, char **argv) {
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(640, 480);
  glutCreateWindow(\"test_gl\");
  glewInit();
  glutDisplayFunc(test_display);
  glutMainLoop();
  return 0;
}
static void test_display(void) {
" &&
 cat | sed 's|\(.*\)|printf("\1\\t\\t\\t%p\\n", \1);|g' && echo "
  exit(0);
}
") | gcc -xc - -o test_gl -lGLEW -lGL -lGLU -lglut && ./test_gl | grep "(nil)"
