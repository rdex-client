/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
World sphere shader
===================================================================== */

#include "worldsphere.h"
#include "worldsphere.frag.c"

//======================================================================
// worldsphere shader initialization
struct worldsphere *worldsphere_init(struct worldsphere *worldsphere) {
  if (! worldsphere) { return 0; }
  if (! shader_init(&worldsphere->shader, 0, worldsphere_frag)) {
    return 0;
  }
  shader_uniform(worldsphere, texture);
  shader_uniform(worldsphere, rot);
  shader_uniform(worldsphere, size);
  worldsphere->value.texture = 0;
  worldsphere->value.rot[0] = 0;
  worldsphere->value.rot[1] = 0;
  return worldsphere;
}

//======================================================================
// worldsphere shader activation
void worldsphere_use(struct worldsphere *worldsphere) {
  if (worldsphere) {
    glUseProgramObjectARB(worldsphere->shader.program);
    shader_updatei(worldsphere, texture);
    shader_updatef2(worldsphere, rot);
    shader_updatef2(worldsphere, size);
  } else {
    glUseProgramObjectARB(0);
  }
}

void worldsphere_idle(struct worldsphere *worldsphere) {
  worldsphere->value.rot[0] += worldsphere->delta[0];
  worldsphere->value.rot[1] += worldsphere->delta[1];
  while (worldsphere->value.rot[0] <  0) worldsphere->value.rot[0] += 1;
  while (worldsphere->value.rot[1] <  0) worldsphere->value.rot[1] += 1;
  while (worldsphere->value.rot[0] >= 1) worldsphere->value.rot[0] -= 1;
  while (worldsphere->value.rot[1] >= 1) worldsphere->value.rot[1] -= 1;
}

// EOF
