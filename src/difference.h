/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Difference Shader
===================================================================== */

#ifndef DIFFERENCE_H
#define DIFFERENCE_H 1

#include "shader.h"
#include "arithmeticmean.h"

//======================================================================
// difference shader data
struct difference { struct shader shader;
  struct { GLint texture1; GLint texture2; } uniform;
  struct { int   texture1; GLint texture2; } value;
  struct arithmeticmean arithmeticmean;
  GLuint textures[2];
  int width;
  int height;
  int snap;
  int calc;
  float mean;
};

//======================================================================
// prototypes
struct difference *difference_init(struct difference *difference);
void difference_reshape(struct difference *difference, int w, int h);
void difference_display(
  struct difference *difference, GLuint fbo, GLuint texture
);
void difference_idle(struct difference *difference);

#endif
