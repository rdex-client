/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
===================================================================== */

uniform sampler2D texture;                        // (U,V,_,_)

void main(void) {
  vec2 p = gl_TexCoord[0].st;
  vec2 c = texture2D(texture, p).rg;
  gl_FragColor = vec4(c.r,c.r*c.r,c.g,c.g*c.g);   // (U,U^2,V,V^2)
}
