/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2011  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Text bitmap fonts
===================================================================== */

#ifndef FONTS_H
#define FONTS_H 1

#include <GL/glew.h>

struct fonts {
  GLuint texture;
  int size;
  int pack;
};

struct fonts *fonts_init(struct fonts *fonts);

#endif
