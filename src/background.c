/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2009,2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Background processing
===================================================================== */

#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>
#include "background.h"
#include "histogram.h"
#include "image.h"
#include "rdex.h"
#include "segment.h"
#include "tamura.h"

//======================================================================
// upload
#define UPLOAD_DATA_STRLEN 32
#define UPLOAD_DATA_PPMLEN (1024 + rdex_tex_size * rdex_tex_size * 3)
struct background_upload {
  char ru              [UPLOAD_DATA_STRLEN];
  char rv              [UPLOAD_DATA_STRLEN];
  char f               [UPLOAD_DATA_STRLEN];
  char k               [UPLOAD_DATA_STRLEN];
  char behaviour       [UPLOAD_DATA_STRLEN];
  char coarseness_u    [UPLOAD_DATA_STRLEN];
  char contrast_u      [UPLOAD_DATA_STRLEN];
  char directionality_u[UPLOAD_DATA_STRLEN];
  char coarseness_v    [UPLOAD_DATA_STRLEN];
  char contrast_v      [UPLOAD_DATA_STRLEN];
  char directionality_v[UPLOAD_DATA_STRLEN];
  char histogram[HISTOGRAM_BUCKETS][HISTOGRAM_BUCKETS][HISTOGRAM_BUCKETS][UPLOAD_DATA_STRLEN];
  char segment[SEGMENT_BUCKETS][UPLOAD_DATA_STRLEN];
  char ppm             [UPLOAD_DATA_PPMLEN];
  int  ppm_bytes;
};

//======================================================================
// background processing data
struct background_data {
  float ru; float rv; float f; float k; float score; const char *behaviour;
//  char *thumb_ppm; int thumb_ppm_bytes; // must be freed by background thread
  char *full_ppm; int full_ppm_bytes; // must be freed by background thread
  struct image *raw_image; // must be freed by background thread
  struct image *full_image; // must be freed by background thread
};

//======================================================================
// prototypes
void background_background(struct background *, size_t, struct background_data *);

//======================================================================
// initialise uploader
struct background *background_init(struct background *background, const char *session) {
  CURLcode c = curl_global_init(CURL_GLOBAL_ALL);
  if (c) { return 0; }
  background->session = session;
  background->url = getenv("RDEX_UPLOAD");
  background->queue = pfifo_create((pfifo_consumer *) background_background, background);
  tamura_init(&background->tamura, rdex_tex_size, rdex_tex_size); //FIXME
  return background;
}

//======================================================================
// clean up uploader
void background_atexit(struct background *background) {
  curl_global_cleanup();
}

//======================================================================
// prepare background processing
void background_enqueue(
  struct background *background,
  float ru, float rv, float f, float k, float score, const char *behaviour,
//  char *thumb_ppm, int thumb_ppm_bytes, // must be freed by background thread
  char *full_ppm, int full_ppm_bytes,  // must be freed by background thread
  struct image *raw_image, // must be freed by background thread
  struct image *full_image // must be freed by background thread
) {
  struct background_data data;
  data.ru = ru;
  data.rv = rv;
  data.f = f;
  data.k = k;
  data.score = score;
  data.behaviour = behaviour;
//  data.thumb_ppm = thumb_ppm;
//  data.thumb_ppm_bytes = thumb_ppm_bytes;
  data.full_ppm = full_ppm;
  data.full_ppm_bytes = full_ppm_bytes;
  data.raw_image = raw_image;
  data.full_image = full_image;
  pfifo_enqueue(background->queue, sizeof(struct background_data), &data);
}

//======================================================================
// do background processing
void background_background(
  struct background *background,
  size_t size,
  struct background_data *data
) {
  { // save image
    char filename[1024];
    snprintf(
      filename, 1000,
      "%s/ru_%f__rv_%f__f_%f__k_%f__s_%f.ppm",  // FIXME ensure same stem
      background->session, data->ru, data->rv, data->f, data->k, data->score
    );
    FILE *imgfile = fopen(filename, "wb");
    if (imgfile) {
      fwrite(data->full_ppm, data->full_ppm_bytes, 1, imgfile);
      fclose(imgfile);
    }
  } // save thumbnail

  if (background->url) { // upload
    // image analysis
    struct tamura_features feature[2];
    struct histogram histogram;
    struct segment segment;
    tamura_calculate(&background->tamura, &feature[0], data->raw_image);
    histogram_calc(&histogram, data->full_image);
    segment_calc(&segment, data->full_image);
    struct background_upload up;
    snprintf(up.ru,               UPLOAD_DATA_STRLEN, "%f", data->ru);
    snprintf(up.rv,               UPLOAD_DATA_STRLEN, "%f", data->rv);
    snprintf(up.f,                UPLOAD_DATA_STRLEN, "%f", data->f);
    snprintf(up.k,                UPLOAD_DATA_STRLEN, "%f", data->k);
    snprintf(up.behaviour,        UPLOAD_DATA_STRLEN, "%s", data->behaviour);
    snprintf(up.coarseness_u,     UPLOAD_DATA_STRLEN, "%f", feature[0].coarseness);
    snprintf(up.contrast_u,       UPLOAD_DATA_STRLEN, "%f", feature[0].contrast);
    snprintf(up.directionality_u, UPLOAD_DATA_STRLEN, "%f", feature[0].directionality);
    snprintf(up.coarseness_v,     UPLOAD_DATA_STRLEN, "%f", feature[1].coarseness);
    snprintf(up.contrast_v,       UPLOAD_DATA_STRLEN, "%f", feature[1].contrast);
    snprintf(up.directionality_v, UPLOAD_DATA_STRLEN, "%f", feature[1].directionality);
    for (int r = 0; r < HISTOGRAM_BUCKETS; ++r) {
    for (int g = 0; g < HISTOGRAM_BUCKETS; ++g) {
    for (int b = 0; b < HISTOGRAM_BUCKETS; ++b) {
      snprintf(up.histogram[r][g][b], UPLOAD_DATA_STRLEN, "%f", histogram.bucket[r][g][b]);
    }
    }
    }
    for (int s = 0; s < SEGMENT_BUCKETS; ++s) {
      snprintf(up.segment[s], UPLOAD_DATA_STRLEN, "%f", segment.bucket[s]);
    }
    struct curl_httppost *formpost = NULL;
    struct curl_httppost *lastptr = NULL;
#define FORM(s) curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, #s, CURLFORM_COPYCONTENTS, up . s, CURLFORM_END)
    FORM(ru);
    FORM(rv);
    FORM(f);
    FORM(k);
    FORM(behaviour);
    FORM(coarseness_u);
    FORM(contrast_u);
    FORM(directionality_u);
    FORM(coarseness_v);
    FORM(contrast_v);
    FORM(directionality_v);
#undef FORM
    for (int r = 0; r < HISTOGRAM_BUCKETS; ++r) {
    for (int g = 0; g < HISTOGRAM_BUCKETS; ++g) {
    for (int b = 0; b < HISTOGRAM_BUCKETS; ++b) {
      char hist[UPLOAD_DATA_STRLEN];
      snprintf(hist, UPLOAD_DATA_STRLEN, "hist_%d_%d_%d", r, g, b);
      curl_formadd(
        &formpost, &lastptr,
        CURLFORM_COPYNAME, hist,
        CURLFORM_COPYCONTENTS, up.histogram[r][g][b],
        CURLFORM_END
      );
    }
    }
    }
    for (int s = 0; s < SEGMENT_BUCKETS; ++s) {
      char seg[UPLOAD_DATA_STRLEN];
      snprintf(seg, UPLOAD_DATA_STRLEN, "seg_%d", s);
      curl_formadd(
        &formpost, &lastptr,
        CURLFORM_COPYNAME, seg,
        CURLFORM_COPYCONTENTS, up.segment[s],
        CURLFORM_END
      );
    }
    curl_formadd(
      &formpost, &lastptr,
      CURLFORM_COPYNAME, "image",
      CURLFORM_BUFFER, "rdex.ppm",
      CURLFORM_BUFFERPTR, data->full_ppm,
      CURLFORM_BUFFERLENGTH, data->full_ppm_bytes,
      CURLFORM_END
    );
    curl_formadd(
      &formpost, &lastptr,
      CURLFORM_COPYNAME, "submit",
      CURLFORM_COPYCONTENTS, "submit",
      CURLFORM_END
    );
    CURL *curl = curl_easy_init();
    if (curl) {
      curl_easy_setopt(curl, CURLOPT_URL, background->url);
      curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);
      curl_easy_setopt(curl, CURLOPT_WRITEDATA, stderr);
      curl_easy_perform(curl);
      curl_easy_cleanup(curl);
      curl_formfree(formpost);
    }
  } // upload

  { // cleanup
//    free(data->thumb_ppm);
    free(data->full_ppm);
    image_free(data->raw_image);
    image_free(data->full_image);
  } // cleanup

}

// EOF
