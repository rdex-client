/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Generic Shader
===================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include "shader.h"

//======================================================================
// print a shader object's debug log
void shader_debug(GLhandleARB obj) {
//  return; // FIXME: only dump logs when shader compile/link failed
  int infologLength = 0;
  int maxLength;
  if (glIsShader(obj)) {
    glGetShaderiv(obj, GL_INFO_LOG_LENGTH, &maxLength);
  } else {
    glGetProgramiv(obj, GL_INFO_LOG_LENGTH, &maxLength);
  }
  char *infoLog = malloc(maxLength);
  if (!infoLog) {
    return;
  }
  if (glIsShader(obj)) {
    glGetShaderInfoLog(obj, maxLength, &infologLength, infoLog);
  } else {
    glGetProgramInfoLog(obj, maxLength, &infologLength, infoLog);
  }
  if (infologLength > 0) {
    fprintf(stderr, "%s\n", infoLog);
  }
  free(infoLog);
}

//======================================================================
// generic shader uniform location access macro
#define shader_uniform(self,name) \
  (self)->uniform.name = \
     glGetUniformLocationARB((self)->shader.program, #name)

//======================================================================
// generic shader uniform update access macro (integer)
#define shader_updatei(self,name) \
  glUniform1iARB((self)->uniform.name, (self)->value.name)

//======================================================================
// generic shader uniform update access macro (float)
#define shader_updatef(self,name) \
  glUniform1fARB((self)->uniform.name, (self)->value.name)

//======================================================================
// generic shader initialization
struct shader *shader_init(
  struct shader *shader, const char *vert, const char *frag
) {
  if (! shader) { return 0; }
  shader->linkStatus     = 0;
  shader->vertexSource   = vert;
  shader->fragmentSource = frag;
  if (shader->vertexSource || shader->fragmentSource) {
    shader->program = glCreateProgramObjectARB();
    if (shader->vertexSource) {
      shader->vertex =
        glCreateShaderObjectARB(GL_VERTEX_SHADER_ARB);
      glShaderSourceARB(shader->vertex,
        1, (const GLcharARB **) &shader->vertexSource, 0
      );
      glCompileShaderARB(shader->vertex);
      shader_debug(shader->vertex);
      glAttachObjectARB(shader->program, shader->vertex);
    }
    if (shader->fragmentSource) {
      shader->fragment =
        glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);
      glShaderSourceARB(shader->fragment,
        1, (const GLcharARB **) &shader->fragmentSource, 0
      );
      glCompileShaderARB(shader->fragment);
      shader_debug(shader->fragment);
      glAttachObjectARB(shader->program, shader->fragment);
    }
    glLinkProgramARB(shader->program);
    shader_debug(shader->program);
    glGetObjectParameterivARB(shader->program,
      GL_OBJECT_LINK_STATUS_ARB, &shader->linkStatus
    );
    if (! shader->linkStatus) { return 0; }
  } else { return 0; }
  return shader;
}
