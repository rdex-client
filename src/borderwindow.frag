/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2010,2011,2019  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Sphere shader
===================================================================== */

uniform sampler2DArray tex;

varying vec2 spin2;
varying float scale2;

void main(void) {
  float p = gl_TexCoord[0].x;               // float in [0..]
  vec2 q0 = gl_TexCoord[0].zw;
  vec2 q1 = 2.0 * (q0 - vec2(0.5, 0.5));
  float d = dot(q1,q1);
  if (d < 1.0) {
    vec3 n = normalize(vec3(q1, sqrt(1.0-d)));
    vec2 q = q1 / (1.0 + sqrt(1.0 - d));
    vec2 q2 = scale2 * q + spin2;
    vec2 frac = q2;
    // linear interpolation
    vec3 m = texture(tex, vec3(frac, p)).rgb;
    // output
    float a = 1.0;
    float k = gl_FragCoord.z - 0.1 * cos(sqrt(d) * 3.1415926*0.5);
    float c = 0.0625 + pow(dot(vec3(0.0, 0.0, 1.0), n), 2.0);
    vec3 rgb = m * c;
    gl_FragDepth = k;
    gl_FragColor = vec4(rgb, a);
  } else {
    discard;
  }
}
