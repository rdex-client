/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
CPU-Based Image Segmentation
===================================================================== */

#include <math.h>
#include <stdlib.h>
#include "segment.h"
#include "util.h"

#define SEGMENT_LEVELS 4

struct segment_edge {
  int   fx,fy,tx,ty;
  float w;
};

static inline void segment_weight(struct segment_edge *e, const struct image *i) {
  e->w = fabs(I(i,e->fx,e->fy,0)-I(i,e->tx,e->ty,0))
       + fabs(I(i,e->fx,e->fy,1)-I(i,e->tx,e->ty,1))
       + fabs(I(i,e->fx,e->fy,2)-I(i,e->tx,e->ty,2));
}

static int segment_compare(const void *x, const void *y) {
  const struct segment_edge *e1 = x;
  const struct segment_edge *e2 = y;
  if (e1->w < e2->w) return -1;
  if (e1->w > e2->w) return 1;
  return 0;
}

struct segment_component {
  int deleted;
  int merged;
  int size;
  float diff;
};

void segment_calc(struct segment *s, const struct image *i) {
  float threshold = 0.1;
  for (int b = 0; b < SEGMENT_BUCKETS; ++b) {
    s->bucket[b] = 0;
  }
  int n = i->width * i->height;
  int m = n * 4;
  struct segment_edge *edges = malloc(m * sizeof(struct segment_edge));
  int e = 0;                             //  ...
  for (int y = 0; y < i->height; ++y) {  //  .ox
  for (int x = 0; x < i->width; ++x) {   //  xxx
    edges[e].fx = x; edges[e].fy = y; edges[e].tx = (x + 1) % i->width; edges[e].ty = y; segment_weight(&edges[e], i); e++;
    edges[e].fx = x; edges[e].fy = y; edges[e].tx = x; edges[e].ty = (y + 1) % i->height; segment_weight(&edges[e], i); e++;
    edges[e].fx = x; edges[e].fy = y; edges[e].tx = (x + 1) % i->width; edges[e].ty = (y + 1) % i->height; segment_weight(&edges[e], i); e++;
    edges[e].fx = x; edges[e].fy = y; edges[e].tx = (x - 1 + i->width) % i->width; edges[e].ty = (y + 1) % i->height; segment_weight(&edges[e], i); e++;
  }
  }
  qsort(edges, m, sizeof(struct segment_edge), segment_compare);
  struct image *S = image_alloc(i->width, i->height, 1);
  int k = 0;
  for (int y = 0; y < i->height; ++y) {
  for (int x = 0; x < i->width; ++x) {
    I(S,x,y,0) = k++;
  }
  }
  struct segment_component *components = malloc(n * sizeof(struct segment_component));
  for (k = 0; k < n; ++k) {
    components[k].deleted = 0;
    components[k].merged = -1;
    components[k].size = 1;
    components[k].diff = 0;
  }
  for (int q = 0; q < m; ++q) {
    int ci = I(S, edges[q].fx, edges[q].fy, 0);
    int cj = I(S, edges[q].tx, edges[q].ty, 0);
    while (components[ci].deleted) { ci = components[ci].merged; }
    while (components[cj].deleted) { cj = components[cj].merged; }
    if (ci != cj) {
      if (ci > cj) { int t = cj; cj = ci; ci = t; }
      float d = fminf(components[ci].diff + threshold / components[ci].size,
                      components[cj].diff + threshold / components[cj].size);
      if (edges[q].w <= d) {
        components[ci].size += components[cj].size;
        components[ci].diff = fmaxf(fmaxf(components[ci].diff, components[cj].diff), edges[q].w);
        components[cj].deleted = 1;
        components[cj].merged = ci;
        I(S, edges[q].fx, edges[q].fy, 0) = ci;
        I(S, edges[q].tx, edges[q].ty, 0) = ci;
      }
    }
  }
  image_free(S); S = NULL;
  free(edges); edges = NULL;
  for (int c = 0; c < n; ++c) {
    if (!components[c].deleted) {
      int b = logtwo(components[c].size);
      s->bucket[b] += components[c].size;
    }
  }
  free(components); components = NULL;
  for (int b = 0; b < SEGMENT_BUCKETS; ++b) {
    s->bucket[b] /= n;
  }
}
