/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010,2011  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Timeline module
===================================================================== */

#ifndef TIMELINE_H
#define TIMELINE_H 1

#define TIMELINE_INTRO1_LENGTH 5
#define TIMELINE_INTRO2_LENGTH 25
#define TIMELINE_OUTRO_LENGTH 30

enum timeline_state {
  timeline_idle,
  timeline_intro1,
  timeline_intro2,
  timeline_main,
  timeline_outro,
  timeline_done
};

struct timeline {
  enum timeline_state state1, state;
  double phase;
};

struct timeline *timeline_init(struct timeline *timeline);
void timeline_advance(struct timeline *timeline);
void timeline_start(struct timeline *timeline);
void timeline_stop(struct timeline *timeline);

#endif
