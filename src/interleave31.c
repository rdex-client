#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
  if (argc != 4) {
    return 1;
  }
  int d = atoi(argv[3]);
  FILE *in3 = fopen(argv[1], "rb");
  if (in3) {
    FILE *in1 = fopen(argv[2], "rb");
    if (in1) {
      for (int i = 0; i < d*d; ++i) {
        putchar(getc(in3));
        putchar(getc(in3));
        putchar(getc(in3));
        putchar(getc(in1));
      }
      fclose(in1);
    } else {
      return 1;
    }
    fclose(in3);
  } else {
    return 1;
  }
  return 0;
}
