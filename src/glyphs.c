/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Sequencer glyphs
===================================================================== */

#include <GL/glew.h>
#include "glyphs.h"

extern char _binary_glyph_curve_less_rgba_start;
extern char _binary_glyph_curve_more_rgba_start;
extern char _binary_glyph_speed_less_rgba_start;
extern char _binary_glyph_speed_more_rgba_start;

struct glyphs *glyphs_init(struct glyphs *glyphs) {
  glGenTextures(4, &glyphs->textures[0]);
  // curve less
  glBindTexture(GL_TEXTURE_2D, glyphs->textures[sequence_curve_less]);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 128, 128, 0, GL_RGBA, GL_UNSIGNED_BYTE, &_binary_glyph_curve_less_rgba_start);
  // curve more
  glBindTexture(GL_TEXTURE_2D, glyphs->textures[sequence_curve_more]);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 128, 128, 0, GL_RGBA, GL_UNSIGNED_BYTE, &_binary_glyph_curve_more_rgba_start);
  // speed less
  glBindTexture(GL_TEXTURE_2D, glyphs->textures[sequence_speed_less]);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 128, 128, 0, GL_RGBA, GL_UNSIGNED_BYTE, &_binary_glyph_speed_less_rgba_start);
  // speed more
  glBindTexture(GL_TEXTURE_2D, glyphs->textures[sequence_speed_more]);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 128, 128, 0, GL_RGBA, GL_UNSIGNED_BYTE, &_binary_glyph_speed_more_rgba_start);
  return glyphs;
}
