/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Exponential colour space conversion
===================================================================== */

uniform sampler2D texture;

void main(void) {
  vec2 p = gl_TexCoord[0].st;
  vec4 x = texture2D(texture, p);
  float l = dot(vec3(0.3, 0.5, 0.2), x.rgb);
  float a;
  if (l > 0.5) {
    a = 1.0;
  } else {
    a = 0.0;
  }
  if (l > 0.0) {
    x /= l;
    l *= 2.0;
    l = exp(l) / (l + 1.0) * 0.4;
    x *= l;
  }
  gl_FragColor = vec4(x.rgb, a);
}
