/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2009,2010,2019  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Reaction Diffusion Shader
===================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "util.h"
#include "reactiondiffusion.h"
#include "reactiondiffusion.frag.c"

//======================================================================
// reaction diffusion shader parameter randomization
void reactiondiffusion_randomize(
  struct reactiondiffusion *reactiondiffusion
) {
  reactiondiffusion->value.ru = 0.3*rand() / (double) RAND_MAX + 0.001;
  reactiondiffusion->value.rv = 0.3*rand() / (double) RAND_MAX + 0.001;
  reactiondiffusion->value.f  = 0.1*rand() / (double) RAND_MAX + 0.001;
  reactiondiffusion->value.k  = 0.1*rand() / (double) RAND_MAX + 0.001;
  reactiondiffusion->frame = 0;
#if 0
  fprintf(stderr, "\n%f\t%f\t%f\t%f\t",
    reactiondiffusion->value.ru,
    reactiondiffusion->value.rv,
    reactiondiffusion->value.f,
    reactiondiffusion->value.k
  );
#endif
}
/*
  reactiondiffusion->spawnangle += 1.0 / 32.0;
  double a = reactiondiffusion->spawnangle;
  reactiondiffusion->value.ru = (cos(sqrt(2) * a) * 0.5 + 0.5) * 0.01 + 0.15;
  reactiondiffusion->value.rv = (cos(sqrt(3) * a) * 0.5 + 0.5) * 0.01 + 0.15;
  reactiondiffusion->value.f  = (sin(sqrt(5) * a) * 0.5 + 0.5) * 0.01 + 0.001;
  reactiondiffusion->value.k  = (sin(sqrt(7) * a) * 0.5 + 0.5) * 0.01 + 0.001;
*/

void reactiondiffusion_near(
  struct reactiondiffusion *reactiondiffusion, float ru, float rv, float f, float k, float d
) {
  reactiondiffusion->value.ru = ru + d * 0.3*(rand() / (double) RAND_MAX - 0.5);
  reactiondiffusion->value.rv = rv + d * 0.3*(rand() / (double) RAND_MAX - 0.5);
  reactiondiffusion->value.f  = f  + d * 0.1*(rand() / (double) RAND_MAX - 0.5);
  reactiondiffusion->value.k  = k  + d * 0.1*(rand() / (double) RAND_MAX - 0.5);
  reactiondiffusion->frame = 0;
    fprintf(stderr, "\n%f\t%f\t%f\t%f\t",
    reactiondiffusion->value.ru,
    reactiondiffusion->value.rv,
    reactiondiffusion->value.f,
    reactiondiffusion->value.k
  );
}

//======================================================================
// reaction diffusion shader parameter perturbation
void reactiondiffusion_perturb(
  struct reactiondiffusion *reactiondiffusion
) {
  reactiondiffusion->value.ru += 0.003 * (rand() / (double) RAND_MAX - 0.5);
  reactiondiffusion->value.rv += 0.003 * (rand() / (double) RAND_MAX - 0.5);
  reactiondiffusion->value.f  += 0.003 * (rand() / (double) RAND_MAX - 0.5);
  reactiondiffusion->value.k  += 0.003 * (rand() / (double) RAND_MAX - 0.5);
#define clip(x,m,M) ((x)=(((x)<(m))?(m):(((x)>(M))?(M):(x))))
  clip(reactiondiffusion->value.ru, 0.0, 0.3);
  clip(reactiondiffusion->value.rv, 0.0, 0.3);
  clip(reactiondiffusion->value.f,  0.0, 0.1);
  clip(reactiondiffusion->value.k,  0.0, 0.1);
#undef clip
//  reactiondiffusion->frame = 0;
}

//======================================================================
// reaction diffusion shader initialization
struct reactiondiffusion *reactiondiffusion_init(
  struct reactiondiffusion *reactiondiffusion
) {
  if (! reactiondiffusion) { return 0; }
  if (! shader_init(
          &reactiondiffusion->shader, 0, reactiondiffusion_frag
     )) {
    return 0;
  }
  shader_uniform(reactiondiffusion, texture);
  shader_uniform(reactiondiffusion, dx);
  shader_uniform(reactiondiffusion, dy);
  shader_uniform(reactiondiffusion, ru);
  shader_uniform(reactiondiffusion, rv);
  shader_uniform(reactiondiffusion, f);
  shader_uniform(reactiondiffusion, k);
  reactiondiffusion->value.texture = 0;
  reactiondiffusion->value.dx      = 0;
  reactiondiffusion->value.dy      = 0;
  reactiondiffusion->spawnangle = 0.0;
  reactiondiffusion->frame = 0;
  reactiondiffusion_randomize(reactiondiffusion);
  glGenTextures(2, reactiondiffusion->textures);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, reactiondiffusion->textures[0]);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glBindTexture(GL_TEXTURE_2D, reactiondiffusion->textures[1]);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glDisable(GL_TEXTURE_2D);
  reactiondiffusion->buffer = 0;
  reactiondiffusion->width  = 0;
  reactiondiffusion->height = 0;
  reactiondiffusion->seed  = 60;
  return reactiondiffusion;
}

//======================================================================
// reaction diffusion shader reshape callback
void reactiondiffusion_reshape(
  struct reactiondiffusion *reactiondiffusion, int w, int h
) {
  reactiondiffusion->width  = roundtwo(w);
  reactiondiffusion->height = roundtwo(h);
  reactiondiffusion->value.dx = 1.0 / reactiondiffusion->width;
  reactiondiffusion->value.dy = 1.0 / reactiondiffusion->height;
  float *zero = calloc(1, reactiondiffusion->width * reactiondiffusion->height * 2 * sizeof(float));
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, reactiondiffusion->textures[0]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RG32F,
    reactiondiffusion->width, reactiondiffusion->height,
    0, GL_RG, GL_FLOAT, zero
  );
  glBindTexture(GL_TEXTURE_2D, reactiondiffusion->textures[1]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RG32F,
    reactiondiffusion->width, reactiondiffusion->height,
    0, GL_RG, GL_FLOAT, zero
  );
  free(zero);
  glDisable(GL_TEXTURE_2D);
}

//======================================================================
// reaction diffusion shader display callback
void reactiondiffusion_display(
  struct reactiondiffusion *reactiondiffusion, GLuint fbo
) {
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D,
    reactiondiffusion->textures[reactiondiffusion->buffer]
  );
  //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
  glFramebufferTexture2DEXT(
    GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D,
    reactiondiffusion->textures[1 - reactiondiffusion->buffer], 0
  );
  glUseProgramObjectARB(reactiondiffusion->shader.program);
  shader_updatei(reactiondiffusion, texture);
  shader_updatef(reactiondiffusion, dx);
  shader_updatef(reactiondiffusion, dy);
  shader_updatef(reactiondiffusion, ru);
  shader_updatef(reactiondiffusion, rv);
  shader_updatef(reactiondiffusion, f);
  shader_updatef(reactiondiffusion, k);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, 1, 0, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glViewport(0, 0, reactiondiffusion->width, reactiondiffusion->height);
  glBegin(GL_QUADS); { glColor4f(1,1,1,1);
    glTexCoord2f(0, 0); glVertex2f(0, 0);
    glTexCoord2f(1, 0); glVertex2f(1, 0);
    glTexCoord2f(1, 1); glVertex2f(1, 1);
    glTexCoord2f(0, 1); glVertex2f(0, 1);
  } glEnd();
  glUseProgramObjectARB(0);
  glDisable(GL_TEXTURE_2D);
  if (reactiondiffusion->frame < reactiondiffusion->seed) {
    if (reactiondiffusion->frame == 0) {
      glBegin(GL_QUADS); {
        glColor4f(0.5,0.5,0.5,1);
        glVertex2f(0, 0);
        glVertex2f(1, 0);
        glVertex2f(1, 1);
        glVertex2f(0, 1);
      } glEnd();
    }
    glEnable(GL_POLYGON_SMOOTH);
    glBegin(GL_QUADS); { for (int q = 0; q < reactiondiffusion->seed - reactiondiffusion->frame; ++q) {
      float u = (double) rand() / (double) RAND_MAX;
      float v = (double) rand() / (double) RAND_MAX;
      glColor4f(u,v,0,1); //(0,1,0,1);
      float x = (double) rand() / (double) RAND_MAX;
      float y = (double) rand() / (double) RAND_MAX;
      float w = ((double) rand() / (double) RAND_MAX - 0.5) / 16.0;
      float h = ((double) rand() / (double) RAND_MAX - 0.5) / 16.0;
      float a = ((double) rand() / (double) RAND_MAX) * 2.0 * 3.1415926;
      float c = cos(a);
      float s = sin(a);
      for (int dx = -1; dx <= 1; dx += 1) {
        for (int dy = -1; dy <= 1; dy += 1) {
          glVertex2f(dx + x + w * c + h *  s, dy + y + w * -s + h * c);
          glVertex2f(dx + x + w * c + h * -s, dy + y + w * -s + h *-c);
          glVertex2f(dx + x + w *-c + h * -s, dy + y + w *  s + h *-c);
          glVertex2f(dx + x + w *-c + h *  s, dy + y + w *  s + h * c);
        }
      }
    } } glEnd();
    glDisable(GL_POLYGON_SMOOTH);
  }
  //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  glDisable(GL_TEXTURE_2D);
  reactiondiffusion->buffer = 1 - reactiondiffusion->buffer;
  reactiondiffusion->texture =
    reactiondiffusion->textures[reactiondiffusion->buffer];
}

//======================================================================
// reaction diffusion shader idle callback
void reactiondiffusion_idle(
  struct reactiondiffusion *reactiondiffusion
) {
  reactiondiffusion->frame += 1;
}

// EOF
