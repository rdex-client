/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2010,2011  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Border/Window Shader
===================================================================== */

#include "borderwindow.h"
#include "borderwindow.frag.c"
#include "projection.vert.c"
#include "borderwindow_pick.frag.c"
#include "projection_pick.vert.c"

//======================================================================
// borderwindow shader initialization
struct borderwindow *borderwindow_init(struct borderwindow *borderwindow) {
  if (! borderwindow) { return 0; }
  if (! shader_init(&borderwindow->shader, projection_vert, borderwindow_frag)) {
    return 0;
  }
  // vertex shader
  shader_uniform(borderwindow, origin);
  shader_uniform(borderwindow, scale);
  shader_uniform(borderwindow, rotate);
  shader_uniform(borderwindow, translate);
  shader_uniform(borderwindow, near);
  shader_uniform(borderwindow, far);
  borderwindow->value.origin[0] = 0.15;
  borderwindow->value.origin[1] = 0.15;
  borderwindow->value.origin[2] = 0.05;
  borderwindow->value.origin[3] = 0.05;
  borderwindow->value.scale[0] = 1.0/0.15;
  borderwindow->value.scale[1] = 1.0/0.15;
  borderwindow->value.scale[2] = 1.0/0.05;
  borderwindow->value.scale[3] = 1.0/0.05;
  int k = 0;
  for (int i = 0; i < 4; ++i) {
  for (int j = 0; j < 4; ++j) {
    borderwindow->value.rotate[k++] = i == j;
  }}
  borderwindow->value.translate[0] = 0.0;
  borderwindow->value.translate[1] = 0.0;
  borderwindow->value.translate[2] = 0.0;
  borderwindow->value.translate[3] = 8.0;
  borderwindow->value.near[0] = -1.0;
  borderwindow->value.near[1] = -1.0;
  borderwindow->value.near[2] = -1.0;
  borderwindow->value.near[3] =  4.0;
  borderwindow->value.far[0] =  1.0;
  borderwindow->value.far[1] =  1.0;
  borderwindow->value.far[2] =  1.0;
  borderwindow->value.far[3] = 12.0;
  // fragment shader
  shader_uniform(borderwindow, texture);
  shader_uniform(borderwindow, pack);
  shader_uniform(borderwindow, size);
  borderwindow->value.texture = 0;
  borderwindow->value.pack = 0;
  return borderwindow;
}

//======================================================================
// borderwindow shader activation
void borderwindow_use(struct borderwindow *borderwindow) {
  if (borderwindow) {
    glUseProgramObjectARB(borderwindow->shader.program);
    borderwindow->attr.delta = glGetAttribLocationARB(borderwindow->shader.program, "delta");
    borderwindow->attr.spin  = glGetAttribLocationARB(borderwindow->shader.program, "spin");
    borderwindow->attr.pick  = glGetAttribLocationARB(borderwindow->shader.program, "pick");
    // vertex shader
    shader_updatef4(borderwindow, origin);
    shader_updatef4(borderwindow, scale);
    shader_updatem4(borderwindow, rotate);
    shader_updatef4(borderwindow, translate);
    shader_updatef4(borderwindow, near);
    shader_updatef4(borderwindow, far);
    // fragment shader
    shader_updatei (borderwindow, texture);
    shader_updatef (borderwindow, pack);
    shader_updatef (borderwindow, size);
  } else {
    glUseProgramObjectARB(0);
  }
}

//======================================================================
// borderwindow picking shader initialization
struct borderwindow_pick *borderwindow_pick_init(struct borderwindow_pick *borderwindow_pick) {
  if (! borderwindow_pick) { return 0; }
  if (! shader_init(&borderwindow_pick->shader, projection_pick_vert, borderwindow_pick_frag)) {
    return 0;
  }
  // vertex shader
  shader_uniform(borderwindow_pick, origin);
  shader_uniform(borderwindow_pick, scale);
  shader_uniform(borderwindow_pick, rotate);
  shader_uniform(borderwindow_pick, translate);
  shader_uniform(borderwindow_pick, near);
  shader_uniform(borderwindow_pick, far);
  borderwindow_pick->value.origin[0] = 0.15;
  borderwindow_pick->value.origin[1] = 0.15;
  borderwindow_pick->value.origin[2] = 0.05;
  borderwindow_pick->value.origin[3] = 0.05;
  borderwindow_pick->value.scale[0] = 1.0/0.15;
  borderwindow_pick->value.scale[1] = 1.0/0.15;
  borderwindow_pick->value.scale[2] = 1.0/0.05;
  borderwindow_pick->value.scale[3] = 1.0/0.05;
  int k = 0;
  for (int i = 0; i < 4; ++i) {
  for (int j = 0; j < 4; ++j) {
    borderwindow_pick->value.rotate[k++] = i == j;
  }}
  borderwindow_pick->value.translate[0] = 0.0;
  borderwindow_pick->value.translate[1] = 0.0;
  borderwindow_pick->value.translate[2] = 0.0;
  borderwindow_pick->value.translate[3] = 8.0;
  borderwindow_pick->value.near[0] = -1.0;
  borderwindow_pick->value.near[1] = -1.0;
  borderwindow_pick->value.near[2] = -1.0;
  borderwindow_pick->value.near[3] =  4.0;
  borderwindow_pick->value.far[0] =  1.0;
  borderwindow_pick->value.far[1] =  1.0;
  borderwindow_pick->value.far[2] =  1.0;
  borderwindow_pick->value.far[3] = 12.0;
  return borderwindow_pick;
}

//======================================================================
// borderwindow_pick shader activation
void borderwindow_pick_use(struct borderwindow_pick *borderwindow_pick) {
  if (borderwindow_pick) {
    glUseProgramObjectARB(borderwindow_pick->shader.program);
    borderwindow_pick->attr.delta = glGetAttribLocationARB(borderwindow_pick->shader.program, "delta");
    // vertex shader
    shader_updatef4(borderwindow_pick, origin);
    shader_updatef4(borderwindow_pick, scale);
    shader_updatem4(borderwindow_pick, rotate);
    shader_updatef4(borderwindow_pick, translate);
    shader_updatef4(borderwindow_pick, near);
    shader_updatef4(borderwindow_pick, far);
  } else {
    glUseProgramObjectARB(0);
  }
}

// EOF
