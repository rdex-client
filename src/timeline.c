/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Timeline module
===================================================================== */

#include "timeline.h"

struct timeline *timeline_init(struct timeline *timeline) {
  timeline->state1 = timeline_idle;
  timeline->state = timeline_idle;
  timeline->phase = 0;
  return timeline;
}

void timeline_advance(struct timeline *timeline) {
  timeline->state1 = timeline->state;
  timeline->phase += 0.04;
  switch (timeline->state) {
    case timeline_done: break;
    case timeline_idle: break;
    case timeline_main: break;
    case timeline_intro1:
      if (! (timeline->phase < TIMELINE_INTRO1_LENGTH)) {
        timeline->phase = 0;
        timeline->state = timeline_intro2;
      }
      break;
    case timeline_intro2:
      if (! (timeline->phase < TIMELINE_INTRO2_LENGTH)) {
        timeline->phase = 0;
        timeline->state = timeline_main;
      }
      break;
    case timeline_outro:
      if (! (timeline->phase < TIMELINE_OUTRO_LENGTH)) {
        timeline->phase = 0;
        timeline->state = timeline_done;
      }
      break;
  }
}

void timeline_start(struct timeline *timeline) {
  timeline->phase = 0;
  timeline->state = timeline_intro1;
}

void timeline_stop(struct timeline *timeline) {
  timeline->phase = 0;
  timeline->state = timeline_outro;
}

/* EOF */
