/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Multi-scale Gaussian Blur
===================================================================== */

#ifndef GBLUR_H
#define GBLUR_H 1

#include "shader.h"
#include "expcolour.h"

//======================================================================
// maximum number of textures to allocate (16 => 65536x65536 max size)
#define gblur_tex_max 16

//======================================================================
// shader data
struct gblur { struct shader shader;
  struct { GLint texture; GLint d; } uniform;
  struct { int   texture; float d[2]; } value;
  int count;
  // textures[0] is blur output
  // textures[1] is temporary
  GLuint textures[2][gblur_tex_max];
  struct expcolour expcolour;
};

//======================================================================
// prototypes
struct gblur *gblur_init(struct gblur *gblur);
void gblur_reshape(struct gblur *gblur, int w, int h);
void gblur_display(struct gblur *gblur, GLuint fbo, GLuint texture);

#endif
