/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010,2011,2017,2019  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Audio output
===================================================================== */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <GL/glew.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/soundcard.h>
#include "audio.h"
#include "audio.frag.c"

volatile char audio_read  = AUDIO_BUFFERS / 2;
volatile char audio_write = 0;
volatile char audio_reset = 1;

void audio_compute(struct audio *audio, jack_default_audio_sample_t **out, jack_nframes_t nframes) {
  /* copy buffers to JACK */
  for (int i = 0; i < nframes; ++i) {
    int r = audio_read;
    for (int c = 0; c < AUDIO_CHANNELS; ++c) {
      // DC blocker
      switch (audio->state) {
        case audio_state_prepare:
          audio->sample[c] = 0;
          audio->iphase[c] = 0.0;
          out[c][i] = 0.0;
          break;
        case audio_state_intro: {
          double hz = 1000;
          double sr = audio->sr ? audio->sr : 48000;
          double incr = 6.283185307179586 * hz / sr;
          audio->iphase[c] = fmod(audio->iphase[c] + incr, 6.283185307179586);
          out[c][i] = sin(audio->iphase[c]) / 10;
          audio->sample[c]++;
          break;
        }
        case audio_state_outro: {
          int loop = audio->size - 3.0 * audio->sphase;
          int j = audio->index + audio->sphase * 4.0 * (rand()/((double) RAND_MAX) - 0.5);
          j %= loop;
          if (j < 0) { j += loop; }
          jack_default_audio_sample_t xn = audio->buffer[r][c][j];
          audio->yn1[0][c] = xn - audio->xn1[0][c] + audio->r[0][c] * audio->yn1[0][c];
          audio->xn1[0][c] = xn;
          out[c][i] = audio->yn1[0][c];
          break;
        }
        case audio_state_idle: {
          jack_default_audio_sample_t xn = 0;
          audio->yn1[0][c] = xn - audio->xn1[0][c] + audio->r[0][c] * audio->yn1[0][c];
          audio->xn1[0][c] = xn;
          out[c][i] = 0;
          break;
        }
        case audio_state_rolling: {
          jack_default_audio_sample_t xn = audio->buffer[r][c][audio->index];
          audio->yn1[0][c] = xn - audio->xn1[0][c] + audio->r[0][c] * audio->yn1[0][c];
          audio->xn1[0][c] = xn;
          jack_default_audio_sample_t g = fmax(0, 1 - 1 / (1 + (audio->sphase - 2) / 10));
          out[c][i] = g * audio->yn1[0][c];
          break;
        }
      }
    }
    /* increment/wrap read counter, and swap buffers */
    ++audio->index;
    while (audio->index >= audio->size) {
      audio->index -= audio->size;
      int audio_read2 = (r + 1) % AUDIO_BUFFERS;
      // don't overtake write pointer
      if (audio_read2 == audio_write) {
        audio_read2 = (audio_read2 - 1 + AUDIO_BUFFERS) % AUDIO_BUFFERS;
      }
      audio_read = audio_read2;
    }
  }
  // update state
  audio->sphase += nframes / (double) audio->sr;
  switch (audio->state) {
    case audio_state_prepare:
      if (! (audio->sphase < 2)) {
        audio->state = audio_state_intro;
        audio->sphase = 0;
      }
      break;
    case audio_state_intro:
#if 0
      if (! (audio->sphase < 5)) {
        audio->state = audio_state_rolling;
        audio->sphase = 0;
      }
#endif
      audio->sphase = 0;
      break;
    case audio_state_outro:
      if (! (audio->sphase < 60)) {
        audio->state = audio_state_idle;
        audio->sphase = 0;
/*
        if (! audio->nrt) {
          jack_transport_stop(audio->client);
        }
*/
      }
      break;
    default:
      break;
  }
}

/* JACK audio processing callback */
static int audio_process(jack_nframes_t nframes, void *arg) {
  struct audio *audio = arg;
  /* get JACK buffers */
  jack_default_audio_sample_t *out[2];
  out[0] = (jack_default_audio_sample_t *)
    jack_port_get_buffer(audio->port[0], nframes);
  out[1] = (jack_default_audio_sample_t *)
    jack_port_get_buffer(audio->port[1], nframes);
  audio_compute(audio, out, nframes);
  return 0;
}

static void *audio_process_oss(void *arg) {
  struct audio *audio = arg;
  const char *device = "/dev/dsp1";
  int audio_fd;
  if ((audio_fd = open(device, O_WRONLY, 0)) == -1) {
    perror(device);
    return 0;
  }
  // buffering
  int buffering = (AUDIO_BUFFERS << 16) | 8; // fragment size 2^8 = 256
  if (ioctl(audio_fd, SNDCTL_DSP_SETFRAGMENT, &buffering)) {
    perror("SNDCTL_DSP_SETFRAGMENT");
    return 0;
  }
  // int16_t
  int format = AFMT_S16_NE;
  if (ioctl(audio_fd, SNDCTL_DSP_SETFMT, &format) == -1) {
    perror("SNDCTL_DSP_SETFMT");
    return 0;
  }
  if (format != AFMT_S16_LE) {
    fprintf(stderr, "BAD OSS FORMAT %d != %d\n", format, AFMT_S16_NE);
    return 0;
  }
  // stereo
  int stereo = 1;
  if (ioctl(audio_fd, SNDCTL_DSP_STEREO, &stereo) == -1) {
    perror("SNDCTL_DSP_STEREO");
    return 0;
  }
  if (stereo != 1) {
    fprintf(stderr, "BAD OSS STEREO %d != 1\n", stereo);
    return 0;
  }
  // sample rate
  int speed = audio->sr;
  if (ioctl(audio_fd, SNDCTL_DSP_SPEED, &speed) == -1) {
    perror("SNDCTL_DSP_SPEED");
    return 0;
  }
  if (speed != audio->sr) {
    fprintf(stderr, "BAD OSS SPEED %d != %d\n", speed, (int) audio->sr);
    return 0;
  }
  // loop
  jack_default_audio_sample_t *out[2] = { &audio->oss_jack_buffer[0][0], &audio->oss_jack_buffer[1][0] };
  const int nframes = audio->size / 2;
  while (audio->running) {
    audio_compute(audio, out, nframes);
    for (int i = 0; i < nframes; ++i) {
      for (int c = 0; c < 2; ++c) {
        audio->oss_buffer[i][c] = 32767 * fmin(fmax(audio->oss_jack_buffer[c][i], -1), 1);
      }
    }
    if (write(audio_fd, &audio->oss_buffer[0][0], nframes * 2 * sizeof(int16_t)) == -1) {
      perror("audio write");
      //exit(1);
    }
  }
  return 0;
}

void audio_start_oss(struct audio *audio) {
  pthread_create(&audio->oss_thread, 0, audio_process_oss, audio);
}

/* JACK sample rate callback */
static int audio_srate(jack_nframes_t sr, void *arg) {
  struct audio *audio = arg;
  audio->sr = sr;
  audio->size = 240; // FIXME hardcoded
  fprintf(stderr, "JACK sample rate: %d\n", sr);
  return 0;
}

/* JACK error callback */
static void audio_error(const char *desc) {
  fprintf(stderr, "JACK error: %s\n", desc);
}

/* JACK shutdown callback */
static void audio_shutdown(void *arg) {
  fprintf(stderr, "JACK shutdown\n");
}

/* JACK timebase callback */
static void audio_timebase(jack_transport_state_t state, jack_nframes_t nframes, jack_position_t *pos, int new_pos, void *arg) {
  //struct audio *audio = arg;
  if (new_pos || audio_reset) {
    pos->valid = JackPositionBBT;
    pos->beats_per_bar = 4;
    pos->beat_type = 0.25;
    pos->ticks_per_beat = 1920;
    pos->beats_per_minute = 120;
    audio_reset = 0;
    pos->bar = 1;
    pos->beat = 1;
    pos->tick = 0;
    pos->bar_start_tick = (pos->bar - 1) * pos->beats_per_bar * pos->ticks_per_beat;
  } else {
    pos->tick +=nframes * pos->ticks_per_beat * pos->beats_per_minute / (pos->frame_rate * 60);
    while (pos->tick >= pos->ticks_per_beat) {
      pos->tick -= pos->ticks_per_beat;
      if (++pos->beat > pos->beats_per_bar) {
        pos->beat = 1;
        ++pos->bar;
        pos->bar_start_tick += pos->beats_per_bar * pos->ticks_per_beat;
      }
    }
  }
}

/* exit callback */
void audio_atexit(struct audio *audio) {
  if (audio->nrt) {
    free(audio->nrtbuffers[0]);
    free(audio->nrtbuffers[1]);
    free(audio->nrtbuffers_interleaved);
    sf_close(audio->nrtf);
  } else {
    audio->running = 0;
    if (0)
      jack_client_close(audio->client);
    if (1)
      pthread_join(audio->oss_thread, 0);
  }
}

//======================================================================
// ...
struct audio *audio_init(struct audio *audio, int nrt) {
  if (! audio) { return 0; }
  if (! shader_init(&audio->shader, 0, audio_frag)) {
    return 0;
  }
  shader_uniform(audio, texture);
  shader_uniform(audio, coords);
  audio->value.texture = 0;
  audio->value.coords = 1;
  // pitch
  audio->pitch = 1;
  // scan point
  audio->x = 0;
  audio->y = 0;
  audio->a = 0;
  audio->curve = 40;
  audio->speed = 128;
  audio->dcurve = 0;
  audio->dspeed = 0;
  // time passing
  audio->samples = 0;

  // pixel buffers
  for (int c = 0; c < AUDIO_CHANNELS; ++c) {
    glGenBuffersARB(AUDIO_COUNT, &audio->pbos[c][0]);
    for (int i = 0; i < AUDIO_COUNT; ++i) {
      glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, audio->pbos[c][i]);
      glBufferDataARB(GL_PIXEL_PACK_BUFFER_ARB, audio_tex_size * audio_tex_size * sizeof(GLfloat), 0, GL_STREAM_READ_ARB); // 1 channel
    }
  }
  glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, 0);

  /* default sample rate */
  audio->sr = 48000;
  audio->size = 240; // FIXME hardcoded...
  //audio->value.size = audio->size;
  glGenTextures(2, &audio->textures[0]);
  glEnable(GL_TEXTURE_2D);
  // output
  glBindTexture(GL_TEXTURE_2D, audio->textures[0]);
  glTexImage2D(
    GL_TEXTURE_2D, 0, GL_RGBA32F_ARB, audio_tex_size, audio_tex_size, 0, GL_RGBA,
    GL_FLOAT, 0
  );
//  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  // coordinates
  glBindTexture(GL_TEXTURE_2D, audio->textures[1]);
  glTexImage2D(
    GL_TEXTURE_2D, 0, GL_RGBA32F_ARB, audio_tex_size, audio_tex_size, 0, GL_RGBA,
    GL_FLOAT, 0 // audio->coords
  );
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glDisable(GL_TEXTURE_2D);
  audio->index = 0;
  /* initialize DC blocking filters */
  for (int c = 0; c < AUDIO_CHANNELS; ++c) {
    for (int d = 0; d < 2; ++d) {
      audio->r[d][c] = 0.995;
      audio->xn1[d][c] = 0;
      audio->yn1[d][c] = 0;
    }
    // initialize compressor/limiter
    audio->gain[c] = 1.0;
  }
  audio->nrt = nrt;
  audio->frame = 0;
  if (audio->nrt) {
//    audio->size = audio->sr / 25;
    audio->nrtbuffers[0] = calloc(1, 8 * audio->size * sizeof(jack_default_audio_sample_t));// FIXME hardcoded
    audio->nrtbuffers[1] = calloc(1, 8 * audio->size * sizeof(jack_default_audio_sample_t));// FIXME hardcoded
    audio->nrtbuffers_interleaved = calloc(1, 2 * 8 * audio->size * sizeof(float));// FIXME hardcoded
    SF_INFO info = { 0, audio->sr, 2, SF_FORMAT_WAV | SF_FORMAT_FLOAT, 0, 0 };
    audio->nrtf = sf_open("audio.wav", SFM_WRITE, &info);
  } else if (1) {
    audio->state = audio_state_idle;
    audio->sphase = 0;
    audio->running = 1;
    audio_start_oss(audio);
  } else {
    /* set up JACK */
    audio->state = audio_state_idle;
    audio->sphase = 0;
    jack_set_error_function(audio_error);
    if (!(audio->client = jack_client_open("rdex", JackNoStartServer | JackUseExactName, 0))) {
      fprintf (stderr, "JACK server not running?\n");
    } else {
      jack_set_process_callback(audio->client, audio_process, audio);
      jack_set_sample_rate_callback(audio->client, audio_srate, audio);
      jack_on_shutdown(audio->client, audio_shutdown, audio);
      /* create ports */
      audio->port[0] = jack_port_register(
        audio->client, "output_1", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0
      );
      audio->port[1] = jack_port_register(
        audio->client, "output_2", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0
      );
      /* activate audio */
      if (jack_activate(audio->client)) {
        fprintf (stderr, "cannot activate JACK client\n");
      } else {
#if 1
        /* must be activated before connecting JACK ports */
        const char **ports;
        if ((ports = jack_get_ports(
          audio->client, NULL, NULL, JackPortIsPhysical | JackPortIsInput
        ))) {
          /* connect up to two physical playback ports */
          int i = 0;
          while (ports[i] && i < 2) {
            if (jack_connect(
              audio->client, jack_port_name(audio->port[i]), ports[i]
            )) {
              fprintf(stderr, "cannot connect JACK output port\n");
            }
            i++;
          }
          free(ports);
        }
#endif
        /* become timebase master */
        if (jack_set_timebase_callback(audio->client, 0, audio_timebase, audio)) {
          fprintf(stderr, "cannot become JACK master\n");
        }
      }
    }
  }
  return audio;
}

//======================================================================
// ...
void audio_display1(struct audio *audio, GLuint fbo, GLuint texture, int iter) {
  { // compute coordinates
    memset(audio->coords, 0, 4 * audio_tex_size * audio_tex_size * sizeof(float));
    float x  = audio->x;
    float y  = audio->y;
    float a  = audio->a;
    float pi2 = 2.0 * 3.1415926;
    float ds =       audio->speed / 256.0 / 256.0;
    for (int i = 0; i < audio->size; ++i) {
      audio->curve = audio->pitch + 1.0 * rand() / (double) RAND_MAX;
      float da = pi2 * audio->curve / audio->sr;
      float dx = ds * cosf(a);
      float dy = ds * sinf(a);
      x += dx;
      y += dy;
      a += da;
      audio->coords[2 * i + 0] = x;
      audio->coords[2 * i + 1] = y;
    }
    audio->x  = x - floorf(x);
    audio->y  = y - floorf(y);
    audio->a  = a - floorf(a/pi2) * pi2;
    audio->samples += audio->size;
  }
  // upload coordinates
  glEnable(GL_TEXTURE_2D);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, audio->textures[1]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F_ARB, audio_tex_size, audio_tex_size, 0, GL_RG, GL_FLOAT, audio->coords);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  // image
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
//  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
  // extract
  glUseProgramObjectARB(audio->shader.program);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, 1, 0, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glViewport(0, 0, audio_tex_size, audio_tex_size);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, audio->textures[0], 0);
  shader_updatei (audio, texture);
  shader_updatei (audio, coords);
  glBegin(GL_QUADS); { glColor4f(1,1,1,1);
    glTexCoord2f(0, 0); glVertex2f(0, 0);
    glTexCoord2f(1, 0); glVertex2f(1, 0);
    glTexCoord2f(1, 1); glVertex2f(1, 1);
    glTexCoord2f(0, 1); glVertex2f(0, 1);
  } glEnd();
  glActiveTexture(GL_TEXTURE1); glBindTexture(GL_TEXTURE_2D, 0);
  glActiveTexture(GL_TEXTURE0); glBindTexture(GL_TEXTURE_2D, 0);
  glUseProgramObjectARB(0);
  //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  // copy texture to PBO
  glBindTexture(GL_TEXTURE_2D, audio->textures[0]);
  glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, audio->pbos[0][iter]);
  glGetTexImage(GL_TEXTURE_2D, 0, GL_RED,   GL_FLOAT, 0); // async
  glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, audio->pbos[1][iter]);
  glGetTexImage(GL_TEXTURE_2D, 0, GL_GREEN, GL_FLOAT, 0); // async
  glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, 0);
  glBindTexture(GL_TEXTURE_2D, 0);
}

void audio_display2(struct audio *audio) {
  // copy pixel buffers
  for (int i = 0; i < AUDIO_COUNT; ++i) { // FIXME hardcoded
    int w = audio_write;
    for (int c = 0; c < AUDIO_CHANNELS; ++c) {
      glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, audio->pbos[c][i]);
      float *p = glMapBufferARB(GL_PIXEL_PACK_BUFFER_ARB, GL_READ_ONLY_ARB);
      if (p) {
        memcpy(&audio->buffer[w][c][0], p, audio->size * sizeof(float));
        if (! glUnmapBufferARB(GL_PIXEL_PACK_BUFFER_ARB)) {
          fprintf(stderr, "unmap buffer error\n");
        }
      } else {
        memset(&audio->buffer[w][c][0], 0, audio->size * sizeof(float));
      }
    }
    // don't overtake read pointer
    int audio_write2 = (w + 1) % AUDIO_BUFFERS;
    if (audio_write2 == audio_read) {
      audio_write2 = (audio_write2 - 1 + AUDIO_BUFFERS) % AUDIO_BUFFERS;
    }
    audio_write = audio_write2;
  }
  glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, 0);
  // non-realtime fun
#if 1
  if (audio->nrt) {
    int rate = 1;
    int fps = 25;
    int frame = audio->frame;
    int period = 60 * 60 * fps * rate; // 60 minutes
    int interval = 5 * 60 * fps * rate; // 5 minutes
    if ((frame % rate) == 0 && (frame % period) < interval) {
      audio_compute(audio, audio->nrtbuffers, 8 * audio->size); // FIXME hardcoded
      for (int i = 0; i < 8 * audio->size; ++i) { // FIXME hardcoded
        for (int c = 0; c < 2; ++c) {
          audio->nrtbuffers_interleaved[i*2+c] = audio->nrtbuffers[c][i];
        }
      }
      sf_write_float(audio->nrtf, &audio->nrtbuffers_interleaved[0], 2 * 8 * audio->size);// FIXME hardcoded
    } else if (frame % period == interval) {
      sf_write_sync(audio->nrtf);
    }
  }
#endif
  audio->frame++;
}

void audio_start(struct audio *audio) {
  switch (audio->state) {
    case audio_state_idle:
      audio->state = audio_state_intro;
      audio->sphase = 0;
      if (0)
        jack_transport_start(audio->client);
      break;
    default:
      break;
  }
}

void audio_stop(struct audio *audio) {
  switch (audio->state) {
    case audio_state_rolling:
      audio->state = audio_state_outro;
      audio->sphase = 0;
      break;
    default:
      break;
  }
}

// EOF
