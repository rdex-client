/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
===================================================================== */

uniform sampler2D texture; // U := r, V := g, other channels ignored
uniform float dx;          // horizontal distance between texels
uniform float dy;          // vertical distance between texels

void main(void) {
  vec2 p = gl_TexCoord[0].st;                         // coordinates
  vec2 gx = texture2D(texture, p + vec2(-dx,0.0)).rg  // x-wards edge
          + texture2D(texture, p + vec2( dx,0.0)).rg
          - texture2D(texture, p + vec2(0.0,0.0)).rg * 2.0;
  vec2 gy = texture2D(texture, p + vec2(0.0,-dy)).rg  // y-wards edge
          + texture2D(texture, p + vec2(0.0, dy)).rg
          - texture2D(texture, p + vec2(0.0,0.0)).rg * 2.0;
  vec2 xy = texture2D(texture, p + vec2(-dx,-dy)).rg  // xy-wards edge
          + texture2D(texture, p + vec2( dx, dy)).rg
          - texture2D(texture, p + vec2(0.0,0.0)).rg * 2.0;
  vec2 yx = texture2D(texture, p + vec2( dx, dy)).rg  // yx-wards edge
          + texture2D(texture, p + vec2(-dx,-dy)).rg
          - texture2D(texture, p + vec2(0.0,0.0)).rg * 2.0;
  float e = sqrt(dot(gx,gx)+dot(gy,gy)+dot(xy,xy)+dot(yx,yx));
  gl_FragColor = vec4(e, e, e, 1.0);                    // total edges
}
