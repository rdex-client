/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2009,2010  Claude Heiland-Allen <claude@mathr.co.uk>
===================================================================== */

uniform sampler2D texture; // U := r, V := g, other channels ignored

void main(void) {
  const mat3 lcc2rgb = mat3(                    // LC1C2 => RGB
     1.0,  1.407,  0.0,                         // colour space
     1.0, -0.677, -0.236,                       // conversion matrix
     1.0,  0.0,    1.848
  );
  vec2 p = gl_TexCoord[0].st;                   // texel coordinates
  vec2 q;
  float u, v, h, l;
  vec3 c;
  // point to colour
  q = texture2D(texture, p).rg;
  u = 1.0 - q.r;
  v = q.g;
  h = 16.0*atan(v, u);
  l = clamp(cos(16.0*(v*v + u*u)), 0.0, 1.0);
  c = vec3(l, 0.5*cos(h), 0.5*sin(h));
  gl_FragColor = vec4(c * lcc2rgb, 1.0);
}
