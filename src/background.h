/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2009,2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Background processing
===================================================================== */

#ifndef BACKGROUND_H
#define BACKGROUND_H 1

#include "image.h"
#include "histogram.h"
#include "segment.h"
#include "tamura.h"
#include "pfifo.h"

//======================================================================
// background state
struct background {
  const char *session;
  char *url;
  struct tamura tamura;
  struct pfifo *queue;
};

//======================================================================
// prototypes
struct background *background_init(struct background *background, const char *session);
void background_atexit(struct background *background);
void background_enqueue(
  struct background *background,
  float ru, float rv, float f, float k, float score, const char *behaviour,
//  char *thumb_ppm, int thumb_ppm_bytes, // must be freed by background thread
  char *full_ppm, int full_ppm_bytes,  // must be freed by background thread
  struct image *raw_image, // must be freed by background thread
  struct image *full_image // must be freed by background thread
);

#endif
