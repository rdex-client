/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
CPU-Based Images
===================================================================== */

#ifndef IMAGE_H
#define IMAGE_H 1

#include <GL/glew.h>

struct image {
  int width;
  int height;
  int channels;
  float *data;
};

#define I(i,x,y,z) ((i)->data[(i)->channels*((i)->width*(y)+(x))+(z)])

struct image *image_alloc(int width, int height, int channels);
void image_free(struct image *image);
void image_copytexture(struct image *image, GLuint texture);

#endif
