/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
CPU-Based Image Segmentation
===================================================================== */

#ifndef SEGMENT_H
#define SEGMENT_H 1

#include "image.h"

#define SEGMENT_BUCKETS 32
struct segment {
  float bucket[SEGMENT_BUCKETS];
};

void segment_calc(struct segment * s, const struct image *i);

#endif
