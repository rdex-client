/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010,2019  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Sequencer module
===================================================================== */

#include <math.h>
#include <GL/glew.h>
#include <GL/glut.h>
#include "sequence.h"
#include "sequence.vert.c"
#include "sequence.frag.c"
#if 0
struct sequence *sequence_init(struct sequence *sequence) {
  if (! shader_init(&sequence->shader, sequence_vert, sequence_frag)) {
    return 0;
  }
  shader_uniform(sequence, texture);
  shader_uniform(sequence, pack);
  shader_uniform(sequence, size);
  glyphs_init(&sequence->glyphs);
  for (int i = 0; i < SEQUENCE_LENGTH; ++i) {
    sequence->seq1[i].active = 0;
    sequence->seq1[i].index = - (i == 0);
    sequence->seq2[i].active = 0;
    sequence->seq2[i].index = -1;
    // pitch sequence
    sequence->pitch[i].active = 0;
    sequence->pitch[i].pitch = 50;
  }
  sequence->page = seqpage_library;
  sequence->step = 0;
  sequence->cursor = 0;
  sequence->frame = 0;
  return sequence;
}

void sequence_reshape(struct sequence *sequence, int w, int h) {
  sequence->width = w;
  sequence->height = h;
}

void sequence_display(struct sequence *sequence, struct library *library, struct reactiondiffusion *react, struct audio *audio) {
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

  // reaction sequence
  if (sequence->seq1[sequence->step].active) {
    struct libelem *e = &library->array[sequence->seq1[sequence->step].index];
    react->value.ru = e->v[0];
    react->value.rv = e->v[1];
    react->value.f  = e->v[2];
    react->value.k  = e->v[3];
  }

  // pitch sequence
  if (sequence->pitch[sequence->step].active) {
    audio->pitch = sequence->pitch[sequence->step].pitch;
  }
  
#if 0
  // curve/speed sequence
  if (sequence->frame == 0 && sequence->seq2[sequence->step].active) {
    switch (sequence->seq2[sequence->step].index) {
    case sequence_curve_less: audio->dcurve = fmax(audio->dcurve - 1.0, -1.0); break;
    case sequence_curve_more: audio->dcurve = fmin(audio->dcurve + 1.0,  1.0); break;
    case sequence_speed_less: audio->dspeed = fmax(audio->dspeed - 1.0, -1.0); break;
    case sequence_speed_more: audio->dspeed = fmin(audio->dspeed + 1.0,  1.0); break;
    default: break;
    }
  }
  if (sequence->frame == 0) {
    audio->speed += audio->dspeed;
    audio->curve += audio->dcurve;
  }
#endif

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glViewport(0, 0, sequence->width, sequence->height);
  gluOrtho2D(0, sequence->width, 0, sequence->height);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  // sequence 1: library balls
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, library->tex[library->which]);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glUseProgramObjectARB(sequence->shader.program);
  sequence->attr.spin   = glGetAttribLocationARB(sequence->shader.program, "myspin0");
//  sequence->attr.step   = glGetAttribLocationARB(sequence->shader.program, "mystep0");
  sequence->attr.cursor = glGetAttribLocationARB(sequence->shader.program, "mycursor0");
  sequence->attr.active = glGetAttribLocationARB(sequence->shader.program, "myactive0");
  sequence->attr.pitch  = glGetAttribLocationARB(sequence->shader.program, "mypitch0");
  sequence->value.texture = 0;
  sequence->value.pack    = library_pack;
  sequence->value.size    = library_packed_size;
  shader_updatei(sequence, texture);
  shader_updatef(sequence, pack);
  shader_updatef(sequence, size);
  glBegin(GL_QUADS); {
    for (int j = 0; j < SEQUENCE_LENGTH; ++j) {
      float t = sequence->step + sequence->frame / 12.0;
      float x0 = library->width * (j + 0.5) / SEQUENCE_LENGTH;
      float y0 = library->width * 0.5 / SEQUENCE_LENGTH;
      float d = 1.0 / (0.3 + pow(fmin(fmin(fabs(t - j), fabs(t - j + SEQUENCE_LENGTH)), fabs(t - j - SEQUENCE_LENGTH))/(SEQUENCE_LENGTH/2.0), 0.5));
      float dx = (0.75 + 0.25 * d) * y0;
      float dy = (0.75 + 0.25 * d) * y0;
        int i = sequence->seq1[j].index;
//        glVertexAttrib1f(sequence->attr.step,   j == sequence->step);
        glVertexAttrib1f(sequence->attr.cursor, j == sequence->cursor);
        glVertexAttrib1f(sequence->attr.active, sequence->seq1[j].active);
        glVertexAttrib1f(sequence->attr.pitch,  sequence->pitch[j].active ? sequence->pitch[j].pitch / 5 : 0);
      if (sequence->seq1[j].active) {
        glVertexAttrib2f(sequence->attr.spin, library->array[i].spin[0], library->array[i].spin[1]);
        glTexCoord4f( (i % library_pack)      / (float) library_pack, ((i / library_pack) + 1) / (float) library_pack, 0, 1);
      } else {
        glVertexAttrib2f(sequence->attr.spin, 0, 0);
        glTexCoord4f(-1,-1,0,1);
      }
        glVertex2f(x0 - dx, y0 + dy);
//        glVertexAttrib1f(sequence->attr.step,   j == sequence->step);
        glVertexAttrib1f(sequence->attr.cursor, j == sequence->cursor);
        glVertexAttrib1f(sequence->attr.active, sequence->seq1[j].active);
        glVertexAttrib1f(sequence->attr.pitch,  sequence->pitch[j].active ? sequence->pitch[j].pitch / 5 : 0);
      if (sequence->seq1[j].active) {
        glVertexAttrib2f(sequence->attr.spin, library->array[i].spin[0], library->array[i].spin[1]);
        glTexCoord4f(((i % library_pack) + 1) / (float) library_pack, ((i / library_pack) + 1) / (float) library_pack, 1, 1);
      } else {
        glVertexAttrib2f(sequence->attr.spin, 0, 0);
        glTexCoord4f(-1,-1,1,1);
      }
        glVertex2f(x0 + dx, y0 + dy);
//        glVertexAttrib1f(sequence->attr.step,   j == sequence->step);
        glVertexAttrib1f(sequence->attr.cursor, j == sequence->cursor);
        glVertexAttrib1f(sequence->attr.active, sequence->seq1[j].active);
        glVertexAttrib1f(sequence->attr.pitch,  sequence->pitch[j].active ? sequence->pitch[j].pitch / 5 : 0);
      if (sequence->seq1[j].active) {
        glVertexAttrib2f(sequence->attr.spin, library->array[i].spin[0], library->array[i].spin[1]);
        glTexCoord4f(((i % library_pack) + 1) / (float) library_pack,  (i / library_pack)      / (float) library_pack, 1, 0);
      } else {
        glVertexAttrib2f(sequence->attr.spin, 0, 0);
        glTexCoord4f(-1,-1,1,0);
      }
        glVertex2f(x0 + dx, y0 - dy);
//        glVertexAttrib1f(sequence->attr.step,   j == sequence->step);
        glVertexAttrib1f(sequence->attr.cursor, j == sequence->cursor);
        glVertexAttrib1f(sequence->attr.active, sequence->seq1[j].active);
        glVertexAttrib1f(sequence->attr.pitch,  sequence->pitch[j].active ? sequence->pitch[j].pitch / 5 : 0);
      if (sequence->seq1[j].active) {
        glVertexAttrib2f(sequence->attr.spin, library->array[i].spin[0], library->array[i].spin[1]);
        glTexCoord4f( (i % library_pack)      / (float) library_pack,  (i / library_pack)      / (float) library_pack, 0, 0);
      } else {
        glVertexAttrib2f(sequence->attr.spin, 0, 0);
        glTexCoord4f(-1,-1,0,0);
      }
        glVertex2f(x0 - dx, y0 - dy);
    }
  } glEnd();
  glUseProgramObjectARB(0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

#if 0
  // sequence 2: speed/curve controls
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  for (int j = 0; j < SEQUENCE_LENGTH; ++j) {
    if (sequence->seq2[j].active) {
      glBindTexture(GL_TEXTURE_2D, sequence->glyphs.textures[sequence->seq2[j].index]);
      glBegin(GL_QUADS); {
        float t = sequence->step + sequence->frame / 12.0;
        float x0 = library->width * (j + 0.5) / SEQUENCE_LENGTH;
        float y0 = 32;
        float d = 1.0 / (0.3 + pow(fmin(fmin(fabs(t - j), fabs(t - j + SEQUENCE_LENGTH)), fabs(t - j - SEQUENCE_LENGTH))/(SEQUENCE_LENGTH/2.0), 0.5));
        float dx = 24 + 8 * d;
        float dy = 24 + 8 * d;
        glColor4f(1,1,1,1);
        glTexCoord2f(0, 0); glVertex2f(x0 + dx, y0 + dy);
        glTexCoord2f(0, 1); glVertex2f(x0 + dx, y0 - dy);
        glTexCoord2f(1, 1); glVertex2f(x0 - dx, y0 - dy);
        glTexCoord2f(1, 0); glVertex2f(x0 - dx, y0 + dy);
      } glEnd();
    }
  }
  glDisable(GL_BLEND);
#endif

  glBindTexture(GL_TEXTURE_2D, 0);
  glDisable(GL_TEXTURE_2D);
  if (++sequence->frame == 12) {
    sequence->frame = 0;
    if (++sequence->step == SEQUENCE_LENGTH) {
      sequence->step = 0;
    }
  }

}

int sequence_keynormal(struct sequence *sequence, struct library *library, int key, int x, int y) {
  switch (key) {
/*
    case '[':
      switch (sequence->page) {
        case seqpage_curves:
          sequence->seq2[sequence->cursor].index = sequence_curve_less;
          sequence->seq2[sequence->cursor].active = 1;
          return 1;
        default:
          return 0;
      }
    case ']':
      switch (sequence->page) {
        case seqpage_curves:
          sequence->seq2[sequence->cursor].index = sequence_curve_more;
          sequence->seq2[sequence->cursor].active = 1;
          return 1;
        default:
          return 0;
      }
*/
    case ' ':
      switch (sequence->page) {
        case seqpage_library:
          sequence->seq1[sequence->cursor].active = 0;
          sequence->seq1[sequence->cursor].index = -1;
          return 1;
        case seqpage_curves:
          sequence->seq2[sequence->cursor].active = 0;
          sequence->seq2[sequence->cursor].index = -1;
          return 1;
      }

    // pitch sequence
#define P(c,p) \
    case c: sequence->pitch[sequence->cursor].pitch = p; sequence->pitch[sequence->cursor].active = 1; return 1
      P('7', 80); P('8', 85); P('9', 90); P('0', 95); P('-', 100);
      P('u', 60); P('i', 65); P('o', 70); P('p', 75);
      P('j', 40); P('k', 45); P('l', 50); P(';', 55);
      P('m', 20); P(',', 25); P('.', 30); P('/', 35);
#undef P
    case 'n': sequence->pitch[sequence->cursor].active = 0; return 1;

    default:
      return 0;
  }
}

int sequence_keyspecial(struct sequence *sequence, struct library *library, int key, int x, int y) {
  switch (key) {
    case GLUT_KEY_LEFT:
      sequence->cursor = (sequence->cursor + SEQUENCE_LENGTH - 1) % SEQUENCE_LENGTH;
      return 1;
    case GLUT_KEY_RIGHT:
      sequence->cursor = (sequence->cursor + 1) % SEQUENCE_LENGTH;
      return 1;
    case GLUT_KEY_UP:
      switch (sequence->page) {
      case seqpage_library:
        sequence->seq1[sequence->cursor].index = rand() % library->count;
        sequence->seq1[sequence->cursor].active = 1;
        break;
      case seqpage_curves:
        sequence->seq2[sequence->cursor].index = sequence_speed_more;
        sequence->seq2[sequence->cursor].active = 1;
        break;
      }
      return 1;
    case GLUT_KEY_DOWN:
      switch (sequence->page) {
      case seqpage_library:
        sequence->seq1[sequence->cursor].active = 0;
        sequence->seq1[sequence->cursor].index = -1;
        break;
      case seqpage_curves:
        sequence->seq2[sequence->cursor].index = sequence_speed_less;
        sequence->seq2[sequence->cursor].active = 1;
        break;
      }
      return 1;
    case GLUT_KEY_PAGE_UP:
      sequence->page = seqpage_library;
      return 1;
    case GLUT_KEY_PAGE_DOWN:
      sequence->page = seqpage_curves;
      return 1;
    default:
      return 0;
  }
}
#endif
