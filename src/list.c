/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2009  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
doubly-linked list implementation
===================================================================== */

#include <assert.h>
#include "list.h"

void list_init(struct list *l) {
  assert(l);
  l->head = &(l->headNode);
  l->tail = &(l->tailNode);
  l->head->pred = 0;
  l->head->next = l->tail;
  l->tail->pred = l->head;
  l->tail->next = 0;
}

int list_ishead(struct node *n) {
  assert(n);
  return !n->pred;
}

int list_istail(struct node *n) {
  assert(n);
  return !n->next;
}

int list_isempty(struct list *l) {
  assert(l && l->head && l->tail);
  return l->head->next == l->tail;
}

int list_length(struct list *l) {
  struct node *n = l->head->next;
  int i = 0;
  while (n != l->tail) {
    n = n->next;
    i++;
  }
  return i;
}

void list_remove(struct node *n) {
  assert(n && n->pred && n->next);
  n->pred->next = n->next;
  n->next->pred = n->pred;
  n->next = 0;
  n->pred = 0;
}

void list_insertbefore(struct node *n, struct node *beforethis) {
  assert(n && beforethis && beforethis->pred);
  n->next = beforethis;
  n->pred = beforethis->pred;
  beforethis->pred->next = n;
  beforethis->pred = n;
}

void list_insertafter(struct node *n, struct node *afterthis) {
  assert(n && afterthis && afterthis->next);
  n->next = afterthis->next;
  n->pred = afterthis;
  afterthis->next->pred = n;
  afterthis->next = n;
}

void list_inserttail(struct list *l, struct node *n) {
  assert(l);
  list_insertbefore(n, l->tail);
}


struct node *list_removehead(struct list *l) {
  assert(l && ! list_isempty(l));
  struct node *n = l->head->next;
  list_remove(n);
  return n;
}

// EOF

