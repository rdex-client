/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2009,2010,2017,2019  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Main Program Entry Point
===================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <GL/glew.h>
#include <GL/glut.h>

#ifdef RDEX_ICON
#include <GL/glx.h>
#include <X11/StringDefs.h>
#include <X11/Intrinsic.h>
#include <X11/IntrinsicP.h>
#include <X11/Shell.h>
#ifdef VMS
#include <X11/shape.h>
#else
#include <X11/extensions/shape.h>
#endif
#include <X11/xpm.h>
#endif

#include "rdex.h"

#ifdef RDEX_ICON
#include "rdex-logo-32x32.xpm"

struct XpmIcon {
  Pixmap pixmap;
  Pixmap mask;
  XpmAttributes attributes;
};
#endif

int main(int argc, char **argv) {
  /* initialize GLUT etc */
  fprintf(stderr, "rdex (GPL) 2008-2019 Claude Heiland-Allen <claude@mathr.co.uk>\n");
  srand(time(NULL));
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(788, 576);
  glutInit(&argc, argv);
  glutCreateWindow("rdex");
  glutFullScreen();
  glutSetCursor(GLUT_CURSOR_NONE);
  glewInit();
#ifdef RDEX_ICON
  /* set icon */
  Display *display = glXGetCurrentDisplay();
  if (display) {
    GLXDrawable drawable = glXGetCurrentDrawable();
    if (drawable) {
      struct XpmIcon icon;
      if (0 == XpmCreatePixmapFromData(display, drawable, rdex_logo_32x32_xpm, &icon.pixmap, &icon.mask, NULL)) {
        XWMHints* hints = XAllocWMHints();
        if (hints) {
          hints->flags = IconPixmapHint;
          hints->icon_pixmap = icon.pixmap;
          if (icon.mask) {
            hints->flags |= IconMaskHint;
            hints->icon_mask = icon.mask;
          }
          XSetWMHints(display, drawable, hints);
          XFree(hints);
        }
      }
    }
  }
#endif
  /* activate callbacks and enter main loop */
  if (rdex_init()) {
    glutDisplayFunc(rdex_display);
    glutReshapeFunc(rdex_reshape);
    glutTimerFunc(1, rdex_timer, 0);
//    glutIdleFunc(rdex_idle);
    glutPassiveMotionFunc(rdex_pmotion);
    glutMotionFunc(rdex_amotion);
    glutMouseFunc(rdex_mouse);
    glutKeyboardFunc(rdex_keynormal);
    glutSpecialFunc(rdex_keyspecial);
    glutSetCursor(GLUT_CURSOR_NONE);
    glutMainLoop();
    return 0; // never reached
  } else {
    return 1;
  }
}

// EOF
