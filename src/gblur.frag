/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Separated Gaussian Blur Shader
===================================================================== */

uniform sampler2D texture;
uniform vec2 d;

// weights
const float w0 = 0.402619947, w1 = 0.244201342, w2 = 0.054488685;

void main(void) {
  vec2 p = gl_TexCoord[0].st;
  vec4 o = vec4(0.0, 0.0, 0.0, 0.0);
  o += w2 * texture2D(texture, p - 2.0 * d);
  o += w1 * texture2D(texture, p -       d);
  o += w0 * texture2D(texture, p          );
  o += w1 * texture2D(texture, p +       d);
  o += w2 * texture2D(texture, p + 2.0 * d);
  gl_FragColor = o;
}
