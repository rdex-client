/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Screenshot Module
===================================================================== */

#ifndef SCREENSHOT_H
#define SCREENSHOT_H 1

#include "pfifo.h"

//======================================================================
// screenshot module data
struct screenshot {
  int nrt;
  int rate;
  int frame;
  int width;
  int height;
  char *buffer;
  GLuint pbo;
  int ready;
  struct pfifo *queue;
};

//======================================================================
// prototypes
struct screenshot *screenshot_init(struct screenshot *screenshot, int nrt);
void screenshot_display1(struct screenshot *screenshot);
void screenshot_display2(struct screenshot *screenshot);
void screenshot_reshape(struct screenshot *screenshot, int w, int h);
void screenshot_idle(struct screenshot *screenshot);

#endif
