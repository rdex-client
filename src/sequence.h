/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Sequencer module
===================================================================== */

#ifndef SEQUENCE_H
#define SEQUENCE_H 1

#include "shader.h"
#include "library.h"
#include "reactiondiffusion.h"
#include "audio.h"
#include "glyphs.h"

#define SEQUENCE_LENGTH 16

struct seqelem {
  int active; // is this step going to change things
  int index;  // index for this step
};

enum sequence_page { seqpage_library, seqpage_curves };

struct sequence { struct shader shader;
  struct {
    GLint texture, pack, size; // fragment
  } uniform;
  struct {
    int texture; float pack, size; // fragment
  } value;
  struct {
    GLint spin, cursor, step, active, pitch; // vertex
  } attr;
  struct seqelem seq1[SEQUENCE_LENGTH]; // library index

  struct { int active; float pitch; } pitch[SEQUENCE_LENGTH]; // pitch sequence  

  struct glyphs glyphs;
  struct seqelem seq2[SEQUENCE_LENGTH]; // curve/speed controls
  enum sequence_page page;
  int step;
  int cursor;
  int frame;
  int width;
  int height;
};

struct sequence *sequence_init(struct sequence *sequence);
void sequence_reshape(struct sequence *sequence, int w, int h);
void sequence_display(struct sequence *sequence, struct library *library, struct reactiondiffusion *react, struct audio *audio);
int sequence_keynormal(struct sequence *sequence, struct library *library, int key, int x, int y);
int sequence_keyspecial(struct sequence *sequence, struct library *library, int key, int x, int y);

#endif
