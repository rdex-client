/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2010,2011,2019  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
4D->3D projection shader for ball picking
===================================================================== */

// 4D perspective projection
uniform vec4 origin;
uniform vec4 scale;
uniform mat4 rotate;
uniform vec4 translate;
uniform vec4 near;
uniform vec4 far;

// billboarding
attribute vec2 delta;

varying float ignore;

void main(void) {
  vec4 p0 = rotate * (0.5 * scale * (gl_Vertex - origin));
  vec4 p = p0 + translate;
  mat4 m = mat4(
    2.0*near.w/(far.x - near.x), 0.0, 0.0, (far.x + near.x)/(far.x - near.x),
    0.0, 2.0*near.w/(far.y - near.y), 0.0, (far.y + near.y)/(far.y - near.y),
    0.0, 0.0, 2.0*near.w/(far.z - near.z), (far.z + near.z)/(far.z - near.z),
    0.0, 0.0, 0.0,                         (far.w + near.w)/(far.w - near.w)
  );
  float k = 2.0 * near.w * far.w / (far.w - near.w);
  vec4 q = m * p;
  q.w -= k;
  q /= p.w;
  vec4 v = vec4(10000.0, 10000.0, 10000.0, 1.0); // way off-screen
  float r2 = dot(p0,p0) * 0.5;
  float s = 0.0;
  if (r2 < 1.0) {
    s = sqrt(1.0 - r2);
    v = gl_ModelViewProjectionMatrix * q;
    v.xy += delta * s;
    ignore = 0.0;
  } else {
    ignore = 1.0;
  }
  gl_TexCoord[0] = gl_MultiTexCoord0;
  gl_Position = v;
}
