/* =====================================================================
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2010,2011  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Shader for text
===================================================================== */

uniform sampler2D texture;

void main(void) {
  vec2 p = gl_TexCoord[0].xy;
  vec4 c = vec4(0.0, 1.0, 0.0, 1.0) * texture2D(texture, p).rgba;
  if (c.a < 0.1) {
    discard;
  } else {
    gl_FragColor = c;
  }
}
