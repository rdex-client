rdex-client tickets
===================

URGENT
------

  * bash any possible NaN/inf to 0 in audio
  * intro sequence
  * outro sequence
  * use vertex arrays instead of immediate mode for ball drawing

TODO
----

  * check audio sample rate / video sample rate interdependence
  * game controller input
  * 8-way nearest neighbour navigation for sequence editing
  * set CPU affinity and performance governors if necessary
  * interpolate textures for audio if not everything
  * attempt to use async PBOs for realtime video recording
  * restore non-realtime rendering
  * fix audio at other than JACK/48000Hz
  * merge from CyPi for loading screen/progress bar
  * incremental library/session loading with progress bar
  * sequencer controls for audio radius/spin/travel/speed/etc
  * keyboard controls for reseed stuff
  * audio waveform / spirograph display overlay
  * fix Makefile dependencies to avoid memory corruption and OS crash
  * use multiple texture tile-sheets to boost ball count limit
  * fill hyperspace with fog/dust/ether
  * alpha blend balls with background for smoother appearance
  * video script option handling for 4:3 vs 16:9 aspect ratio
  * full screen anti-aliasing (render at 2x size, downscale smoothly)
  * interestingness boost by seeing if larger r-d grid works fast enough
  * authentication for upload to server
  * maintain README to be in sync with implementation
  * webpage texts

ONGOING
-------

  * make some videos at various stages of development

LATER
-----

  * motion blur
  * make balls nicely glowing
  * ripples when interesting species found
  * catch libcurl std(out|err) output and use it somehow
    http://curl.haxx.se/libcurl/c/curl_easy_setopt.html#CURLOPTWRITEFUNCTION
  * statistics display in-program
  * use GLEW more sensibly/correctly/etc
  * use OpenGL 2.x variants instead of a mishmash of ARB/EXT stuff
  * investigate OpenGL 3.x/4.x API changes and start updating things
  * Debian/Ubuntu/etc packages
  * select point from library for rendering
  * rdex-remote to handle rdex:// urls from browser for above
  * interactive camera controls (back to rolling ball?)
  * make camera target have an orientation somehow
  * make camera target spin
  * 4D lighting of worldsphere and balls
  * paint into the reactions
  * add more automation of mode switching ("attract mode")
  * investigate USB game controller support
  * distributed rendering!?
  * rdex.rc for options (eg, session dir and initial fullscreenness)
  * add command line arguments to override environment variables
  * runtime setting of NRT render frame rate (both audio and video)
  * runtime setting of NRT capture intervals (both period and interval)
  * runtime setting of NRT quit when rendering complete
  * runtime setting of display aspect ratio and size
  * lock display size when in NRT mode to avoid accidental mess-ups
  * hyperbolic layout by image similarity
  * download library from server, again with threads
  * download library using bittorrent or other p2p

DONE
----

  * worldsphere size variation over time (2010-09-27)
  * disable screensaver and DPMS etc (2010-09-25)
  * send JACK transport commands on start / stop commands (2010-09-25)
  * investigate sync to vblank (2010-09-25)
  * hide mouse cursor (2010-09-25)
  * fix explore/random mode and generate a new pristine session dir (2010-05-17)
  * CPU to generate coordinate texture for audio.frag lookup (2010-05-14)
  * audio coordinates to vary smoothly over time to reduce glitches (2010-05-14)
    ie: spirograph-style wandering instead of discrete circles
  * audio coordinates to be integrated with overdrive to reduce glitches (2010-05-14)
    ie: grab part of waveform from each overdriven frame
  * quality boost by antialiased downscaling to 64x64 or so for the thumbs (2010-05-14)
    ie: upscale -> false colours -> downscale smoothly
  * improve audio tone smoothness (better interpolation/filtering) [ATi BUG] (2010-05-12)
  * check that the linear interpolation implementation is correct [ATi BUG] (2010-05-12)
  * audio dynamic range compression [using jack-rack] (2010-05-12)
  * window size/aspect appearance independence (seems fixed?) (2010-05-12)
  * worldsphere aspect ratio checkup (is it really circular) (2010-05-12)
  * test 4:3 to see if it looks better than 16:9 [handle both] (2010-05-12)
  * video script should clean up files when complete (2010-05-08)
  * video script should generate an ogv from the mpeg (2010-05-08)
  * space scaling for camera physics as well as ball display (2010-05-06)
  * improve audio change smoothness (more often than display frame rate) (2010-05-05)
  * audio sample rate handling logic (sound should be rate independent) (2010-05-05)
  * speed up by using vertex shaders for projection from 4D (billboarding) (2010-05-04)
  * immersive hyperspace experience (flying around to new worlds) (2010-05-04)
  * speed up by using arrays with large texture containing many small images (2010-04-??)
  * move image analysis stuff into uploader thread (avoid long pause) (2010-??-??)
  * backport style from current rdex-server (2009-10-27)
  * use threads to upload to server in background (2009-10-18)
  * test on other machines (2009-10-17)
  * rdex server (Python/WSGI) with rdex client and browser clients (2009-04-04)
  * make cute icon! (2009-04-01)
  * random vs search vs what? research, decide! (perturb) (2009-04-01)
  * investigate glut "game mode" instead of "fullscreen" (sucks) (2009-04-01)
  * debug segfault on startup (latest fglrx is fine) (2009-04-01)
  * image texture feature extraction (ripped from FIRE) (2008-11-07)
  * health warning (2008-11-03)
  * hide mouse pointer (2008-11-03)
  * fullscreen mode support (2008-11-03)
  * handle perspective frustrum properly (2008-11-03)
  * debug rolling ball implementation (2008-11-03)
  * rationalize Makefile (2008-11-03)
  * depth sorting of points for library display (2008-11-02)
  * save library to session folder while running (2008-11-02)
  * load library from session folder at startup (2008-11-02)
  * split the behemoth (2008-11-01)

WONT
----

  * interpolate between adjacent frames for even smoother audio
  * audio silence detection -> position randomization
  * space scaling by local ball density (statistics of nearest N balls)

