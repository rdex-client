#!/bin/bash
MKV="rdex-client-$(date --iso=s | tr ':' '-').mkv"
RDEX_RENDER="1" RDEX_SESSION="./session/" ./src/rdex 2>"${MKV}.log" > "${HOME}/out/rec.ppm"
ecasound -f:f32,1,48000 -a 1 -i:audio.l.raw -chmove 1,1 -ea 200 -a 2 -i:audio.r.raw -chmove 1,2 -ea 200 -a 1,2 -f:s16_le,2,48000 -o audio.wav &&
ffmpeg -r 25 -f image2pipe -i "${HOME}/out/rec.ppm" -b 5000000 -i audio.wav -ab 192000 "${MKV}"
#ppmtoy4m -S 444 -F 25:1 |
#y4mscaler -I sar=1/1 -O preset=dvd -O yscale=1/1 |
#mpeg2enc -f 8 -q 3 -b 8000 -B 768 -D 9 -g 9 -G 15 -P -R 2 -o video.m2v &&
#ecasound -f:f32,1,48000 -a 1 -i:audio.l.raw -chmove 1,1 -ea 200 -a 2 -i:audio.r.raw -chmove 1,2 -ea 200 -a 1,2 -f:s16_le,2,48000 -o audio.wav &&
#twolame -b 224 audio.wav &&
#mplex -f 8 -V -o "${MPEG}" video.m2v audio.mp2 &&
#rm -f video.m2v audio.mp2 audio.wav audio.l.raw audio.r.raw &&
#ffmpeg2theora -p preview "${MKV}"
